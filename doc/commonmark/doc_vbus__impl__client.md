# Header file `vbus/impl/client.hpp`

``` cpp
#include "response_handler.hpp"

#include "string_array.hpp"

#include "subcription_handler.hpp"

#include "type_helpers.hpp"

namespace vbus
{
    namespace abi
    {
        class client;
    }
}
```
