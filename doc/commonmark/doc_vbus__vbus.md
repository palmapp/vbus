# Header file `vbus/vbus.h`

``` cpp
#define EXPORT_API

#define IMPORT_API

#define VBUS_API

extern "C"
{
    struct vbus_string;

    using vbus_string_t = vbus_string;

    struct vbus_string_array;

    using vbus_string_array_t = vbus_string_array;

    struct vbus_uuid;

    using vbus_uuid_t = vbus_uuid;

    struct vbus_error;

    using vbus_error_t = vbus_error;

    struct vbus_response;

    using vbus_response_t = vbus_response;

    struct vbus_request;

    using vbus_request_t = vbus_request;

    using vbus_directory_ptr = void*;

    using vbus_client_ptr = void*;

    using vbus_message_sender_ptr = void*;

    using subscription_callback = void(*)(void*, vbus_string_array_t);

    using vbus_service_callback_ = void(*)(void*, vbus_message_sender_ptr, vbus_request_t*, vbus_string_array_t);

    struct vbus_service;

    using vbus_service = vbus_service;

    struct vbus_service_config;

    using vbus_service_config = vbus_service_config;

    vbus_directory_ptr vbus_run_directory(vbus_string_t properties);

    vbus_string_t vbus_get_connection_string(vbus_directory_ptr directory);

    void vbus_free_directory(vbus_directory_ptr directory);

    void vbus_init_logger(vbus_string_t easylogging_properties);

    vbus_client_ptr vbus_create_client(vbus_string_t properties);

    vbus_client_ptr vbus_create_service(vbus_string_t properties, vbus_service_config config);

    void vbus_run_async(vbus_client_ptr client);

    void vbus_send_no_response(vbus_message_sender_ptr sender);
}
```

## Struct `vbus_string`

``` cpp
struct vbus_string
{
    size_t length;

    char const* data;
};
```

This is a string representation for vbus interaction.

It is just length and data pointer. When length == 0 then data can point to nullptr

It should be always treated as non-owning view to string data. When passed to vbus API it will get copied. When passed to callbacks the life time is going to be managed by vbus\_client\_t instance.

#### Member variables

  - `length` - length of the string - zero termination byte is excluded
  - `data` - data pointer, if length == 0 it can be nullptr

-----

## Type alias `vbus_string_t`

``` cpp
using vbus_string_t = vbus_string;
```

C - style type definition

-----

## Struct `vbus_string_array`

``` cpp
struct vbus_string_array
{
    size_t length;

    vbus_string_t* elements;
};
```

This is a basic string non-owning array view representation.

It is used as message object that contains its parts.

It should be always treated as non-owning view to string data. When passed to vbus API it will get copied. When passed to callbacks the life time is going to be managed by vbus\_client\_t instance.

#### Member variables

  - `length` - number of string elements in the array
  - `elements` - pointer to first string element in the array, if length == 0 it can be nullptr.

-----

## Type alias `vbus_string_array_t`

``` cpp
using vbus_string_array_t = vbus_string_array;
```

C - style type definition

-----

## Struct `vbus_uuid`

``` cpp
struct vbus_uuid
{
    uint8_t data[16];
};
```

128bit UUID representation

#### Member variables

  - `data` - 16 bytes length array

-----

## Type alias `vbus_uuid_t`

``` cpp
using vbus_uuid_t = vbus_uuid;
```

C - style type definition

-----

## Struct `vbus_error`

``` cpp
struct vbus_error
{
    vbus_string_t message;

    int64_t code;
};
```

Error reporting struct

#### Member variables

  - `message` - The error message
  - `code` - The error code

-----

## Type alias `vbus_error_t`

``` cpp
using vbus_error_t = vbus_error;
```

C - style type definition

-----

## Struct `vbus_response`

``` cpp
struct vbus_response
{
    int64_t status;

    vbus_string_array_t message;
};
```

VBUS response object

The response instance provided when request\_callback is called is  owned by caller. The response object returned from the vbus\_send\_request\_sync must be freed by calling vbus\_free\_response function.

#### Member variables

  - `message` - response message data

### Variable `vbus_response::status`

``` cpp
int64_t status;
```

status == 0 - means success status \>= 0 - can report eg. number of items status \< 0 - is considered to be an error status == -503 - Service Unavailable (time out)

Meaning of returned statuses can differ depending on your service implementation.

-----

-----

## Type alias `vbus_response_t`

``` cpp
using vbus_response_t = vbus_response;
```

C - style type definition

-----

## Struct `vbus_request`

``` cpp
struct vbus_request
{
    vbus_string_t service_id;

    int64_t action_id;

    vbus_uuid request_id;

    int64_t time_out_at;

    int64_t sequence_number;

    void* user_data;

    void(* request_callback)(vbus_request*, vbus_response_t*);
};
```

VBUS request object

#### Member variables

  - `service_id` - limit is 127 bytes long + 1 byte for binary zero = 128
  - `action_id` - custom action id that can be anything that underlying service recognizes
  - `request_id` - unique request id it is generated by vbus\_create\_request method
  - `time_out_at` - request scoped timeout settings (default 0 no timeout) - milliseconds since 1.1.1970 (unix time)
  - `sequence_number` - request sequence number - it can be used to build more failure tolerant systems it is automatically assigned and incremented during creation in vbus\_create\_request but can be changed to anything that make sense in the service context
  - `user_data` - pointer to data for request\_callback default nullptr - keep it default value for synchronous request processing
  - `request_callback` - request callback for async request processing default nullptr - keep it default value for request processing

-----

## Type alias `vbus_request_t`

``` cpp
using vbus_request_t = vbus_request;
```

C - style type definition

-----

## Type alias `vbus_directory_ptr`

``` cpp
using vbus_directory_ptr = void*;
```

lifecycle handle for vbus directory service is started by calling vbus\_run\_directory service is stopped by calling vbus\_free\_directory

-----

## Type alias `vbus_client_ptr`

``` cpp
using vbus_client_ptr = void*;
```

lifecycle handle for vbus infrastruture client client connection is created by calling vbus\_create\_client service server and client connection calling vbus\_create\_service

then you can use channel subscribtion: vbus\_subscribe

the last call in the initialization must by vbus\_run\_async to start one the communication background thread

instance is stopped and destroyed by calling vbus\_free\_client

-----

## Type alias `vbus_message_sender_ptr`

``` cpp
using vbus_message_sender_ptr = void*;
```

handler for message sender used when service generates a response for the received request message

-----

## Type alias `subscription_callback`

``` cpp
using subscription_callback = void(*)(void*, vbus_string_array_t);
```

subscription callback function

param void \* user\_data - context used in subscription callback registration

param vbus\_string\_array\_t message - received message (index 0 - channel, index \> 0 - message parts)

-----

## Type alias `vbus_service_callback_`

``` cpp
using vbus_service_callback_ = void(*)(void*, vbus_message_sender_ptr, vbus_request_t*, vbus_string_array_t);
```

This function is required to call vbus\_send\_response in some time later with vbus\_response that has been allocated with create\_response

-----

## Struct `vbus_service`

``` cpp
struct vbus_service
{
    vbus_string_t id;

    vbus_string_t interface;

    vbus_string_t data;

    void* user_data;

    vbus_service_callback_ service_callback;
};
```

vbus\_service configuration struct

#### Member variables

  - `id` - the unique service id amongs the system the id must be \< 127 bytes long
  - `interface` - not used at the moment - future interface identification
  - `data` - not used at the moment - future metadata about service in domain specific format

### Variable `vbus_service::user_data`

``` cpp
void* user_data;
```

user data will be passed to vbus\_service\_callback\_ as first argument user\_data instance must outlive vbus\_free\_client

*Requires:* user\_data \!= nullptr

-----

### Variable `vbus_service::service_callback`

``` cpp
vbus_service_callback_ service_callback;
```

the actual callback function for receiving service messages

*Requires:* service\_callback \!= nullptr

-----

-----

## Type alias `vbus_service`

``` cpp
using vbus_service = vbus_service;
```

C - style type definition

-----

## Struct `vbus_service_config`

``` cpp
struct vbus_service_config
{
    size_t services_length;

    vbus_service* services;

    size_t thread_pool_count;
};
```

struct used in vbus\_create\_service to configure serviced by the client services will share the same thread pool

#### Member variables

  - `thread_pool_count` - if \> 1 thread pool is created and used to handle requests

### Variable `vbus_service_config::services_length`

``` cpp
size_t services_length;
```

the number of services in the array

*Requires:* services\_length \> 0

-----

### Variable `vbus_service_config::services`

``` cpp
vbus_service* services;
```

pointer to the array that will contain service configuration

*Requires:* services \!= nullptr

-----

-----

## Type alias `vbus_service_config`

``` cpp
using vbus_service_config = vbus_service_config;
```

C - style type definition

-----

## Function `vbus_run_directory`

``` cpp
vbus_directory_ptr vbus_run_directory(vbus_string_t properties);
```

This will start the inproc vbus directory on random port.

*Return values:* running vbus directory instance

You will get the current connection string from vbus\_get\_connection\_string.

Use vbus\_free\_directory to stop and destroy vbus\_directory instance.

-----

## Function `vbus_get_connection_string`

``` cpp
vbus_string_t vbus_get_connection_string(vbus_directory_ptr directory);
```

This method returns the connection string to the provided directory service.

*Requires:* This method must be called during initialization phase

*Return values:* the connection string that can be used to connect to the provided directory

This method is not thread safe.

-----

## Function `vbus_free_directory`

``` cpp
void vbus_free_directory(vbus_directory_ptr directory);
```

Method stops and destroys the vbus\_directory instance.

This method should be called during application cleanup phase.

#### Parameters

  - `directory` - \!= nullptr

-----

## Function `vbus_init_logger`

``` cpp
void vbus_init_logger(vbus_string_t easylogging_properties);
```

Provide easylogging properties. If the argument is a valid path logging settings is going to be loaded from it.

Configuration string can be provided inplace as direct loaded string.

Configuration is described here: https://github.com/amrayn/easyloggingpp

#### Parameters

  - `easylogging_properties` - requires to be not empty

-----

## Function `vbus_create_client`

``` cpp
vbus_client_ptr vbus_create_client(vbus_string_t properties);
```

It creates a vbus\_client instance. This method differs from vbus\_create\_service in a way that it will to expose any service api to the directory service.

*Return values:* the initialized vbus\_client - next steps (in following order): a) vbus\_subscribe + vbus\_set\_subscription\_callback  (optional) - to subscribe one or more channels b) vbus\_run\_async - start bg thread to handle communication c) sending requests through vbus\_send\_request/vbus\_send\_request\_sync methods

Use this if you want to create a client for existing service or subscribe to some PUB/SUB channel.

### Parameter `properties`

``` cpp
vbus_string_t properties
```

  - following configuration is necessary to establish connection to the directory service: vbus.connection.string = tcp://\<vbus server ip\>:\<vbus port\> vbus.instance.name = \<instance name for the vbus directory\> default value = inproc-dir

-----

-----

## Function `vbus_create_service`

``` cpp
vbus_client_ptr vbus_create_service(vbus_string_t properties, vbus_service_config config);
```

It creates a vbus\_client instance. This method differs from vbus\_create\_client in a way that it allows you to define services that are callable.

*Return values:* the initialized vbus\_client - next steps (in following order): a) vbus\_subscribe + vbus\_set\_subscription\_callback  (optional) - to subscribe one or more channels b) vbus\_run\_async - start bg thread to handle communication c) sending requests through vbus\_send\_request/vbus\_send\_request\_sync methods

Use this if you want to service, send requests to other services and  subscribe to some PUB/SUB channel.

### Parameter `properties`

``` cpp
vbus_string_t properties
```

  - following configuration is necessary to establish connection to the directory service: vbus.connection.string = tcp://\<vbus server ip\>:\<vbus port\> vbus.instance.name = \<instance name for the vbus directory\> default value = inproc-dir vbus.service.private.ip = \<ip address to bind a TCP server\> default value = 127.0.0.1 vbus.service.public.ip  = \<ip address that is used to access from outside world\> default value = 127.0.0.1

-----

### Parameter `config`

``` cpp
vbus_service_config config
```

  - exported services and thread pool settings

-----

-----

## Function `vbus_run_async`

``` cpp
void vbus_run_async(vbus_client_ptr client);
```

This will start a new vbus thread to do vbus\_request processing.

This method can be call only one time per client instance If you wants to terminate the thread call vbus\_free\_client Note: all vbus\_subscribe call must be done before this call

@param client \!= nullptr

-----

## Function `vbus_send_no_response`

``` cpp
void vbus_send_no_response(vbus_message_sender_ptr sender);
```

This method must be called when no response is generated to free the request context memory @param sender

-----
