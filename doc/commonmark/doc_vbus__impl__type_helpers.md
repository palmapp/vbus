# Header file `vbus/impl/type_helpers.hpp`

``` cpp
namespace vbus
{
    namespace abi
    {
        struct request_deleter;

        struct response_deleter;
    }
}
```
