# Project index

  - [`EXPORT_API`](doc_vbus__vbus.md#standardese-vbus-h)

  - [`IMPORT_API`](doc_vbus__vbus.md#standardese-vbus-h)

  - [`VBUS_API`](doc_vbus__vbus.md#standardese-vbus-h)

  - [`subscription_callback`](doc_vbus__vbus.md#standardese-subscription_callback) - subscription callback function

  - ## Namespace `vbus`
    
      - ### Namespace `vbus::abi`
        
          - [`client`](doc_vbus__impl__client.md#standardese-vbus__abi)
        
          - [`connection_data`](doc_vbus__impl__connection_data.md#standardese-vbus__abi)
        
          - [`directory_service`](doc_vbus__impl__directory_service.md#standardese-vbus__abi)
        
          - [`functor_response_handler`](doc_vbus__impl__response_handler.md#standardese-vbus__abi)
        
          - [`functor_service`](doc_vbus__impl__service_config.md#standardese-vbus__abi)
        
          - [`functor_subscription_handler`](doc_vbus__impl__subcription_handler.md#standardese-vbus__abi)
        
          - [`request_deleter`](doc_vbus__impl__type_helpers.md#standardese-vbus__abi)
        
          - [`response_deleter`](doc_vbus__impl__type_helpers.md#standardese-vbus__abi)
        
          - [`response_handler`](doc_vbus__impl__response_handler.md#standardese-vbus__abi)
        
          - [`response_sender`](doc_vbus__impl__service_config.md#standardese-vbus__abi)
        
          - [`service`](doc_vbus__impl__service_config.md#standardese-vbus__abi)
        
          - [`service_config`](doc_vbus__impl__service_config.md#standardese-vbus__abi)
        
          - [`service_metadata`](doc_vbus__impl__service_config.md#standardese-vbus__abi)
        
          - [`string_array`](doc_vbus__impl__string_array.md#standardese-vbus__abi)
        
          - [`subscription_handler`](doc_vbus__impl__subcription_handler.md#standardese-vbus__abi)

  - [`vbus_client_ptr`](doc_vbus__vbus.md#standardese-vbus_client_ptr) - lifecycle handle for vbus infrastruture client client connection is created by calling vbus\_create\_client service server and client connection calling vbus\_create\_service

  - [`vbus_create_client`](doc_vbus__vbus.md#standardese-vbus_create_client-vbus_string_t-) - It creates a vbus\_client instance. This method differs from vbus\_create\_service in a way that it will to expose any service api to the directory service.

  - [`vbus_create_service`](doc_vbus__vbus.md#standardese-vbus_create_service-vbus_string_t-vbus_service_config-) - It creates a vbus\_client instance. This method differs from vbus\_create\_client in a way that it allows you to define services that are callable.

  - [`vbus_directory_ptr`](doc_vbus__vbus.md#standardese-vbus_directory_ptr) - lifecycle handle for vbus directory service is started by calling vbus\_run\_directory service is stopped by calling vbus\_free\_directory

  - [`vbus_error`](doc_vbus__vbus.md#standardese-vbus_error) - Error reporting struct

  - [`vbus_error_t`](doc_vbus__vbus.md#standardese-vbus_error_t) - C - style type definition

  - [`vbus_free_directory`](doc_vbus__vbus.md#standardese-vbus_free_directory-vbus_directory_ptr-) - Method stops and destroys the vbus\_directory instance.

  - [`vbus_get_connection_string`](doc_vbus__vbus.md#standardese-vbus_get_connection_string-vbus_directory_ptr-) - This method returns the connection string to the provided directory service.

  - [`vbus_init_logger`](doc_vbus__vbus.md#standardese-vbus_init_logger-vbus_string_t-) - Provide easylogging properties. If the argument is a valid path logging settings is going to be loaded from it.

  - [`vbus_message_sender_ptr`](doc_vbus__vbus.md#standardese-vbus_message_sender_ptr) - handler for message sender used when service generates a response for the received request message

  - [`vbus_request`](doc_vbus__vbus.md#standardese-vbus_request) - VBUS request object

  - [`vbus_request_t`](doc_vbus__vbus.md#standardese-vbus_request_t) - C - style type definition

  - [`vbus_response`](doc_vbus__vbus.md#standardese-vbus_response) - VBUS response object

  - [`vbus_response_t`](doc_vbus__vbus.md#standardese-vbus_response_t) - C - style type definition

  - [`vbus_run_async`](doc_vbus__vbus.md#standardese-vbus_run_async-vbus_client_ptr-) - This will start a new vbus thread to do vbus\_request processing.

  - [`vbus_run_directory`](doc_vbus__vbus.md#standardese-vbus_run_directory-vbus_string_t-) - This will start the inproc vbus directory on random port.

  - [`vbus_send_no_response`](doc_vbus__vbus.md#standardese-vbus_send_no_response-vbus_message_sender_ptr-) - This method must be called when no response is generated to free the request context memory @param sender

  - [`vbus_service`](doc_vbus__vbus.md#standardese-vbus_service) - vbus\_service configuration struct

  - [`vbus_service_callback_`](doc_vbus__vbus.md#standardese-vbus_service_callback_) - This function is required to call vbus\_send\_response in some time later with vbus\_response that has been allocated with create\_response

  - [`vbus_service_config`](doc_vbus__vbus.md#standardese-vbus_service_config) - struct used in vbus\_create\_service to configure serviced by the client services will share the same thread pool

  - [`vbus_string`](doc_vbus__vbus.md#standardese-vbus_string) - This is a string representation for vbus interaction.

  - [`vbus_string_array`](doc_vbus__vbus.md#standardese-vbus_string_array) - This is a basic string non-owning array view representation.

  - [`vbus_string_array_t`](doc_vbus__vbus.md#standardese-vbus_string_array_t) - C - style type definition

  - [`vbus_string_t`](doc_vbus__vbus.md#standardese-vbus_string_t) - C - style type definition

  - [`vbus_uuid`](doc_vbus__vbus.md#standardese-vbus_uuid) - 128bit UUID representation

  - [`vbus_uuid_t`](doc_vbus__vbus.md#standardese-vbus_uuid_t) - C - style type definition
