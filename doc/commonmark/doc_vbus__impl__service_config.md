# Header file `vbus/impl/service_config.hpp`

``` cpp
#include "string_array.hpp"

#include "type_helpers.hpp"

namespace vbus
{
    namespace abi
    {
        class response_sender;

        class service;

        template <class Funct>
        class functor_service;

        struct service_metadata;

        class service_config;
    }
}
```
