# Header file `vbus/impl/subcription_handler.hpp`

``` cpp
#include "string_array.hpp"

namespace vbus
{
    namespace abi
    {
        class subscription_handler;

        template <class Funct>
        class functor_subscription_handler;
    }
}
```
