# Header file `vbus/impl/response_handler.hpp`

``` cpp
namespace vbus
{
    namespace abi
    {
        class response_handler;

        template <class Funct>
        class functor_response_handler;
    }
}
```
