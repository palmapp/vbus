mkdir commonmark
mkdir html

del /y html\*.*
del /y commonmark\*.*

cd html
standardese --config ..\doc.ini  --output.format=html ..\..\src\vbus-abi\include

cd ..
cd commonmark

standardese --config ..\doc.ini  --output.format=commonmark ..\..\src\vbus-abi\include

cd..