set(target vbus-client-test)

set(clientTest_Sources
        src/main.cpp
        src/request_response.cpp)

add_executable(${target} ${clientTest_Sources})

set(clientTest_Libs ${Boost_LIBRARIES} commons logging fmt::fmt base64 vbus-lib vbus-client Catch2::Catch2WithMain ${EXE_LIBS})
target_link_libraries(${target} PUBLIC ${clientTest_Libs})


