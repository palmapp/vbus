//
// Created by jhrub on 26.01.2021.
//
#include <catch.hpp>

#include "../../vbus-client/include/vbus/client.h"
#include <commons/guid.h>
#include <vbus/client.h>
#include <vbus/runner.h>
using namespace vbus;

class test_request_service : public service_server
{
 public:
   std::string init(response_sender &sender) override
   {
      sender_ = &sender;
      return "test_request_service";
   }

   void on_request(request_ptr req, client_pin_ptr) override
   {
      auto response = response_allocator_.construct();
      response->request_id = req->metadata.id;
      response->status = static_cast<std::int64_t>(req->metadata.action_id);

      sender_->send_response(std::move(response));
   }

   uint16_t get_bind_port() override
   {
      return 0;
   }

 private:
   response_sender *sender_ = nullptr;
   allocator<response> response_allocator_;
};

class test_message_handler : public message_handler
{
 public:
   void on_subscription_message(multipart_message &message) override {}
   void on_response_received(request_ptr request, std::int64_t status, multipart_message &message) override
   {

      set_status(status);
   }
   void set_status(int64_t status)
   {
      {
         std::unique_lock lk{mx_};
         status_ = status;
      }
      cv_.notify_all();
   }
   void on_request_timed_out(request_ptr request) override
   {
      set_status(-1);
   }

   void on_request_cancelled(request_ptr request) override
   {
      set_status(-1);
   }

   void wait_for_status()
   {
      using namespace std::chrono_literals;
      std::unique_lock lk{mx_};
      cv_.wait_for(lk, 10s, [this]() { return status_ != -2; });
   }
   std::int64_t status_ = -2;
   std::condition_variable cv_;
   std::mutex mx_;
};

TEST_CASE("test request response", "[reqres]")
{

   bus_properties props;
   props.bind_string.protocol = "tcp";
   props.bind_string.host = "127.0.0.1";
   props.bind_string.port = 0;
   props.remote_access_address = props.bind_string.host;

   runner dir{"test", props};

   connection_data client_cfg;
   client_cfg.connection_string = dir.get_bus_connection_string();
   client_cfg.private_ip = client_cfg.public_ip = props.bind_string.host;
   client_cfg.vbus_instance_name = "test";

   std::vector<std::unique_ptr<service_server>> services;
   services.emplace_back(new test_request_service);

   allocator<request> allocator;

   auto client_ptr = client::make_service_server(client_cfg, std::move(services));

   commons::cancellation_token_source cts;
   test_message_handler handler;

   std::jthread bus([&]() { dir.run_forever(cts.get_cancellation_token()); });
   std::jthread client([&]() { client_ptr->run_forever(handler, cts.get_cancellation_token()); });

   service_client &test_service = client_ptr->get_service_client("test_request_service");

   auto request_ptr = allocator.construct();

   request_ptr->metadata.action_id = 10;
   request_ptr->metadata.id = commons::guid::convert_to_std(commons::guid::generate_unsafe_uuid());

   test_service.send_request(std::move(request_ptr));

   handler.wait_for_status();

   cts.cancel();

   REQUIRE(handler.status_ == 10);
}
