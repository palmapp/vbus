//
// Created by jhrub on 26.01.2021.
//
// clang-format off

#include <catch.hpp>
#include <chrono>
#include <commons/make_string.h>
#include <thread>
#include <vbus/vbus.hpp>
#include <atomic>
#include <optional>
#include <condition_variable>
#include <future>
#include <boost/asio.hpp>
#include <easylogging++.h>
// clang-format on

using namespace vbus::abi;
using namespace std::literals;

TEST_CASE("specific ports", "[request]")
{
   auto test_props = R"(
   vbus.instance.name = specific-ports
   vbus.connection.string = tcp://127.0.0.1:15001
   vbus.pubsub.port = 15002
   vbus.router.port = 15003
)"sv;
   directory_service service{test_props};

   auto test_service = make_service(
       [](response_sender &sender, vbus_request_t *req, string_array const &msg)
       {
          std::vector<std::string> result;
          result.reserve(msg.size());

          for (auto view : msg)
          {
             result.push_back(static_cast<std::string>(view));
          }

          sender.send_response(100, result);
       });

   service_config config;
   config.register_service(service_metadata{"test", {}, {}, false, 15004}, test_service);

   client services{test_props, config.config_view()};
   services.run_async();

   client cl{test_props};

   cl.run_async();

   std::vector<std::string> messages = {"one", "two", "three"};

   auto request = cl.create_request("test");
   request->timeout_ms = 200;
   auto response = cl.send_request_sync(std::move(request), messages);

   REQUIRE(response->message.length == messages.size());
   REQUIRE(response->status == 100);

   auto message_view = string_array{response->message};
   REQUIRE(message_view.size() == messages.size());

   REQUIRE(message_view[0] == "one");
   REQUIRE(message_view[1] == "two");
   REQUIRE(message_view[2] == "three");
}
