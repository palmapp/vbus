//
// Created by jhrub on 14.03.2021.
//
#include <catch.hpp>
#include <chrono>
#include <commons/cancellation_token.h>
#include <commons/make_string.h>
#include <condition_variable>
#include <easylogging++.h>
#include <fmt/format.h>
#include <mutex>
#include <thread>
#include <vbus/vbus.hpp>

using namespace vbus::abi;
using namespace std::chrono_literals;
using namespace std::string_literals;

TEST_CASE("test publish subscribe", "[pubsub]")
{
   using namespace std::string_literals;
   directory_service service;
   auto props = service.get_inprocess_connection_props();
   std::mutex m;
   std::condition_variable cv;

   std::vector<std::string> received_message;

   auto handler = make_subscription_handler(
       [&](string_array const &message)
       {
          if (message[0] != "/dir")
          {
             std::unique_lock lk{m};
             {
                std::transform(message.begin(), message.end(), std::back_inserter(received_message),
                               [](std::string_view part) { return static_cast<std::string>(part); });
             }
             cv.notify_one();
          }
       });
   client cl{props};
   cl.set_subscription_handler(handler);

   cl.subscribe("/channel");
   cl.run_async();

   std::vector<std::string> message;
   message.emplace_back("/channel");
   message.emplace_back("msg");

   cl.publish(message, false);

   std::unique_lock lk{m};
   cv.wait_for(lk, 5s, [&]() { return !received_message.empty(); });

   REQUIRE(received_message.size() == 2);
   auto channel = received_message[0];
   REQUIRE(channel == "/channel");
   auto msg = received_message[1];
   REQUIRE(msg == "msg");
}

TEST_CASE("test publish subscribe: sticky", "[pubsub]")
{
   using namespace std::string_literals;

   directory_service service;
   auto props = service.get_inprocess_connection_props();
   std::mutex m;
   std::condition_variable cv;
   bool received = false;
   auto handler = make_subscription_handler(
       [&](string_array const &message)
       {
          if (message[0] != "/dir")
          {
             REQUIRE(message.size() == 2);
             auto channel = message[0];
             REQUIRE(channel == "/channel");
             auto msg = message[1];
             REQUIRE(msg == "msg");

             std::unique_lock lk{m};
             {
                received = true;
             }
             cv.notify_one();
          }
       });

   auto publish_and_wait = [&](bool skip_publish)
   {
      client cl{props};
      cl.set_subscription_handler(handler);

      cl.subscribe("/channel");
      cl.run_async();

      if (!skip_publish)
      {
         std::vector<std::string> message;
         message.emplace_back("/channel");
         message.emplace_back("msg");

         cl.publish(message, true);
      }

      std::unique_lock lk{m};
      cv.wait_for(lk, 5s);
   };

   publish_and_wait(false);
   REQUIRE(received == true);

   received = false;
   publish_and_wait(true);

   REQUIRE(received == true);
}

TEST_CASE("test publish subscribe + service calling", "[pubsub]")
{
   using namespace std::string_literals;
   directory_service service;
   auto props = service.get_inprocess_connection_props();

   auto test_service = make_service([](response_sender &sender, vbus_request_t *req, string_array const &msg)
                                    { sender.send_response(0); });

   service_config config;
   config.register_service({"test"}, test_service);

   client services{props, config.config_view()};
   services.run_async();

   commons::cancellation_token_source cts{};

   std::mutex m;
   std::condition_variable cv;
   std::atomic<int> counter = -1;

   std::thread bg_sender(
       [token = cts.get_cancellation_token(), props, &counter, &cv, &m]()
       {
          client cl{props};
          cl.run_async();

          {
             std::unique_lock lk{m};
             cv.wait(lk, [&]() { return counter == 0; });
          }

          std::vector<std::string> message{"/channel", "test"};

          for (int i = 0; i != 1000; ++i)
          {
             if (!cl.publish(message, false))
             {
                break;
             }
             ++counter;
             // token.wait_for(1ms);
          }
       });

   int requests = 0;

   auto handler = make_subscription_handler(
       [&](string_array const &message)
       {
          if (message[0] != "/dir")
          {
             std::unique_lock lk{m};
             ++counter;

             if (counter == 1000) cv.notify_one();
          }
       });

   client cl{props};
   cl.set_subscription_handler(handler);

   cl.subscribe("/channel");
   cl.run_async();

   cl.set_default_request_timeout(1000);

   {
      std::unique_lock lk{m};
      counter = 0;
   }
   cv.notify_one();

   for (; requests != 100; ++requests)
   {
      LOG(DEBUG) << "sending";
      auto request = cl.create_request("test");
      auto response = cl.send_request_sync(std::move(request));

      try
      {

         REQUIRE(response->status == 0);
      }
      catch (...)
      {
         LOG(DEBUG) << "failed timestamp";
         throw;
      }
   }

   {
      std::unique_lock lk{m};
      cv.wait(lk, [&]() { return counter > 0; });

      REQUIRE(requests == 100);
      REQUIRE(counter > 0);
   }

   cts.cancel();
   if (bg_sender.joinable()) bg_sender.join();
}

TEST_CASE("test publish subscribe + service calling > no subscriber", "[pubsub]")
{
   using namespace std::string_literals;
   directory_service service;
   auto props = service.get_inprocess_connection_props();

   auto test_service = make_service([](response_sender &sender, vbus_request_t *req, string_array const &msg)
                                    { sender.send_response(0); });

   service_config config;
   config.register_service({"test"}, test_service);

   client services{fmt::format("{}\nvbus.client.name=test-srv\n", props), config.config_view()};
   services.run_async();

   commons::cancellation_token_source cts{};

   std::mutex m;
   std::condition_variable cv;
   std::atomic<int> counter = -1;

   std::thread bg_sender(
       [token = cts.get_cancellation_token(), props, &counter, &cv, &m]()
       {
          try
          {
             client cl{fmt::format("{}\nvbus.client.name=bg_sender\n", props)};
             cl.run_async();

             {
                std::unique_lock lk{m};
                cv.wait(lk, [&]() { return counter == 0; });
             }
             LOG(INFO) << "bg_sender active";

             std::vector<std::string> message{"/channel", "test"};

             for (int i = 0; i != 1000; ++i)
             {
                if (!cl.publish(message, false))
                {
                   LOG(WARNING) << "publish returned false";
                   break;
                }

                ++counter;
                // token.wait_for(1ms);
             }
          }
          catch (std::exception const &exception)
          {
             LOG(ERROR) << "exception " << exception.what();
          }
          catch (...)
          {
             LOG(ERROR) << "unknown exception";
          }

          LOG(INFO) << "bg_sender thread exited with counter = " << counter;
       });

   int requests = 0;

   client cl{fmt::format("{}\nvbus.client.name=main\n", props)};
   cl.run_async();

   cl.set_default_request_timeout(10000);

   {
      std::unique_lock lk{m};
      counter = 0;
   }
   cv.notify_one();

   for (; requests != 100; ++requests)
   {
      LOG(DEBUG) << "sending";
      auto request = cl.create_request("test");
      auto response = cl.send_request_sync(std::move(request));

      try
      {

         REQUIRE(response->status == 0);
      }
      catch (...)
      {
         LOG(DEBUG) << "failed timestamp";
         throw;
      }
   }

   REQUIRE(requests == 100);

   cts.cancel();
   if (bg_sender.joinable()) bg_sender.join();
}

TEST_CASE("repeated initialization - to see if there is a problem with service call", "[pubsub]")
{
   using namespace std::string_literals;

   directory_service service;
   auto props = service.get_inprocess_connection_props();

   auto test_service = make_service([](response_sender &sender, vbus_request_t *req, string_array const &msg)
                                    { sender.send_response(0); });

   service_config config;
   config.register_service({"test"}, test_service);

   client services{fmt::format("{}\nvbus.client.name=test-srv\n", props), config.config_view()};
   services.run_async();

   for (int r = 0; r != 100; ++r)
   {
      LOG(INFO) << "repeated initialization begin : " << r;

      std::mutex m;
      std::condition_variable cv;
      std::atomic<int> counter = -1;

      std::thread bg_sender(
          [props, &counter, &cv, &m]()
          {
             try
             {
                client cl{fmt::format("{}\nvbus.client.name=bg_sender\n", props)};
                cl.run_async();

                {
                   std::unique_lock lk{m};
                   cv.wait(lk, [&]() { return counter == 0; });
                }
             }
             catch (std::exception const &exception)
             {
                LOG(ERROR) << "exception " << exception.what();
             }
             catch (...)
             {
                LOG(ERROR) << "unknown exception";
             }

             LOG(INFO) << "bg_sender thread exited with counter = " << counter;
          });

      int requests = 0;

      client cl{fmt::format("{}\nvbus.client.name=main\n", props)};
      cl.run_async();

      cl.set_default_request_timeout(10000);

      for (; requests != 100; ++requests)
      {
         LOG(DEBUG) << "sending";
         auto request = cl.create_request("test");
         auto response = cl.send_request_sync(std::move(request));

         try
         {

            REQUIRE(response->status == 0);
         }
         catch (...)
         {
            LOG(DEBUG) << "failed timestamp";
            throw;
         }
      }

      REQUIRE(requests == 100);

      {
         std::unique_lock lk{m};
         counter = 0;
      }
      cv.notify_one();

      if (bg_sender.joinable()) bg_sender.join();
   }
}

TEST_CASE("test publish subscribe + service calling: initialization blocking", "[pubsub]")
{
   using namespace std::string_literals;
   directory_service service;
   auto props = service.get_inprocess_connection_props();

   auto test_service = make_service([](response_sender &sender, vbus_request_t *req, string_array const &msg)
                                    { sender.send_response(0); });

   service_config config;
   config.register_service({"test"}, test_service);

   client services{props, config.config_view()};
   services.run_async();

   std::atomic<int> counter{}, requests{}, failed{};

   std::thread bg_sender(
       [props, &counter]()
       {
          client cl{props};
          cl.run_async();

          std::vector<std::string> message{"/channel", "test"};

          while (true)
          {
             for (int i = 0; i != 1000; ++i)
             {
                if (!cl.publish(message, false))
                {
                   break;
                }
                // token.wait_for(1ms);
             }
             LOG(INFO) << "here";

             if (counter > 0)
             {
                break;
             }
          }
       });

   auto handler = make_subscription_handler(
       [&](string_array const &message)
       {
          if (message[0] != "/dir")
          {
             ++counter;
          }
       });

   client cl{props};
   cl.set_subscription_handler(handler);

   cl.subscribe("/channel");
   cl.run_async();

   cl.set_default_request_timeout(100);

   auto begin = std::chrono::steady_clock::now();

   while (true)
   {
      auto request = cl.create_request("test");
      auto response = cl.send_request_sync(std::move(request));

      if (response->status == 0)
      {
         ++requests;

         if (counter > 0 || requests > 1000) break;
      }
      else
      {
         if (++failed > 10)
         {
            break;
         }
      }
   }
   auto diff = std::chrono::steady_clock::now() - begin;

   if (bg_sender.joinable()) bg_sender.join();


   REQUIRE(failed < 10);
   REQUIRE(counter > 0);
   REQUIRE(diff < std::chrono::seconds{1});
}