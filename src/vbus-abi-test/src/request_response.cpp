//
// Created by jhrub on 26.01.2021.
//
// clang-format off

#include <catch.hpp>
#include <chrono>
#include <commons/make_string.h>
#include <thread>
#include <vbus/vbus.hpp>
#include <atomic>
#include <optional>
#include <condition_variable>
#include <future>
#include <boost/asio.hpp>
#include <easylogging++.h>
// clang-format on

using namespace vbus::abi;
using namespace std::chrono_literals;

TEST_CASE("test request response", "[request]")
{
   directory_service service;

   auto props = service.get_inprocess_connection_props();
   auto test_service = make_service(
      [](response_sender &sender, vbus_request_t *req, string_array const &msg)
      {
         std::vector<std::string> result;
         result.reserve(msg.size());

         for (auto view : msg)
         {
            result.push_back(static_cast<std::string>(view));
         }

         sender.send_response(100, result);
      });

   service_config config;
   config.register_service({"test"}, test_service);

   client services{props, config.config_view()};
   services.run_async();

   client cl{props};
   cl.run_async();

   std::vector<std::string> messages = {"one", "two", "three"};

   auto request = cl.create_request("test");
   auto response = cl.send_request_sync(std::move(request), messages);

   REQUIRE(response->message.length == messages.size());
   REQUIRE(response->status == 100);

   auto message_view = string_array{response->message};
   REQUIRE(message_view.size() == messages.size());

   REQUIRE(message_view[0] == "one");
   REQUIRE(message_view[1] == "two");
   REQUIRE(message_view[2] == "three");
}

TEST_CASE("test timeout : no vbus", "[request]")
{
   using namespace std::string_literals;

   auto props = "vbus.instance.name = inproc-dir\nvbus.connection.string=tcp://invalid"s;

   client cl{props};
   cl.run_async();

   auto request = cl.create_request("test");
   request->timeout_ms = 1;

   auto response = cl.send_request_sync(std::move(request));

   REQUIRE(response->status == -503);
}

TEST_CASE("test timeout : no service with the name", "[request]")
{
   using namespace std::string_literals;

   directory_service service;

   auto props = service.get_inprocess_connection_props();

   client cl{props};
   cl.set_default_request_timeout(1000);
   cl.run_async();

   auto request = cl.create_request("test");
   auto response = cl.send_request_sync(std::move(request));

   REQUIRE(response->status == -503);
}

TEST_CASE("test timeout : slow service", "[request]")
{
   using namespace std::string_literals;

   directory_service service;

   auto props = service.get_inprocess_connection_props();

   auto test_service = make_service(
      [](response_sender &sender, vbus_request_t *req, string_array const &msg)
      {
         std::this_thread::sleep_for(2s);
         sender.send_response(100);
      });

   service_config config;
   config.register_service({"test"}, test_service);

   client services{props, config.config_view()};
   services.run_async();

   client cl{props};
   cl.set_default_request_timeout(1000);
   cl.run_async();

   auto request = cl.create_request("test");
   auto response = cl.send_request_sync(std::move(request));

   REQUIRE(response->status == -503);
   std::this_thread::sleep_for(3s);
}

TEST_CASE("out of order initialization", "[initialization][request]")
{
   using namespace std::string_literals;
   std::string connection_string = "tcp://127.0.0.1:10501";

   auto props = commons::make_string("vbus.instance.name = inproc-dir\nvbus.connection.string=", connection_string);

   client cl{props};
   cl.set_default_request_timeout(10000);
   cl.run_async();

   auto test_service = make_service([](response_sender &sender, vbus_request_t *req, string_array const &msg)
   {
      sender.send_response(100);
   });

   service_config config;
   config.register_service({"test"}, test_service);

   client services{props, config.config_view()};
   services.run_async();

   std::this_thread::sleep_for(1s);
   directory_service service{props};

   auto request = cl.create_request("test");
   auto response = cl.send_request_sync(std::move(request));

   REQUIRE(response->status == 100);
}

TEST_CASE("out of order initialization: sending call to service before is registered", "[initialization][request]")
{
   using namespace std::string_literals;
   std::string connection_string = "tcp://127.0.0.1:10501";

   auto props = commons::make_string("vbus.instance.name = inproc-dir\nvbus.connection.string=", connection_string);
   directory_service service{props};

   client cl{props};
   cl.set_default_request_timeout(10000);
   cl.run_async();

   std::mutex lock;
   std::condition_variable cv;
   auto response_future = std::async(std::launch::async,
                                     [&]()
                                     {
                                        auto request = cl.create_request("test");
                                        cv.notify_one();

                                        auto response = cl.send_request_sync(std::move(request));

                                        return response->status == 100;
                                     });

   std::unique_lock lk{lock};
   REQUIRE(cv.wait_for(lk, 5s) == std::cv_status::no_timeout);
   std::this_thread::sleep_for(1s);

   auto test_service = make_service([](response_sender &sender, vbus_request_t *req, string_array const &msg)
   {
      sender.send_response(100);
   });

   service_config config;
   config.register_service({"test"}, test_service);

   client services{props, config.config_view()};
   services.run_async();

   std::this_thread::sleep_for(1s);

   REQUIRE(response_future.get());
}

TEST_CASE("service reconnection: requesting service which is restarted after 5s", "[request][restart]")
{
   using namespace std::string_literals;
   std::string connection_string = "tcp://127.0.0.1:10501";

   auto props = commons::make_string("vbus.instance.name = inproc-dir\nvbus.connection.string=", connection_string);
   directory_service service{props};

   client cl{props};
   cl.set_default_request_timeout(10000);
   cl.run_async();

   std::atomic<bool> done = false;
   auto service_thread_test =
      std::thread{[&]() mutable
      {
         auto test_service = make_service([](response_sender &sender, vbus_request_t *req,
                                             string_array const &msg)
         {
            sender.send_response(100);
         });

         {
            bool first = true;
            while (!done)
            {
               if (first)
               {

                  first = false;
               }
               else
               {
                  std::this_thread::sleep_for(1s);
               }

               service_config config;
               config.register_service({"test"}, test_service);

               client services{props, config.config_view()};
               services.run_async();
               std::this_thread::sleep_for(3s);
            }
         }
      }};

   auto service_thread_test_2 =
      std::thread{[&]() mutable
      {
         auto test_service = make_service([](response_sender &sender, vbus_request_t *req,
                                             string_array const &msg)
         {
            sender.send_response(200);
         });

         {
            bool first = true;

            while (!done)
            {
               if (first)
               {

                  first = false;
               }
               else
               {
                  std::this_thread::sleep_for(1s);
               }

               service_config config;
               config.register_service({"test2"}, test_service);

               client services{props, config.config_view()};
               services.run_async();
               std::this_thread::sleep_for(5s);
            }
         }
      }};

   auto started = std::chrono::steady_clock::now();

   std::unordered_map<int64_t, int> test_responses;
   std::unordered_map<int64_t, int> test2_responses;
   while (std::chrono::steady_clock::now() - started < 30s)
   {
      for (int i = 0, len = 100; i != len; ++i)
      {
         std::cout << "r:" << i << std::endl;
         auto request = cl.create_request("test");
         auto response = cl.send_request_sync(std::move(request));

         ++test_responses[response->status];

         auto request_2 = cl.create_request("test2");
         auto response_2 = cl.send_request_sync(std::move(request_2));

         ++test2_responses[response_2->status];
      }
   }

   done = true;

   for (auto [status, count] : test_responses)
   {
      std::cout << status << " = " << count << std::endl;
      REQUIRE(status != 0);
   }

   for (auto [status, count] : test2_responses)
   {
      std::cout << status << " = " << count << std::endl;
      REQUIRE(status != 0);
   }

   if (service_thread_test.joinable()) service_thread_test.join();
   if (service_thread_test_2.joinable()) service_thread_test_2.join();
}

TEST_CASE("service reconnection async: requesting service which is restarted after 5s", "[request][restart]")
{
   using namespace std::string_literals;
   std::string connection_string = "tcp://127.0.0.1:10501";

   auto props = commons::make_string("vbus.instance.name = inproc-dir\nvbus.connection.string=", connection_string);
   directory_service service{props};

   client cl{props};
   cl.set_default_request_timeout(10000);
   cl.run_async();

   std::atomic<bool> done = false;
   auto service_thread_test =
      std::thread{[&]() mutable
      {
         auto test_service = make_service([](response_sender &sender, vbus_request_t *req,
                                             string_array const &msg)
         {
            sender.send_response(100);
         });

         {
            bool first = true;
            while (!done)
            {
               if (first)
               {

                  first = false;
               }
               else
               {
                  std::this_thread::sleep_for(1s);
               }

               service_config config;
               config.register_service({"test"}, test_service);

               client services{props, config.config_view()};
               services.run_async();
               std::this_thread::sleep_for(3s);
            }
         }
      }};

   auto service_thread_test_2 =
      std::thread{[&]() mutable
      {
         auto test_service = make_service([](response_sender &sender, vbus_request_t *req,
                                             string_array const &msg)
         {
            sender.send_response(200);
         });

         {
            bool first = true;

            while (!done)
            {
               if (first)
               {

                  first = false;
               }
               else
               {
                  std::this_thread::sleep_for(1s);
               }

               service_config config;
               config.register_service({"test2"}, test_service);

               client services{props, config.config_view()};
               services.run_async();
               std::this_thread::sleep_for(5s);
            }
         }
      }};

   auto started = std::chrono::steady_clock::now();
   struct async_handler
   {
      static void on_receive(vbus_request_t *request, vbus_response_t *response)
      {
         static_cast<async_handler *>(request->user_data)->on_receive_internal(response);
      }

      void on_receive_internal(vbus_response_t *response)
      {
         ++test_responses[response->status];
         ++received;
      }

      std::unordered_map<int64_t, int> test_responses;
      std::atomic<int64_t> received = 0;
   };

   async_handler test;

   int64_t sent = 0;
   while (std::chrono::steady_clock::now() - started < 30s)
   {
      for (int i = 0, len = 100; i != len; ++i)
      {
         std::cout << "r:" << i << std::endl;
         auto request = cl.create_request("test");
         request->user_data = &test;
         request->request_callback = &async_handler::on_receive;

         cl.send_request(std::move(request));
         ++sent;
      }
      std::this_thread::sleep_for(1s);
   }

   while (sent != test.received.load())
   {
      std::this_thread::sleep_for(1s);
   }

   done = true;

   for (auto [status, count] : test.test_responses)
   {
      std::cout << status << " = " << count << std::endl;
      REQUIRE(status != 0);
      REQUIRE(count != 0);
   }

   if (service_thread_test.joinable()) service_thread_test.join();
   if (service_thread_test_2.joinable()) service_thread_test_2.join();
}

TEST_CASE("sync requests benchmark threading", "[threads][benchmark]")
{
   using namespace std::string_literals;

   directory_service service;

   auto props = service.get_inprocess_connection_props();
   auto num = std::atomic<int64_t>{0};
   auto test_service = make_service([&num](response_sender &sender, vbus_request_t *req, string_array const &msg)
   {
      sender.send_response(++num);
   });

   service_config config;
   config.register_service({"test"}, test_service).use_thread_pool(4);

   client services{props, config.config_view()};
   services.run_async();

   client cl{props};
   cl.set_default_request_timeout(10000);
   cl.run_async();

   std::deque<std::thread> threads_;

   std::atomic<int> req{0}, res{0}, stat{0}, ex{0};

   const auto t_count = 10;
   const auto i_count = 1000;
   for (int t = 0; t < t_count; ++t)
   {

      threads_.emplace_back(
         [&, t]()
         {
            int status_errors = 0, response_errors = 0, request_errors = 0, exception_errors = 0;

            try
            {
               for (int i = 0; i < i_count; ++i)
               {
                  auto request = cl.create_request("test");
                  if (request == nullptr)
                  {
                     ++request_errors;
                     continue;
                  }

                  auto response = cl.send_request_sync(std::move(request));
                  if (response == nullptr)
                  {
                     ++response_errors;
                     continue;
                  }

                  auto status = response->status;
                  if (status <= 0)
                  {
                     ++status_errors;
                  }
               }
            }
            catch (...)
            {
               ++exception_errors;
            }

            req += request_errors;
            res += response_errors;
            stat += status_errors;
            ex += exception_errors;
         });
   }

   for (auto &t : threads_)
   {
      if (t.joinable()) t.join();
   }

   auto result = num.load();
   auto expected_total = t_count * i_count;

   REQUIRE(result == expected_total);
   REQUIRE(req == 0);
   REQUIRE(res == 0);
   REQUIRE(stat == 0);
   REQUIRE(ex == 0);
}

TEST_CASE("sync request benchmark", "[benchmark]")
{
   using namespace std::string_literals;

   directory_service service;

   auto props = service.get_inprocess_connection_props();

   auto test_service = make_service([](response_sender &sender, vbus_request_t *req, string_array const &msg)
   {
      sender.send_response(100);
   });

   service_config config;
   config.register_service({"test"}, test_service);

   client services{props, config.config_view()};
   services.run_async();

   client cl{props};
   cl.set_default_request_timeout(10000);
   cl.run_async();

   for (int i = 0; i < 10000; ++i)
   {
      auto request = cl.create_request("test");
      auto response = cl.send_request_sync(std::move(request));

      REQUIRE(response->status == 100);
   }
}

TEST_CASE("sync requests benchmark threading: with small messages", "[messages][threads][benchmark]")
{
   using namespace std::string_literals;

   directory_service service;

   auto props = service.get_inprocess_connection_props();
   auto num = std::atomic<int64_t>{0};

   std::atomic<int> service_msg_size_errors{0}, service_part_size_errors{0}, service_content_errors{0};
   auto test_service = make_service(
      [&](response_sender &sender, vbus_request_t *req, string_array const &msg)
      {
         if (msg.size() != 4)
         {
            ++service_msg_size_errors;
         }

         for (auto part : msg)
         {
            if (part.size() <= 0)
            {
               ++service_part_size_errors;
            }
         }
         std::vector<std::string> response;
         response.reserve(msg.size() + 1);

         if (msg[0] != "first" || msg[1] != "second" || msg[2] != "third")
         {
            ++service_content_errors;
         }

         for (auto part : msg)
         {
            response.push_back(static_cast<std::string>(part));
         }
         auto result = ++num;
         response.push_back(std::to_string(result));

         sender.send_response(result, response);
      });

   service_config config;
   config.register_service({"test"}, test_service).use_thread_pool(4);

   client services{props, config.config_view()};
   services.run_async();

   client cl{props};
   cl.set_default_request_timeout(10000);
   cl.run_async();

   std::deque<std::thread> threads_;
   std::atomic<int> req{0}, res{0}, stat{0}, ex{0}, msg{0}, empty{0}, content{0};
   const auto t_count = 10;
   const auto i_count = 1000;
   for (int t = 0; t < t_count; ++t)
   {
      threads_.emplace_back(
         [&, t]()
         {
            int status_errors = 0, response_errors = 0, request_errors = 0, exception_errors = 0, msg_size_errors = 0,
                empty_parts_errors = 0, content_errors = 0;

            try
            {

               std::vector<std::string> request_msg;
               request_msg.emplace_back("first");
               request_msg.emplace_back("second");
               request_msg.emplace_back("third");
               request_msg.emplace_back();

               for (int i = 0; i < i_count; ++i)
               {
                  auto request = cl.create_request("test");
                  if (request == nullptr)
                  {
                     ++request_errors;
                     continue;
                  }

                  request_msg.back() = commons::make_string(t, ":", i);

                  auto response = cl.send_request_sync(std::move(request), request_msg);
                  if (response == nullptr)
                  {
                     ++response_errors;
                     continue;
                  }

                  auto status = response->status;
                  if (status <= 0)
                  {
                     ++status_errors;
                  }

                  auto response_message = string_array{response->message};
                  if (response_message.size() != request_msg.size() + 1)
                  {
                     ++msg_size_errors;
                  }

                  for (auto part : response_message)
                  {
                     if (part.size() == 0)
                     {
                        ++empty_parts_errors;
                     }
                  }

                  if (response_message[response_message.size() - 1] != std::to_string(status))
                  {
                     ++content_errors;
                  }
               }
            }
            catch (...)
            {
               ++exception_errors;
            }

            req += request_errors;
            res += response_errors;
            stat += status_errors;
            ex += exception_errors;
            msg += msg_size_errors;
            empty += empty_parts_errors;
            content += content_errors;
         });
   }

   for (auto &t : threads_)
   {
      if (t.joinable()) t.join();
   }

   auto result = num.load();
   auto expected_total = t_count * i_count;

   REQUIRE(result == expected_total);
   REQUIRE(req == 0);
   REQUIRE(res == 0);
   REQUIRE(stat == 0);
   REQUIRE(ex == 0);

   REQUIRE(msg == 0);
   REQUIRE(empty == 0);
   REQUIRE(content == 0);

   REQUIRE(service_msg_size_errors == 0);
   REQUIRE(service_part_size_errors == 0);
   REQUIRE(service_content_errors == 0);
}

TEST_CASE("test request response: large message test", "[request][messages]")
{
   directory_service service;

   auto props = service.get_inprocess_connection_props();
   auto test_service = make_service(
      [](response_sender &sender, vbus_request_t *req, string_array const &msg)
      {
         std::vector<std::string> result;
         result.reserve(msg.size());

         for (auto view : msg)
         {
            result.push_back(static_cast<std::string>(view));
         }

         sender.send_response(100, result);
      });

   service_config config;
   config.register_service({"test"}, test_service);

   client services{props, config.config_view()};
   services.run_async();

   client cl{props};
   cl.run_async();

   std::vector<std::string> messages;
   messages.resize(3);

   std::fill_n(std::back_inserter(messages[0]), 0xFFFF, '0');
   std::fill_n(std::back_inserter(messages[1]), 0xFFFFF, '1');
   std::fill_n(std::back_inserter(messages[2]), 0xFFFFFF, '2');

   auto request = cl.create_request("test");
   auto response = cl.send_request_sync(std::move(request), messages);

   REQUIRE(response->message.length == messages.size());
   REQUIRE(response->status == 100);

   auto message_view = string_array{response->message};
   REQUIRE(message_view.size() == messages.size());

   const bool part_0 = message_view[0] == messages[0];
   REQUIRE(part_0 == true);

   const bool part_1 = message_view[1] == messages[1];
   REQUIRE(part_1 == true);

   const bool part_2 = message_view[2] == messages[2];
   REQUIRE(part_2 == true);
}

TEST_CASE("test directory status", "[directory]")
{
   directory_service service;

   auto props = service.get_inprocess_connection_props();
   auto test_service = make_service(
      [](response_sender &sender, vbus_request_t *req, string_array const &msg)
      {
         std::vector<std::string> result;
         result.reserve(msg.size());

         for (auto view : msg)
         {
            result.push_back(static_cast<std::string>(view));
         }

         sender.send_response(100, result);
      });

   service_config config;
   config.register_service({"test-a", "interface-a", "metadata-a"}, test_service);
   config.register_service({"test-b", "interface-b", "metadata-b"}, test_service);

   client services{props, config.config_view()};
   services.run_async();

   client cl{props};

   directory_status_ptr status;

   auto status_handler =
      make_directory_status_handler([&](directory_status_ptr ptr) mutable
      {
         status = std::move(ptr);
      });

   cl.set_directory_status_handler(status_handler);
   cl.run_async();

   std::this_thread::sleep_for(2s);

   REQUIRE(status != nullptr);
   REQUIRE(status->length == 2);
   for (auto dir_status : directory_status_iterator{*status})
   {
      REQUIRE(dir_status.status.is_online == true);
      REQUIRE(dir_status.metadata.id.length == 6);
      REQUIRE(dir_status.metadata.id.data != nullptr);
      REQUIRE(dir_status.metadata.interface.length == 11);
      REQUIRE(dir_status.metadata.interface.data != nullptr);
      REQUIRE(dir_status.metadata.data.length == 10);
      REQUIRE(dir_status.metadata.data.data != nullptr);
   }

   directory_status_iterator wrapper{*status};
   REQUIRE(wrapper.size() == 2);

   auto a = wrapper[0];
   REQUIRE(to_std_string_view(a.metadata.id) == "test-a");
   REQUIRE(to_std_string_view(a.metadata.interface) == "interface-a");
   REQUIRE(to_std_string_view(a.metadata.data) == "metadata-a");

   auto b = wrapper[1];
   REQUIRE(to_std_string_view(b.metadata.id) == "test-b");
   REQUIRE(to_std_string_view(b.metadata.interface) == "interface-b");
   REQUIRE(to_std_string_view(b.metadata.data) == "metadata-b");
}

TEST_CASE("out of order destroy: sending  service response after client being freed", "[destruction][request]")
{
   using namespace std::string_literals;
   std::string connection_string = "tcp://127.0.0.1:10501";

   auto props = commons::make_string("vbus.instance.name = inproc-dir\nvbus.connection.string=", connection_string);
   directory_service service{props};
   client cl{props};
   cl.set_default_request_timeout(10000);
   cl.run_async();

   auto response_future = std::async(std::launch::async,
                                     [&]()
                                     {
                                        auto request = cl.create_request("test");
                                        auto response = cl.send_request_sync(std::move(request));

                                        return response->status == -503;
                                     });

   std::mutex lock;
   std::condition_variable cv;
   int status = 0;
   boost::asio::thread_pool tp{1};

   auto test_service = make_service(
      [&](response_sender &sender, vbus_request_t *req, string_array const &msg)
      {
         boost::asio::post(tp,
                           [resp_sender = std::move(sender), &status, &cv, &lock]() mutable
                           {
                              {
                                 std::unique_lock lk{lock};
                                 status = 1;
                              }
                              cv.notify_one();
                              {
                                 std::unique_lock lk{lock};
                                 cv.wait(lk, [&]()
                                 {
                                    return status == 2;
                                 });
                              }
                              resp_sender.send_response(100);
                           });
      });

   {
      service_config config;
      config.register_service({"test"}, test_service);

      client services{props, config.config_view()};
      services.run_async();

      std::unique_lock lk{lock};
      bool result = cv.wait_for(lk, 10s, [&]()
      {
         return status == 1;
      });
      REQUIRE(result);

      // services are going to be destroyed, but the task is executed on thread pool so it should crash
   }

   {
      std::unique_lock lk{lock};
      status = 2;
   }

   cv.notify_one();

   REQUIRE(response_future.get());
}

TEST_CASE("async responses: many clients", "[request]")
{

   directory_service service;
   auto props = service.get_inprocess_connection_props();

   struct request_context : public response_handler
   {
      void on_response(vbus_request_t &request, vbus_response_t &response) noexcept
      {
         ++slot_;
      }

      std::atomic<int> slot_ = 0;
   };
   std::array<request_context, 20> responses;
   std::deque<client> clients;
   for (std::size_t c = 0; c != responses.size(); ++c)
   {
      clients.push_back(client{props});
      clients.back().run_async();
   }

   for (std::size_t c = 0; c != responses.size(); ++c)
   {
      auto &client = clients[c];

      for (int r = 0; r != 100; ++r)
      {
         auto request = client.create_request("test", responses[c]);
         client.send_request(std::move(request), std::string{"ahoj"});
      }
   }

   auto test_service = make_service(
      [](response_sender &sender, vbus_request_t *req, string_array const &msg)
      {
         std::vector<std::string> result;
         result.reserve(msg.size());

         for (auto view : msg)
         {
            result.push_back(static_cast<std::string>(view));
         }

         sender.send_response(0, result);
      });

   service_config config;
   config.register_service({"test", "interface-a", "metadata-a"}, test_service);

   client services{props, config.config_view()};
   services.run_async();

   int i = 0;
   while (true)
   {
      if (std::all_of(std::begin(responses), std::end(responses),
                      [](request_context const &r)
                      {
                         return r.slot_ == 100;
                      }))
      {
         break;
      }
      else
      {
         if (++i > 20)
         {
            break;
         }
         std::this_thread::sleep_for(1s);
      }
   }

   REQUIRE(std::all_of(std::begin(responses), std::end(responses),
      [](request_context const &r) { return r.slot_ == 100; }) == true);
}

TEST_CASE("sync request jam", "[jam]")
{
   using namespace std::string_literals;

   directory_service service;

   auto props = service.get_inprocess_connection_props();

   std::mutex test_service_thread_wait_for_exit{};
   std::condition_variable test_service_thread_exit{};
   auto test_service_thread = std::thread(
      [&]()
      {
         std::mutex m{};
         std::condition_variable cv{};
         bool exit = false;
         auto test_service = make_service(
            [&](response_sender &sender, vbus_request_t *req, string_array const &msg)
            {
               sender.send_response(100);
               {
                  std::lock_guard lk{m};
                  exit = true;
               }
               cv.notify_one();
            });

         service_config config;
         config.register_service({"test"}, test_service);
         client services{props, config.config_view()};
         services.run_async();

         std::unique_lock lk{m};
         cv.wait(lk, [&]()
         {
            return exit;
         });

         std::unique_lock exit_lock{test_service_thread_wait_for_exit};
         test_service_thread_exit.wait(exit_lock);
         std::this_thread::sleep_for(10ms);
      });

   client cl{props};
   cl.run_async();

   {
      auto request = cl.create_request("test");
      auto response = cl.send_request_sync(std::move(request));
      REQUIRE(response->status == 100);
   }
   test_service_thread_exit.notify_one();
   test_service_thread.join();

   cl.set_default_request_timeout(10);

   for (int i = 0; i < 10000; ++i)
   {
      auto request = cl.create_request("test");
      // I am not waiting for response here
      cl.send_request(std::move(request));
   }

   /// ensure that we timeout everything
   std::this_thread::sleep_for(100ms);
   std::cout << "10000 requests sent" << std::endl;

   // now I am going to boot the service up
   auto test_service = make_service(
      [&](response_sender &sender, vbus_request_t *req, string_array const &msg)
      {
         LOG(INFO) << "in test service: " << req->action_id << "seq: " << req->sequence_number << std::endl;
         sender.send_response(100);
      });

   service_config config;
   config.register_service({"test"}, test_service);
   client services{props, config.config_view()};
   services.run_async();
   LOG(INFO) << "test service up" << std::endl;

   std::this_thread::sleep_for(1s);

   {

      LOG(INFO) << "trying  to send request " << std::endl;
      cl.set_default_request_timeout(10000);

      auto request = cl.create_request("test");
      request->action_id = 1;

      auto response = cl.send_request_sync(std::move(request));
      REQUIRE(response->status == 100);
   }
}