//
// Created by jhrub on 30.07.2022.
//

#include "catch.hpp"
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <thread>
#include <vbus/vbus.hpp>
using namespace std::string_view_literals;

TEST_CASE("cluster-test publish subscribe", "[cluster]")
{
   vbus::abi::directory_service service_1{R"(
vbus.connection.string=tcp://127.0.0.1:23001
vbus.cluster.file=tcp://127.0.0.1:23001;tcp://127.0.0.1:23002
vbus.tracing = 1

)"sv};

   vbus::abi::directory_service service_2{R"(
vbus.connection.string=tcp://127.0.0.1:23002
vbus.cluster.file=tcp://127.0.0.1:23001;tcp://127.0.0.1:23002
vbus.tracing = 1
)"sv};

   std::mutex m;
   std::condition_variable cv;

   int counter = 0;

   auto handler = vbus::abi::make_subscription_handler(
      [&](vbus::abi::string_array const &message)
      {
         if (message[0] == "/test")
         {
            {
               std::unique_lock lk{m};
               ++counter;
            }
            cv.notify_one();
         }
      });

   vbus::abi::client cl_1{R"(
vbus.connection.string=tcp://127.0.0.1:23001
)"sv};

   cl_1.subscribe("/test");
   cl_1.set_subscription_handler(handler);

   vbus::abi::client cl_2{R"(
vbus.connection.string=tcp://127.0.0.1:23002

)"sv};
   cl_2.subscribe("/test");
   cl_2.set_subscription_handler(handler);

   cl_1.run_async();
   cl_2.run_async();

   std::this_thread::sleep_for(std::chrono::seconds{1});

   std::array msg = {"/test"sv, "message"sv};
   cl_1.publish(msg, false);

   std::unique_lock lock{m};
   cv.wait_for(lock, std::chrono::seconds{10}, [&]()
   {
      return counter == 2;
   });

   REQUIRE(counter == 2);
}

TEST_CASE("cluster-test service request/response", "[cluster]")
{
   using namespace vbus::abi;
   directory_service service_1{R"(
vbus.connection.string=tcp://127.0.0.1:23001
vbus.cluster.file=tcp://127.0.0.1:23001;tcp://127.0.0.1:23002
vbus.tracing = 1

)"sv};

   directory_service service_2{R"(
vbus.connection.string=tcp://127.0.0.1:23002
vbus.cluster.file=tcp://127.0.0.1:23001;tcp://127.0.0.1:23002
vbus.tracing = 1


)"sv};

   auto test_service = make_service([](response_sender &sender, vbus_request_t *req, string_array const &msg)
   {
      sender.send_response(100);
   });

   service_config config;
   config.register_service({"test", {}, {}, true}, test_service);

   client services{R"(
vbus.connection.string=tcp://127.0.0.1:23001
)"sv,
                   config.config_view()};

   vbus::abi::client cl_2{R"(
vbus.connection.string=tcp://127.0.0.1:23002

)"sv};

   services.run_async();
   cl_2.run_async();

   auto request = cl_2.create_request("test");
   auto response = cl_2.send_request_sync(std::move(request));

   REQUIRE(response->status == 100);
}

TEST_CASE("cluster-test service often reconnects", "[cluster]")
{
   int failed = 0;
   int total = 0;
   {
      using namespace vbus::abi;
      using namespace std::literals;
      directory_service service_1{R"(
vbus.connection.string=tcp://127.0.0.1:23001
vbus.cluster.file=tcp://127.0.0.1:23001;tcp://127.0.0.1:23002
vbus.tracing = 1

)"sv};

      directory_service service_2{R"(
vbus.connection.string=tcp://127.0.0.1:23002
vbus.cluster.file=tcp://127.0.0.1:23001;tcp://127.0.0.1:23002
vbus.tracing = 1


)"sv};

      auto make_test_service = [](std::string name)
      {

         auto test_service =
            make_shared_service([](response_sender &sender, vbus_request_t *req,
                                   string_array const &msg)
            {
               sender.send_response(100);
            });

         service_config config;
         config.register_service({name, {}, {}, true}, *test_service);

         client services{R"(
vbus.connection.string=tcp://127.0.0.1:23001
)"sv,
                         config.config_view()};

         services.run_async();
         return std::make_pair(std::move(test_service), std::move(services));
      };

      auto make_restarting_service_thread = [&](std::string name)
      {
         return std::jthread{[&, name](std::stop_token token)
         {
            // this connects to service_1 vbus
            while (!token.stop_requested())
            {
               {
                  auto test_service = make_test_service(name);

                  std::this_thread::sleep_for(100ms);
               }
               std::this_thread::sleep_for(500ms);
            }
         }};
      };

      auto restarting_thread_1 = make_restarting_service_thread("test");
      auto restarting_thread_2 = make_restarting_service_thread("other");

      vbus::abi::client cl_2{R"(
vbus.connection.string=tcp://127.0.0.1:23002

)"sv};

      cl_2.run_async();

      auto started = std::chrono::steady_clock::now();

      while (true)
      {
         auto request = cl_2.create_request("test");
         request->timeout_ms = 1500;

         auto response = cl_2.send_request_sync(std::move(request));

         failed += response->status == 100 ? 0 : 1;
         ++total;

         if (std::chrono::steady_clock::now() - started >= 1min)
         {
            break;
         }
         std::this_thread::sleep_for(10ms);
      }

      restarting_thread_1.request_stop();
      restarting_thread_1.join();

      auto stable_test_svc = make_test_service("test");
      std::this_thread::sleep_for(10ms);

      auto request = cl_2.create_request("test");
      request->timeout_ms = 1500;

      auto response = cl_2.send_request_sync(std::move(request));
      REQUIRE(response->status == 100);
   }

   std::cout << "f: " << failed << " t: " << total << std::endl;
}