#define CATCH_CONFIG_RUNNER

#include <catch.hpp>

#include <vbus/vbus_easylogging.hpp>
SHARE_EASYLOGGINGPP_WITH_VBUS

#include <vbus/vbus.hpp>

using namespace std::string_view_literals;

int main(int argc, char *argv[])
{
   // Custom setup code
   vbus_init_logger(vbus::abi::to_vbus_string_view(R"xzy(
    *GLOBAL:
      FORMAT = %datetime [%levshort] [%fbase:%line] %msg
      To_File = false
      To_Standard_Output = true
    *DEBUG:
      Enabled = true
    *TRACE:
      Enabled = true
    )xzy"));

   // Initialize Catch session
   Catch::Session session;

   // Apply command-line arguments to Catch session
   int result = session.applyCommandLine(argc, argv);
   if (result != 0)
   {
      return result;
   }

   // Run Catch session
   result = session.run();

   // Custom teardown code (if any)

   return result;
}
