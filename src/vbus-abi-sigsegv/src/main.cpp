//
// Created by jhrub on 26.01.2021.
//

#include <chrono>
#include <commons/make_string.h>
#include <thread>
#include <vbus/vbus.hpp>
#include <vbus/vbus_easylogging.hpp>
SHARE_EASYLOGGINGPP_WITH_VBUS

using namespace vbus::abi;
using namespace std::chrono_literals;
using namespace std::string_literals;

int main(int argc, const char *argv[])
{

   directory_service service;
   std::string connection_string = static_cast<std::string>(service.get_connection_string());

   auto props = commons::make_string("vbus.instance.name = inproc-dir\nvbus.connection.string=", connection_string);
   auto num = std::atomic<int64_t>{0};
   auto test_service = make_service([&num](response_sender &sender, vbus_request_t *req, string_array const &msg)
                                    { sender.send_response(num++); });

   service_config config;
   config.register_service({"test"}, test_service);

   client services{props, config.config_view()};
   services.run_async();

   client cl{props};
   cl.set_default_request_timeout(10000);
   cl.run_async();

   const auto start_sending = std::chrono::steady_clock::now();
   int request_count = 100000;
   for (int i = 0; i < request_count; ++i)
   {
      auto request = cl.create_request("test");
      auto response = cl.send_request_sync(std::move(request));
      if (!response)
      {
         LOG(ERROR) << "nullptr response";
      }
      else if (response->status == i)
      {
         LOG_IF(i % 10000 == 0, INFO) << "processed:" << i;
      }
      else
      {
         LOG(ERROR) << "invalid response: " << response->status;
      }
   }

   const auto end_sending = std::chrono::steady_clock::now();

   LOG(INFO) << "execution time required to send  " << request_count << " request responses : "
             << std::chrono::duration_cast<std::chrono::milliseconds>(end_sending - start_sending).count() << "ms";
   return 0;
}