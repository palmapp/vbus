set(target vbus-abi-sigsegv)

set(abiSigSegv_Sources
        src/main.cpp)

add_executable(${target} ${abiSigSegv_Sources})

set(clientTest_Libs ${Boost_LIBRARIES} commons logging fmt::fmt base64 vbus-abi Catch2::Catch2WithMain ${EXE_LIBS})
target_link_libraries(${target} PUBLIC ${clientTest_Libs})


add_custom_command(
        TARGET vbus-abi-sigsegv
        POST_BUILD
        COMMAND cmake -P ${CMAKE_CURRENT_SOURCE_DIR}/copy_deps.cmake
)