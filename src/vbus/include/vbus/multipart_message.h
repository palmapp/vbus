#pragma once
#include <deque>
#include <vector>

#include <string_view>
#include <type_traits>
#include <zmq.hpp>

namespace vbus
{
   struct multipart_message
   {
      std::deque<zmq::message_t> parts;

      multipart_message();
      multipart_message(multipart_message const &);
      multipart_message(multipart_message &&);

      multipart_message &operator=(multipart_message const &);
      multipart_message &operator=(multipart_message &&);

      std::string_view operator[](std::size_t idx) const;

      std::size_t total_byte_count() const;

      template <class T>
      bool try_interpret_part_as(T &result, std::size_t idx) const
      {
         static_assert(std::is_trivially_copyable_v<T>, "T must satisfy is_trivially_copyable_v");
         if (idx >= size()) return false;

         const auto &part = this->operator[](idx);
         if (sizeof(T) < part.size())
         {
            return false;
         }

         std::copy(part.begin(), part.end(), reinterpret_cast<char *>(&result));

         return true;
      }

      std::vector<std::string> to_vector_string(int skip = 0) const;

      bool swap(std::size_t a, std::size_t b);
      std::size_t size() const;
      bool empty() const;

      inline void push_empty_frame()
      {
         parts.emplace_back("\0", 1);
      }

      inline void push_part(zmq::message_t part)
      {
         parts.push_back(std::move(part));
      }

      inline void push_part(zmq::message_t &&part)
      {
         parts.push_back(std::move(part));
      }

      inline void push_part(std::string_view val)
      {
         parts.emplace_back(val.begin(), val.end());
      }

      inline void push_part(std::string const &val)
      {
         parts.emplace_back(val.begin(), val.end());
      }

      template <class T>
      void push_part(T const &val)
      {
         static_assert(std::is_trivially_copyable_v<T>, "T must satisfy is_trivially_copyable_v");
         parts.emplace_back(&val, sizeof(val));
      }

      /// this method pushes the empty part ready for reading
      zmq::message_t &add_part();
      void pop_front(std::size_t n);

      void clear();
   };

   bool recieve_multipart_message_from_socket(zmq::socket_t &socket, multipart_message &msg);
   void send_multipart_message_to_socket(zmq::socket_t &socket, multipart_message &msg, std::size_t skip_frames = 0);
   bool send_multipart_message_to_socket_noblock(zmq::socket_t &socket, multipart_message &msg,
                                                 std::size_t skip_frames = 0);
} // namespace vbus