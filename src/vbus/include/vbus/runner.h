//
// Created by jhrub on 26.01.2021.
//

#pragma once

#include "bus_properties.h"
#include <commons/cancellation_token.h>
namespace vbus
{

   class runner_impl;
   class runner
   {
    public:
      runner(std::string instance_id, bus_properties props);
      ~runner();

      void run_forever(commons::cancellation_token ct);

      const std::string &get_bus_connection_string() const;

    private:
      runner_impl *impl_;
   };
} // namespace vbus
