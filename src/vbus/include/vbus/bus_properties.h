//
// Created by jhrub on 07.05.2021.
//

#pragma once
#include "connection_string.h"
#include <vector>

namespace vbus
{
   struct bus_properties
   {
      connection_string bind_string;

      std::string remote_access_address;
      std::string db_connection_string;

      int recv_timeout_milliseconds = 20;
      bool tracing_enabled = false;

      std::vector<std::string> cluster;
      std::optional<connection_string> prometheus_bind_string;

      uint16_t pub_sub_bind_port = 0;
      uint16_t router_bind_port = 0;
   };
} // namespace vbus