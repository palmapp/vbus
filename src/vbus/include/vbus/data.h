#pragma once

// clang-format off

#include <chrono>
#include <map>
#include <string>
#include <vector>
#include <optional>
#include <msgpack.hpp>
#include <visit_struct/visit_struct.hpp>
//#include <boost/container/flat_map.hpp>
// clang-format on

namespace vbus
{

   using time_point = std::chrono::steady_clock::time_point;
   using duration = std::chrono::steady_clock::duration;

   struct service_metadata
   {
      std::string interface;
      std::string data;

      inline bool operator==(service_metadata const &other) const
      {
         return interface == other.interface && data == other.data;
      }

      inline bool operator!=(service_metadata const &other) const
      {
         return interface != other.interface || data != other.data;
      }

      MSGPACK_DEFINE_ARRAY(interface, data);
   };

   struct service_record
   {
      std::string name;
      std::string connection_string;
      bool is_online = false;

      service_metadata metadata;

      MSGPACK_DEFINE_ARRAY(name, connection_string, is_online, metadata);
   };

   struct directory_state
   {
      std::string hub_connection_string;
      std::vector<service_record> services;
      std::string instance_name;

      std::int64_t id;

      std::string pub_sub_connection_string;
      std::int64_t clock;

      inline directory_state(std::string hs, std::vector<service_record> _services, std::string _instance_name,
                             std::int64_t _id, std::string _pub_sub_connection_string)
          : hub_connection_string(std::move(hs)), services(std::move(_services)),
            instance_name(std::move(_instance_name)), id(_id),
            pub_sub_connection_string(std::move(_pub_sub_connection_string))
      {
      }

      inline directory_state()
      {
         id = -1;
         clock = 0;
      }

      MSGPACK_DEFINE_ARRAY(hub_connection_string, services, instance_name, id, pub_sub_connection_string, clock);
   };

   struct service_registration_message
   {
      std::string vbus_instance_name;
      std::string connection_string;
      std::string service_id;

      service_metadata metadata;

      bool cluster_visible = false;
      MSGPACK_DEFINE_ARRAY(vbus_instance_name, connection_string, service_id, metadata, cluster_visible);
   };

   struct request_metadata_frame
   {
      /// this message sequence number, that is unique per client instance
      std::uint64_t sequence_number = 0;

      /// this is uuid for given request -> it must be globally unique
      std::array<std::uint8_t, 16> id{};

      /// this is unique action id per service (we have 65k possible actions)
      std::uint16_t action_id = 0;

      MSGPACK_DEFINE_ARRAY(sequence_number, id, action_id);
   };

   struct request_timeout_frame
   {
      /// relative timeout
      std::int64_t timeout_ms = -1;
      std::int64_t sent_time_point = 0;

      /// steady clock timeout - not transferred over the network
      std::optional<std::chrono::steady_clock::time_point> timeout_at = {};

      /// steady clock time point - not transferred over the network
      std::optional<std::chrono::steady_clock::time_point> local_sent_time_point = {};

      MSGPACK_DEFINE_ARRAY(timeout_ms, sent_time_point);
   };

   // using service_secrets = boost::container::flat_map<std::string, std::string>;

   enum class bus_message_type : std::uint8_t
   {
      unknown,
      service_registration,
      request,
      response,
      ping,
      publish,
      publish_sticky,
      error
   };

   struct sticky_message
   {
      std::vector<std::string> parts;
      MSGPACK_DEFINE_ARRAY(parts);
   };
}; // namespace vbus

VISITABLE_STRUCT(vbus::service_metadata, interface, data);
VISITABLE_STRUCT(vbus::service_record, name, connection_string, metadata);
VISITABLE_STRUCT(vbus::directory_state, hub_connection_string, services);
VISITABLE_STRUCT(vbus::service_registration_message, service_id, vbus_instance_name, connection_string);
VISITABLE_STRUCT(vbus::sticky_message, parts);