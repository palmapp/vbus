
#pragma once

#include "data.h"
#include <boost/container/flat_map.hpp>
#include <boost/container/flat_set.hpp>
#include <memory>

namespace vbus
{

   enum class directory_change_result
   {
      no_change,
      directory_changed
   };

   struct service_track
   {
      time_point last_update;
      std::string connection_string;
      bool is_offline = false;

      service_metadata metadata;
   };

   class directory_manager
   {
    public:
      directory_manager(std::string instance_name, duration expiration);

      std::shared_ptr<directory_state> get_current_state() const;
      bool validate_instance_name(std::string const &instance_name) const;

      directory_change_result keep_service_registration(time_point now, service_registration_message const &message);

      directory_change_result tick(time_point now);

      inline void set_hub_connection_string(std::string cs, std::string pub_sub)
      {
         hub_connection_string_ = std::move(cs);
         pub_sub_connection_string_ = std::move(pub_sub);

         create_new_public_state();
      }

      inline std::string_view get_instance_name() const
      {
         return instance_name_;
      }

    private:
      std::string instance_name_;
      time_point last_tick_called_;

      duration expiration_;

      std::string hub_connection_string_;
      std::string pub_sub_connection_string_;

      boost::container::flat_map<std::string, service_track> service_tracking_;

      std::shared_ptr<directory_state> public_directory_state_;
      std::int64_t id_counter_ = 0;

      bool refresh_expired_services(time_point now);
      void create_new_public_state();
   };
} // namespace vbus