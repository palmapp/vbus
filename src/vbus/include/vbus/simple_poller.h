//
// Created by jhrub on 20.05.2022.
//

#pragma once
#include "pollable_monitor.h"
#include <commons/basic_types.h>
#include <deque>
#include <easylogging++.h>
#include <functional>
#include <zmq.hpp>

namespace vbus
{

   class simple_poller
   {
    public:
      using u64 = commons::basic_types::u64;

      simple_poller()
      {
         // ensuring events_ always has some valid memory
         events_ = std::make_shared<events_buffer>();
         events_->emplace_back();
      }

      template <class FuncT>
      void add(zmq::socket_t &socket, zmq::event_flags events, FuncT &&func)
      {
         poller_.add(socket, events, create_handler(std::forward<FuncT>(func)));

         handler_index_[std::addressof(socket)] = poll_handlers_.size() - 1;

         events_buffer_increment_size();
      }

      inline void remove(zmq::socket_t &socket)
      {
         poller_.remove(socket);
         if (auto it = handler_index_.find(std::addressof(socket)); it != handler_index_.end())
         {
            events_buffer_decrement_size();

            // we are intentionally wasting one slot here, we cannot change addresses of already inserted handlers
            poll_handlers_[it->second] = nullptr;
            handler_index_.erase(it);
         }
      }

      inline bool wait(std::chrono::milliseconds duration)
      {
         auto buffer_ptr = events_;
         auto &events = *buffer_ptr;

         auto result = poller_.wait_all(events, duration);
         if (result == 0)
         {
            return false;
         }

         auto begin = events.begin();
         auto end = begin + std::min(result, events.size());

         std::for_each(begin, end,
                       [](zmq::poller_event<std::function<void()>> const &event)
                       {
                          if (event.user_data != nullptr)
                          {
                             try
                             {
                                (*reinterpret_cast<std::function<void(void)> *>(event.user_data))();
                             }
                             catch (const std::exception &ex)
                             {
                                LOG(ERROR) << "exception while event was processed : " << ex.what();
                             }
                             catch (...)
                             {
                                LOG(ERROR) << "exception while event was processed : unknown";
                             }
                          }
                          else
                          {
                             LOG(WARNING) << "event.user_data == nullptr";
                          }
                       });

         return true;
      }

    private:
      std::deque<std::function<void()>> poll_handlers_;
      std::unordered_map<zmq::socket_t *, u64> handler_index_;

      using events_buffer = std::vector<zmq::poller_event<std::function<void()>>>;
      std::shared_ptr<events_buffer> events_;

      zmq::poller_t<std::function<void()>> poller_;

      template <class T>
      std::function<void()> *create_handler(T &&function)
      {
         poll_handlers_.push_back(std::forward<T>(function));
         return &poll_handlers_.back();
      }

      template <class Func>
      void modify_events_buffer(Func func)
      {
         auto events = events_;
         auto size = events->size();
         auto ref_counter = events.use_count();

         // 1 - is for member
         // 2 - if for local events
         // if there would be 3 - it means that we are modifying the events buffer within the wait call
         // which caused - use after free bug -> we create a new events_buffer
         if (ref_counter == 2)
         {
            func(*events, size);
         }
         else
         {
            events = std::make_shared<events_buffer>();
            func(*events, size);
            events_ = events;
         }
      }

      void events_buffer_increment_size()
      {
         modify_events_buffer([](events_buffer &buffer, std::size_t current) { buffer.resize(current + 1); });
      }

      void events_buffer_decrement_size()
      {
         modify_events_buffer([](events_buffer &buffer, std::size_t current)
                              { buffer.resize(std::max(current - 1, std::size_t{1})); });
      }
   };

} // namespace vbus