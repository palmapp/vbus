#pragma once
#include <commons/make_string.h>
#include <cstdint>
#include <optional>
#include <string>
#include <string_view>

namespace vbus
{
   struct connection_string
   {
      std::string protocol;
      std::string host;
      std::int32_t port = 0;

      inline std::string to_string() const
      {
         if (port > 0) return commons::make_string(protocol, "://", host, ":", port);
         return commons::make_string(protocol, "://", host, ":*");
      }

      static std::optional<connection_string> parse(std::string_view string);
   };
} // namespace vbus
