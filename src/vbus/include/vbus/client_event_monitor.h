//
// Created by jhrub on 13.03.2021.
//

#pragma once
#include "pollable_monitor.h"
namespace vbus
{
   class client_event_monitor : public zmq::pollable_monitor
   {
    public:
      using pollable_monitor::pollable_monitor;

      virtual ~client_event_monitor();

      void on_event_handshake_succeeded(const zmq_event_t &event_, const char *addr_) override;

      void on_event_disconnected(const zmq_event_t &event_, const char *addr_) override;

      void on_event_connected(const zmq_event_t &event_, const char *addr_) override;
      void on_event_connect_delayed(const zmq_event_t &event_, const char *addr_) override;
      void on_event_connect_retried(const zmq_event_t &event_, const char *addr_) override;

      void on_event_handshake_failed_no_detail(const zmq_event_t &event_, const char *addr_) override;
      void on_event_handshake_failed_protocol(const zmq_event_t &event_, const char *addr_) override;
      void on_event_handshake_failed_auth(const zmq_event_t &event_, const char *addr_) override;

      void set_info(const std::string &info);

    private:
      std::string info_;
   };
} // namespace vbus
