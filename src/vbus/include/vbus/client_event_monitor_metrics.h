//
// Created by jhrub on 20.03.2023.
//

#pragma once
#include "client_event_monitor.h"
namespace spider2::prometheus
{
   class metrics_manager;
}

namespace vbus
{
   struct client_event_monitor_metrics_holder;
   class client_event_monitor_metrics : public client_event_monitor
   {
    public:
      client_event_monitor_metrics(std::string labels, zmq::context_t &context,
                                   spider2::prometheus::metrics_manager &metrics_manager);
      ~client_event_monitor_metrics();

      void on_event_handshake_succeeded(const zmq_event_t &event_, const char *addr_) override;
      void on_event_disconnected(const zmq_event_t &event_, const char *addr_) override;
      void on_event_connected(const zmq_event_t &event_, const char *addr_) override;
      void on_event_connect_delayed(const zmq_event_t &event_, const char *addr_) override;
      void on_event_connect_retried(const zmq_event_t &event_, const char *addr_) override;
      void on_event_handshake_failed_no_detail(const zmq_event_t &event_, const char *addr_) override;
      void on_event_handshake_failed_protocol(const zmq_event_t &event_, const char *addr_) override;
      void on_event_handshake_failed_auth(const zmq_event_t &event_, const char *addr_) override;

    private:
      std::string labels_;
      std::unique_ptr<client_event_monitor_metrics_holder> metrics_;
   };
} // namespace vbus