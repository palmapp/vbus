#pragma once
#include <commons/active_object.h>
#include <map>
#include <string>
#include <vector>
#include <xdb/connection.h>


namespace vbus
{
   struct sticky_model_storage_dialect;
   class sticky_model_storage : commons::active_object<std::pair<std::string, std::string>>
   {
    public:
      sticky_model_storage(std::string connection_string);

      std::vector<std::pair<std::string, std::string>> load_all_values();
      void set_value(std::string key, std::string value);

    protected:
      void on_message(std::pair<std::string, std::string> &message) final;
      void on_exception(std::pair<std::string, std::string> const &message, const std::exception &) noexcept final;
      void on_unknown_exception(std::pair<std::string, std::string> const &message) noexcept final;

    private:
      std::string connection_string_;
      xdb::connection connection_;
      sticky_model_storage_dialect const *sql_dialect_;
      xdb::query replace_query_;
   };

} // namespace vbus