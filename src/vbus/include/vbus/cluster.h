//
// Created by jhrub on 29.07.2022.
//

#pragma once
#include "bus_properties.h"
#include "multipart_message.h"
#include "simple_poller.h"
#include <boost/algorithm/string/predicate.hpp>
#include <boost/unordered_map.hpp>
#include <memory>
#include <spider2/prometheus.h>
#include <string>
#include "client_event_monitor_metrics.h"

namespace vbus
{
   struct directory_state;

   struct monitoring
   {
      std::string labels;

      struct derived_labels
      {
         std::string blocked;
         std::string not_connected;
         std::string state_errors;
         std::string unable_to_recv;

         derived_labels(std::string const &base_label);
      };

      derived_labels error_labels;

      spider2::prometheus::gauge parse_section;
      spider2::prometheus::gauge broadcast_section;
      spider2::prometheus::gauge broadcast_service_registration_section;

      spider2::prometheus::counter directory_state_errors;
      spider2::prometheus::counter directory_state_unable_to_recv;
      spider2::prometheus::counter blocked;
      spider2::prometheus::counter not_connected;
      spider2::prometheus::counter changed_connection_strings;
      spider2::prometheus::counter directory_state_recv;

      spider2::prometheus::gauge clock_difference;

      monitoring(std::string labels, spider2::prometheus::metrics_manager &mng);
   };

   struct cluster_client
   {
      monitoring metrics;
      zmq::socket_t directory;
      zmq::socket_t publish_socket;
      std::string hub_connection_string;
      std::string main_connection_string;


      std::optional<client_event_monitor_metrics> directory_monitor = {};
      std::optional<client_event_monitor_metrics> publish_socket_monitor = {};

      struct service_registration
      {
         std::vector<std::string> message;
         std::chrono::steady_clock::time_point updated;
      };

      boost::unordered_map<std::string, service_registration> service_registration_messages_ = {};
   };

   class cluster
   {
   public:
      cluster(spider2::prometheus::metrics_manager &mng, zmq::context_t &ctx, simple_poller &poller,
              bus_properties const &props);
      void broadcast(std::vector<std::string> const &msg);
      void broadcast_service_registration(std::string const &id, std::vector<std::string> const &msg);

      inline bool can_send(multipart_message &msg) const
      {
         return !clients_.empty() && !boost::starts_with(msg[0], "cluster@");
      }

   private:
      multipart_message temp;
      std::vector<std::unique_ptr<cluster_client>> clients_;

      void prepare_to_send(std::vector<std::string> const &msg);
      void check_clock(directory_state const &dir, cluster_client &client) const;
      bool ensure_publish_socket_connection(directory_state const &dir, cluster_client &client) const;
      void flush_pending_registrations(cluster_client &client);

      bool send_message(cluster_client &client, std::vector<std::string> const &msg);
   };

} // namespace vbus