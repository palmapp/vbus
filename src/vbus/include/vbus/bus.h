#pragma once
// clang-format off
#include "data.h"
#include "connection_string.h"

#include "multipart_message.h"
#include "bus_properties.h"
#include "simple_poller.h"
#include "pollable_monitor.h"
#include <map>
#include <memory>
#include <string>

#include "cluster.h"

#include <spider2/prometheus.h>
#include <boost/container/flat_map.hpp>
// clang-format on

namespace vbus
{

   class event_monitor;
   class directory_manager;
   class sticky_model_storage;

   struct decrement_timer
   {
    public:
      inline void start(int steps)
      {
         steps_ = steps;
      }
      inline bool decrement()
      {
         if (steps_ > 0)
            return --steps_ == 0;
         else
            return false;
      }

    private:
      int steps_ = 0;
   };

   struct bus_monitoring
   {
      spider2::prometheus::gauge broadcast_directory_section;
      spider2::prometheus::gauge broadcast_section;
      spider2::prometheus::gauge service_registration_section;
      spider2::prometheus::gauge ping_section;
      spider2::prometheus::gauge client_message_section;

      spider2::prometheus::counter send_message_counter;

      spider2::prometheus::counter service_registered;
      spider2::prometheus::counter directory_changed;
      spider2::prometheus::counter error_invalid_instance_name;
      spider2::prometheus::counter error_parse;

      bus_monitoring(spider2::prometheus::metrics_manager &mng);
   };

   class bus
   {
    public:
      enum cluster_visibility
      {
         hidden,
         visible
      };

      bus(directory_manager &directory, zmq::context_t &context, bus_properties const &props);
      ~bus();

      inline void run_once()
      {
         validate_directory();
         poller_.wait(std::chrono::milliseconds{10});
         on_after_poll();
      }

      inline const std::string &get_connection_string() const
      {
         return pub_socket_bind_string_;
      }

      spider2::prometheus::metrics_manager metrics_;

    private:
      bus_monitoring health_;
      void on_after_poll();
      void validate_directory();
      void run_step_router_socket();

      bool tracing_enabled_;
      directory_manager &directory_;

      zmq::socket_t pub_socket_directory_;
      zmq::socket_t pub_socket_;
      zmq::socket_t router_socket_;

      std::unique_ptr<event_monitor> monitor_directory_;
      std::unique_ptr<event_monitor> monitor_pub_;
      std::unique_ptr<sticky_model_storage> storage_;

      multipart_message msg_;
      std::map<std::string, std::vector<std::string>> fixed_models_;

      simple_poller poller_;
      std::string pub_socket_bind_string_;

      std::array<decrement_timer, 4> directory_resend_timer_;
      std::array<decrement_timer, 3> sticky_resend_timer_;

      cluster cluster_;

      std::deque<std::string> metrics_strings_;
      boost::container::flat_map<std::string, spider2::prometheus::counter> received_registrations_;

      template <class Array>
      bool timer_decrement(Array &array)
      {
         bool result = false;
         for (auto &timer : array)
         {
            result |= timer.decrement();
         }

         return result;
      }

      friend class event_monitor;

      void process_client_ping_message();

      void process_service_registration_message();
      void broadcast_directory_change();
      void broadcast_sticky();

      void broadcast(std::string const &topic, std::string const &message);
      void broadcast(std::string const &topic, std::vector<std::string> const &parts);

      void broadcast_directory(std::string const &topic, std::string const &message);
      void broadcast_directory(std::string const &topic, std::vector<std::string> const &parts);

      void send_error_msg(std::string const &id, std::string const &error);

      void trace_bus_message(vbus::bus_message_type type);

      static char const *message_type_to_string(bus_message_type type);

      std::string format_tracing_message();
   };
} // namespace vbus