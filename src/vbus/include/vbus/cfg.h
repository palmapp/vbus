//
// Created by jhrub on 14.03.2021.
//

#pragma once
#include <string>
namespace vbus::cfg
{
   void fill_default_values(const std::string &client_connection_string, std::string &pubsub_bind,
                            std::string &remote_address);

}