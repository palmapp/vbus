//
// Created by jhrub on 23.05.2022.
//

#pragma once
#include <zmq.hpp>

namespace vbus
{
   class simple_poller;
}

namespace zmq
{
   class pollable_monitor
   {
   public:
      pollable_monitor(zmq::context_t &context)
         : context_(context), socketPtr(NULL), monitor_socket(context, ZMQ_PAIR)
      {
      }

      virtual ~pollable_monitor()
      {
         if (socketPtr) zmq_socket_monitor(socketPtr, NULL, 0);
      }

#ifdef ZMQ_HAS_RVALUE_REFS
      pollable_monitor(pollable_monitor &&rhs) ZMQ_NOTHROW
         : context_(rhs.context_),
           socketPtr(rhs.socketPtr),
           monitor_socket(std::move(rhs.monitor_socket))
      {
         rhs.socketPtr = NULL;
      }

      socket_t &operator=(socket_t &&rhs) ZMQ_DELETED_FUNCTION;
#endif

      void init(socket_t &socket, std::string const &addr, int events = ZMQ_EVENT_ALL)
      {
         init(socket, addr.c_str(), events);
      }

      void init(socket_t &socket, const char *addr_, int events = ZMQ_EVENT_ALL)
      {
         void *socket_ptr = static_cast<void *>(socket);
         int rc = zmq_socket_monitor(socket_ptr, addr_, events);
         if (rc != 0) throw error_t();

         socketPtr = socket_ptr;

         monitor_socket.connect(addr_);
         monitor_socket.set(sockopt::rcvtimeo, 0);

         on_monitor_started();
      }

      bool recv_events()
      {
         assert(monitor_socket);

         zmq::message_t eventMsg;

         if (!monitor_socket.recv(eventMsg, recv_flags::none))
         {
            return false;
         }

#if ZMQ_VERSION_MAJOR >= 4
         const char *data = static_cast<const char *>(eventMsg.data());
         zmq_event_t msgEvent;
         memcpy(&msgEvent.event, data, sizeof(uint16_t));
         data += sizeof(uint16_t);
         memcpy(&msgEvent.value, data, sizeof(int32_t));
         zmq_event_t *event = &msgEvent;
#else
         zmq_event_t *event = static_cast<zmq_event_t *>(eventMsg.data());
#endif

#ifdef ZMQ_NEW_MONITOR_EVENT_LAYOUT

         zmq::message_t addrMsg;
         if (!monitor_socket.recv(addrMsg, recv_flags::none))
         {
            return false;
         }

         const char *str = static_cast<const char *>(addrMsg.data());
         std::string address(str, str + addrMsg.size());

#else
         // Bit of a hack, but all events in the zmq_event_t union have the same layout so this will work for all event
         // types.
         std::string address = event->data.connected.addr;
#endif

#ifdef ZMQ_EVENT_MONITOR_STOPPED
         if (event->event == ZMQ_EVENT_MONITOR_STOPPED)
         {
            return true;
         }

#endif

         switch (event->event)
         {
         case ZMQ_EVENT_CONNECTED:
            on_event_connected(*event, address.c_str());
            break;
         case ZMQ_EVENT_CONNECT_DELAYED:
            on_event_connect_delayed(*event, address.c_str());
            break;
         case ZMQ_EVENT_CONNECT_RETRIED:
            on_event_connect_retried(*event, address.c_str());
            break;
         case ZMQ_EVENT_LISTENING:
            on_event_listening(*event, address.c_str());
            break;
         case ZMQ_EVENT_BIND_FAILED:
            on_event_bind_failed(*event, address.c_str());
            break;
         case ZMQ_EVENT_ACCEPTED:
            on_event_accepted(*event, address.c_str());
            break;
         case ZMQ_EVENT_ACCEPT_FAILED:
            on_event_accept_failed(*event, address.c_str());
            break;
         case ZMQ_EVENT_CLOSED:
            on_event_closed(*event, address.c_str());
            break;
         case ZMQ_EVENT_CLOSE_FAILED:
            on_event_close_failed(*event, address.c_str());
            break;
         case ZMQ_EVENT_DISCONNECTED:
            on_event_disconnected(*event, address.c_str());
            break;
#ifdef ZMQ_BUILD_DRAFT_API
#if ZMQ_VERSION >= ZMQ_MAKE_VERSION(4, 2, 3)
         case ZMQ_EVENT_HANDSHAKE_FAILED_NO_DETAIL:
            on_event_handshake_failed_no_detail(*event, address.c_str());
            break;
         case ZMQ_EVENT_HANDSHAKE_FAILED_PROTOCOL:
            on_event_handshake_failed_protocol(*event, address.c_str());
            break;
         case ZMQ_EVENT_HANDSHAKE_FAILED_AUTH:
            on_event_handshake_failed_auth(*event, address.c_str());
            break;
         case ZMQ_EVENT_HANDSHAKE_SUCCEEDED:
            on_event_handshake_succeeded(*event, address.c_str());
            break;
#elif ZMQ_VERSION >= ZMQ_MAKE_VERSION(4, 2, 1)
         case ZMQ_EVENT_HANDSHAKE_FAILED:
            on_event_handshake_failed(*event, address.c_str());
            break;
         case ZMQ_EVENT_HANDSHAKE_SUCCEED:
            on_event_handshake_succeed(*event, address.c_str());
            break;
#endif
#endif
         default:
            on_event_unknown(*event, address.c_str());
            break;
         }

         return true;
      }

#ifdef ZMQ_EVENT_MONITOR_STOPPED
      void abort()
      {
         if (socketPtr) zmq_socket_monitor(socketPtr, NULL, 0);

         monitor_socket.close();

         socketPtr = NULL;
      }
#endif
      virtual void on_monitor_started()
      {
      }

      virtual void on_event_connected(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      virtual void on_event_connect_delayed(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      virtual void on_event_connect_retried(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      virtual void on_event_listening(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      virtual void on_event_bind_failed(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      virtual void on_event_accepted(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      virtual void on_event_accept_failed(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      virtual void on_event_closed(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      virtual void on_event_close_failed(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      virtual void on_event_disconnected(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }
#if ZMQ_VERSION >= ZMQ_MAKE_VERSION(4, 2, 3)
      virtual void on_event_handshake_failed_no_detail(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      virtual void on_event_handshake_failed_protocol(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      virtual void on_event_handshake_failed_auth(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      virtual void on_event_handshake_succeeded(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }
#elif ZMQ_VERSION >= ZMQ_MAKE_VERSION(4, 2, 1)
      virtual void on_event_handshake_failed(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }
      virtual void on_event_handshake_succeed(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }
#endif
      virtual void on_event_unknown(const zmq_event_t &event_, const char *addr_)
      {
         (void)event_;
         (void)addr_;
      }

      inline zmq::socket_t &get_monitor_socket()
      {
         return monitor_socket;
      }

   private:
      pollable_monitor(const pollable_monitor &) ZMQ_DELETED_FUNCTION;
      void operator=(const pollable_monitor &) ZMQ_DELETED_FUNCTION;

      zmq::context_t &context_;

      void *socketPtr;
      zmq::socket_t monitor_socket;
   };

} // namespace zmq