//
// Created by jhrub on 14.03.2021.
//
#include "cfg.h"
#include <connection_string.h>

void vbus::cfg::fill_default_values(const std::string &client_connection_string, std::string &pubsub_bind,
                                    std::string &remote_address)
{
   if (!client_connection_string.empty())
   {
      if (pubsub_bind.empty())
      {
         pubsub_bind = client_connection_string;
      }

      if (remote_address.empty())
      {
         auto parsed_client_connection = connection_string::parse(client_connection_string);
         if (parsed_client_connection)
         {
            remote_address = parsed_client_connection.value().host;
         }
      }
   }
}
