//
// Created by jhrub on 20.03.2023.
//
#include "client_event_monitor_metrics.h"
#include <spider2/prometheus.h>

using namespace spider2::prometheus;
namespace vbus
{
   struct client_event_monitor_metrics_holder
   {
      counter connected;
      counter handshake_succeeded;
      counter disconnected;
      counter connect_delayed;
      counter connect_retired;
      counter handshake_failed_no_detail;
      counter handshake_failed_protocol;
      counter handshake_failed_auth;
      counter handshake_failed;

      client_event_monitor_metrics_holder(metrics_manager &mng, std::string_view labels):
            connected(mng.create<counter>({
                .name = "vbus_cluster_socket_connected",
                .help = "on_event_connected",
                .labels = labels,
            })),
            handshake_succeeded(mng.create<counter>({
                .name = "vbus_cluster_socket_handshake_succeeded",
                .help = "on_event_handshake_succeeded",
                .labels = labels,
            })),
            disconnected(mng.create<counter>({
                .name = "vbus_cluster_socket_disconnected",
                .help = "on_event_connected",
                .labels = labels,
            })),
            connect_delayed(mng.create<counter>({
                .name = "vbus_cluster_socket_connect_delayed",
                .help = "on_event_connect_delayed",
                .labels = labels,
            })),
            connect_retired(mng.create<counter>({
                .name = "vbus_cluster_socket_connect_retired",
                .help = "on_event_connect_retried",
                .labels = labels,
            })),
            handshake_failed_no_detail(mng.create<counter>({
                .name = "vbus_cluster_socket_handshake_failed_no_detail",
                .help = "on_event_handshake_failed_no_detail",
                .labels = labels,
            })),
            handshake_failed_protocol(mng.create<counter>({
                .name = "vbus_cluster_socket_handshake_failed_protocol",
                .help = "on_event_handshake_failed_protocol",
                .labels = labels,
            })),
            handshake_failed_auth(mng.create<counter>({
                .name = "vbus_cluster_socket_handshake_failed_auth",
                .help = "on_event_handshake_failed_auth",
                .labels = labels,
            })),
            handshake_failed(mng.create<counter>({
                .name = "vbus_cluster_socket_handshake_failed",
                .help = "at any on_event_handshake_failed_*",
                .labels = labels,
            }))
      {
      }
   };
} // namespace vbus

void vbus::client_event_monitor_metrics::on_event_handshake_succeeded(const zmq_event_t &event_, const char *addr_)
{
   client_event_monitor::on_event_handshake_succeeded(event_, addr_);
   metrics_->handshake_succeeded.increment();
}
void vbus::client_event_monitor_metrics::on_event_disconnected(const zmq_event_t &event_, const char *addr_)
{
   client_event_monitor::on_event_disconnected(event_, addr_);
   metrics_->disconnected.increment();
}
void vbus::client_event_monitor_metrics::on_event_connected(const zmq_event_t &event_, const char *addr_)
{
   client_event_monitor::on_event_connected(event_, addr_);
   metrics_->connected.increment();
}
void vbus::client_event_monitor_metrics::on_event_connect_delayed(const zmq_event_t &event_, const char *addr_)
{
   client_event_monitor::on_event_connect_delayed(event_, addr_);
   metrics_->connect_delayed.increment();
}
void vbus::client_event_monitor_metrics::on_event_connect_retried(const zmq_event_t &event_, const char *addr_)
{
   client_event_monitor::on_event_connect_retried(event_, addr_);
   metrics_->connect_retired.increment();
}
void vbus::client_event_monitor_metrics::on_event_handshake_failed_no_detail(const zmq_event_t &event_,
                                                                             const char *addr_)
{
   client_event_monitor::on_event_handshake_failed_no_detail(event_, addr_);
   metrics_->handshake_failed_no_detail.increment();
   metrics_->handshake_failed.increment();
}

void vbus::client_event_monitor_metrics::on_event_handshake_failed_protocol(const zmq_event_t &event_,
                                                                            const char *addr_)
{
   client_event_monitor::on_event_handshake_failed_protocol(event_, addr_);
   metrics_->handshake_failed_protocol.increment();
   metrics_->handshake_failed.increment();
}
void vbus::client_event_monitor_metrics::on_event_handshake_failed_auth(const zmq_event_t &event_, const char *addr_)
{
   client_event_monitor::on_event_handshake_failed_auth(event_, addr_);
   metrics_->handshake_failed_auth.increment();
   metrics_->handshake_failed.increment();
}

vbus::client_event_monitor_metrics::client_event_monitor_metrics(std::string labels, zmq::context_t &context,
                                                                 metrics_manager &mng)
    : client_event_monitor(context), labels_(std::move(labels)), metrics_(std::make_unique<client_event_monitor_metrics_holder>(mng, labels_))
{
}

vbus::client_event_monitor_metrics::~client_event_monitor_metrics() {}
