// clang-format off

#include <atomic>
#include <easylogging++.h>
#include <xdb/connection.h>
#include <xdb/sqlite/sqlite_driver.h>

// disable warning C4245: 'argument': conversion from 'int' to 'unsigned int', signed/unsigned mismatch
#ifdef BOOST_MSVC
// save warning state
#pragma warning(push)
#pragma warning(disable : 4245)
#endif

#include <boost/application.hpp>

#ifdef BOOST_MSVC
#pragma warning(pop)
#endif

#include <boost/dll/runtime_symbol_info.hpp>
#include <commons/application_properties.h>
#include <commons/cmd_parser.h>
#include <commons/ignore_sig_pipe.h>
#include <commons/logging.h>
#include <commons/run_safe.h>

#include "runner.h"
#include "cfg.h"
#include "commons/win/cancel_on_wm_quit.h"
#include <commons/file_system.h>
#include <zmq.hpp>

// clang-format on

INITIALIZE_EASYLOGGINGPP

using namespace std::chrono_literals;
using namespace vbus;
using namespace boost::filesystem;
using namespace commons;

namespace ba = boost::application;
path g_exe_directory;
bool g_tracing_enabled = false;

struct vbus_app
{

   ba::context &context_;
   vbus_app(ba::context &context) : context_(context) {}

   int operator()()
   {
      int result = 1;

      auto base_path = context_.find<path>()->parent_path();
      auto variant = context_.find<path>()->filename();

      application_properties properties{prepare_properties_load_settings(base_path, variant)};

      auto expiration_timeout = std::chrono::seconds{std::max(
          properties.get_for_keys<int>({"vbus.service.expiration.seconds", "service.expiration.seconds"}, 10), 10)};

      auto instance_name = properties.get_for_keys<std::string>({"vbus.instance.name", "instance.name"},
                                                                std::string{context_.find<path>()->string()});
      auto pubsub_bind = properties.get_for_keys<std::string>({"vbus.server.bind", "pubsub.bind"});
      auto client_connection_string = properties.get<std::string>("vbus.connection.string");
      auto remote_address = properties.get_for_keys<std::string>({"vbus.remote.address", "remote.address"});
      auto db_connection_string = properties.get_for_keys<std::string>({"vbus.db.connection", "db.connection"});
      auto metrics_bind_string = properties.get<std::string>("vbus.metrics.bind", "");
      auto pub_sub_bind_port = properties.get<std::uint16_t>("vbus.pubsub.port", 0);
      auto router_bind_port = properties.get<std::uint16_t>("vbus.router.port", 0);

      std::optional<connection_string> metrics_bind;
      if (!metrics_bind_string.empty())
      {
         metrics_bind = connection_string::parse(metrics_bind_string);
         if (!metrics_bind.has_value())
         {
            LOG(WARNING) << "unable to parse vbus.metrics.bind = " << metrics_bind_string;
         }
      }

      auto cluster_file = properties.get_for_keys<std::string>({"vbus.cluster.file"});
      std::vector<std::string> vbus_cluster_list{};
      if (!cluster_file.empty())
      {
         if (exists(cluster_file))
         {
            cluster_file = read_file(cluster_file);
         }

         std::vector<std::string> connection_strings;
         boost::split(connection_strings, cluster_file, boost::is_any_of("\n;,"));
         for (auto &val : connection_strings)
         {
            boost::trim(val);
            if (connection_string::parse(val).has_value())
            {
               vbus_cluster_list.push_back(std::move(val));
            }
         }
      }

      vbus::cfg::fill_default_values(client_connection_string, pubsub_bind, remote_address);
      if (pubsub_bind.empty())
      {
         pubsub_bind = "tcp://127.0.0.1:50000";
      }

      if (remote_address.empty())
      {
         remote_address = "127.0.0.1";
      }

      auto bind_string = connection_string::parse(pubsub_bind);
      if (!bind_string)
      {
         LOG(ERROR) << "unable to parse bind string : " << pubsub_bind << " exiting.";
      }
      else
      {

         LOG(INFO) << "loaded config:"
                   << "\n service.expiration.seconds = "
                   << std::chrono::duration_cast<std::chrono::seconds>(expiration_timeout).count()
                   << "\n instance.name = " << instance_name << "\n pubsub.bind = " << pubsub_bind
                   << "\n remote.address = " << remote_address << "\n db.connection = " << db_connection_string;

         if (g_tracing_enabled)
         {
            LOG(INFO) << "tracing is enabled";
         }
         else
         {
            LOG(INFO) << "tracing is disabled => run vbus with arguments -add-loggers messages";
         }

#ifdef _WIN32
         auto wait_for_wm_quit_callback = std::stop_callback{stop_source.get_token(), [&] { cts.cancel(); }};
         auto win32_message_thread = commons::cancel_on_wm_quit(stop_source);
#endif

         result = commons::run_safe(
                      [&, this]
                      {
                         runner session(instance_name, {bind_string.value(), remote_address, db_connection_string, 20,
                                                        g_tracing_enabled, vbus_cluster_list, metrics_bind,
                                                        pub_sub_bind_port, router_bind_port});

                         session.run_forever(cts.get_cancellation_token());
                      },
                      "vbus application")
                      ? 0
                      : 2;
      }

      LOG(INFO) << "vbus exited with " << result;
      return result;
   }

   application_properties_settings prepare_properties_load_settings(const path &base_path, const path &variant) const
   {
      application_properties_settings app_prop_settings;
      app_prop_settings.base_path = base_path.c_str();
      if (auto it = std::find(app_prop_settings.properties_variants.begin(),
                              app_prop_settings.properties_variants.end(), variant.string());
          it == app_prop_settings.properties_variants.end())
      {
         app_prop_settings.properties_variants.push_back(variant.string());
      }
      return app_prop_settings;
   }

   bool stop()
   {
      cts.cancel();
      stop_source.request_stop();

      LOG(INFO) << "stopping application";
      return true;
   }

   bool pause()
   {
      return false; // return true to pause, false to ignore
   }

   bool resume()
   {
      return false; // return true to resume, false to ignore
   }

   commons::cancellation_token_source cts;
   std::stop_source stop_source;
};

int main(int argc, const char *argv[])
{
   ignore_sig_pipe();

   try
   {
      xdb::connection::register_driver(std::make_unique<xdb::backend::sqlite::driver>());

      g_exe_directory = boost::dll::program_location().parent_path();
      path config_path = g_exe_directory / "env.properties";
      cmd::try_get_arg(config_path, "-c", argc, argv);
      config_path = absolute(config_path);

      LOG(INFO) << "config file: " << config_path;

      std::vector<std::string> enabled_loggers;
      commons::init_easylogging(argc, argv, "vbus", g_exe_directory, &enabled_loggers);

      g_tracing_enabled = std::any_of(enabled_loggers.begin(), enabled_loggers.end(),
                                      [](auto const &logger) { return logger == "messages"; });

      boost::application::context ctx;
      ctx.insert(boost::application::csbl::make_shared<path>(config_path));

      boost::application::auto_handler<vbus_app> app(ctx);
      boost::system::error_code code;

      std::string console;
      if (cmd::try_get_arg(console, "--mode", argc, argv) && console == "service")
      {
         LOG(INFO) << "starting as a service";

         auto res = boost::application::launch<boost::application::server>(app, ctx, code);
         if (code)
         {
            LOG(FATAL) << "result error code: " << code.value() << " => " << code.message();
         }

         return res;
      }
      else
      {
         LOG(INFO) << "starting as a console";

         auto res = boost::application::launch<boost::application::common>(app, ctx, code);
         if (code)
         {
            LOG(FATAL) << "result error code: " << code.value() << " => " << code.message();
         }

         return res;
      }
   }
   catch (const std::exception &ex)
   {
      LOG(FATAL) << "exception has been thrown: " << ex.what();
   }
   catch (...)
   {
      LOG(FATAL) << "uknown exception has been thrown";
   }
}
