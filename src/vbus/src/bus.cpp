#include "bus.h"
#include "directory_manager.h"
#include "sticky_model_storage.h"
#include <array>
#include <boost/asio.hpp>

#include <commons/hex_encode.h>
#include <easylogging++.h>
#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <optional>
#include <serialization/msgpack.h>
#include <zmq.h>
#include <zmq.hpp>

using namespace std;
using namespace zmq;

namespace
{
   std::string convert_to_msgpack(std::vector<std::string> const &msg)
   {
      vbus::sticky_message to_convert;
      to_convert.parts = msg;

      return spider::serialization::messagepack::stringify_msgpack(to_convert);
   }

   std::vector<std::string> msgpack_to_multipart_message(std::string const &msg)
   {
      vbus::sticky_message to_convert;
      std::vector<std::string> result;

      if (spider::serialization::messagepack::parse_msgpack(to_convert, msg))
      {
         result = std::move(to_convert.parts);
      }
      else
      {
         LOG(WARNING) << "unable to parse message pack for sticky message";
      }

      return result;
   }

} // namespace
namespace vbus
{
   class event_monitor : public zmq::pollable_monitor
   {
    public:
      using pollable_monitor::pollable_monitor;

      ~event_monitor() {}

      void on_event_handshake_succeeded(const zmq_event_t &event_, const char *addr_) final
      {

         LOG(INFO) << fmt::format("{}: handshake_succeeded[{}]", addr_, id_);
         ++client_connected_;
      }

      void on_event_disconnected(const zmq_event_t &event_, const char *addr_) final
      {
         LOG(INFO) << fmt::format("{}: disconnected[{}]", addr_, id_);
      }

      int get_client_connected_and_reset()
      {
         int result = client_connected_;
         client_connected_ = 0;
         return result;
      }

      std::string id_;

    private:
      int client_connected_ = {0};
   };

   std::string get_last_endpoint(socket_t &socket)
   {
      return socket.get(zmq::sockopt::last_endpoint);
      ;
   }

   using namespace spider2;

   bus_monitoring::bus_monitoring(spider2::prometheus::metrics_manager &mng)
       : broadcast_directory_section(mng.create<prometheus::gauge>(
             {.name = "vbus_broadcast_directory_section", .help = "bus::broadcast_directory_change"})),
         broadcast_section(mng.create<prometheus::gauge>({.name = "vbus_broadcast_section", .help = "bus::broadcast"})),
         service_registration_section(mng.create<prometheus::gauge>(
             {.name = "vbus_service_registration_section", .help = "bus::process_service_registration_message"})),
         ping_section(
             mng.create<prometheus::gauge>({.name = "vbus_ping_section", .help = "bus::process_client_ping_message"})),
         client_message_section(mng.create<prometheus::gauge>(
             {.name = "vbus_client_message_section", .help = "section where recv of client message happens"})),
         send_message_counter(mng.create<prometheus::counter>(
             {.name = "vbus_send_message_counter", .help = "increments when message gets sent"})),
         service_registered(mng.create<prometheus::counter>(
             {.name = "vbus_service_registered",
              .help = "increments when service renews its registration and that changes the directory"})),
         directory_changed(mng.create<prometheus::counter>(
             {.name = "vbus_directory_changed", .help = "increments when directory change gets broadcast"})),
         error_invalid_instance_name(
             mng.create<prometheus::counter>({.name = "vbus_error", .labels = "type=\"invalid instance name\""})),
         error_parse(mng.create<prometheus::counter>({.name = "vbus_error", .labels = "type=\"invalid message\""}))
   {
   }
} // namespace vbus

using namespace vbus;
using namespace commons;

bus::bus(directory_manager &dir, context_t &context, bus_properties const &props)
    : health_(metrics_), tracing_enabled_(props.tracing_enabled), directory_(dir),
      pub_socket_directory_(context, ZMQ_PUB), pub_socket_(context, ZMQ_PUB), router_socket_(context, ZMQ_ROUTER),
      monitor_directory_(std::make_unique<event_monitor>(context)),
      monitor_pub_(std::make_unique<event_monitor>(context)), cluster_(metrics_, context, poller_, props)
{
   el::Loggers::getLogger("messages");

   if (props.db_connection_string.empty())
   {
      LOG(WARNING) << "db.connection is not specified. Starting with in-memory storage only.";
   }
   else
   {
      storage_ = std::make_unique<sticky_model_storage>(props.db_connection_string);
      for (auto &[key, value] : storage_->load_all_values())
      {
         fixed_models_[key] = msgpack_to_multipart_message(value);
      }

      LOG(DEBUG) << "fixed_models_ has size = " << fixed_models_.size();
   }

   auto make_endpoint_address_remote = [&](zmq::socket_t &socket) -> std::string
   {
      auto end_point = get_last_endpoint(socket);
      auto parsed = connection_string::parse(end_point);
      if (parsed.has_value())
      {
         if (!props.remote_access_address.empty())
         {
            parsed.value().host = props.remote_access_address;
         }

         return parsed.value().to_string();
      }
      else
      {
         throw zmq::error_t{};
      }
   };

   monitor_directory_->init(pub_socket_directory_,
                            make_string("inproc://monitor_", reinterpret_cast<std::int64_t>(this)));
   monitor_directory_->id_ = "directory";

   monitor_pub_->init(pub_socket_, make_string("inproc://monitor_subs_", reinterpret_cast<std::int64_t>(this)));
   monitor_pub_->id_ = "publisher";

   pub_socket_directory_.set(sockopt::sndhwm, 100000);
   pub_socket_directory_.set(sockopt::linger, 5000);

   pub_socket_directory_.bind(props.bind_string.to_string());
   pub_socket_bind_string_ = make_endpoint_address_remote(pub_socket_directory_);

   auto pub_bind_string = props.bind_string;
   pub_bind_string.port = props.pub_sub_bind_port;

   pub_socket_.set(sockopt::sndhwm, 100000);
   pub_socket_.set(sockopt::linger, 5000);

   pub_socket_.bind(pub_bind_string.to_string());
   auto pub_socket_connection_string = make_endpoint_address_remote(pub_socket_);

   auto router_bind_string = props.bind_string;
   router_bind_string.port = props.router_bind_port;

   router_socket_.bind(router_bind_string.to_string());
   auto router_endpoint = make_endpoint_address_remote(router_socket_);

   // hand over connection to the new connected client with the same identity set
   router_socket_.set(sockopt::router_handover, 1);
   router_socket_.set(sockopt::rcvtimeo, 0);

   dir.set_hub_connection_string(router_endpoint, pub_socket_connection_string);

   LOG(INFO) << fmt::format("Bound local\n    DIR : {}\n PUB-SUB : {}\n   ROUTER : {}\n ", pub_socket_bind_string_,
                            pub_socket_connection_string, router_endpoint);

   poller_.add(router_socket_, zmq::event_flags::pollin, [this]() { run_step_router_socket(); });

   poller_.add(monitor_directory_->get_monitor_socket(), zmq::event_flags::pollin,
               [this]()
               {
                  if (monitor_directory_->recv_events() && monitor_directory_->get_client_connected_and_reset() != 0)
                  {
                     broadcast_directory_change();

                     directory_resend_timer_[0].start(50);
                     directory_resend_timer_[1].start(100);
                     directory_resend_timer_[2].start(500);
                  }
               });

   poller_.add(monitor_pub_->get_monitor_socket(), zmq::event_flags::pollin,
               [this]()
               {
                  if (monitor_pub_->recv_events() && monitor_pub_->get_client_connected_and_reset() != 0)
                  {
                     broadcast_sticky();
                     sticky_resend_timer_[0].start(50);
                     sticky_resend_timer_[1].start(100);
                  }
               });

   directory_resend_timer_.back().start(1000);
   sticky_resend_timer_.back().start(1000);
}

bus::~bus() {}

void bus::validate_directory()
{
   if (directory_.tick(std::chrono::steady_clock::now()) == directory_change_result::directory_changed)
   {
      health_.directory_changed.increment();

      broadcast_directory_change();
   }
}

void bus::run_step_router_socket()
{
   auto client_scope = health_.client_message_section.increment_scope();
   bus_message_type type = bus_message_type::unknown;

   if (recieve_multipart_message_from_socket(router_socket_, msg_) &&
       msg_.try_interpret_part_as<bus_message_type>(type, 1))
   {
      if (tracing_enabled_) trace_bus_message(type);

      switch (type)
      {
      case bus_message_type::service_registration:
         process_service_registration_message();
         break;
      case bus_message_type::request:
         LOG(WARNING) << "bus_message_type::request is not supported on hub socket, please connect directly to "
                         "requested service";

         break;
      case bus_message_type::response:
         LOG(WARNING) << "bus_message_type::response is not supported on hub socket, please connect directly to "
                         "requested service";
         break;
      case bus_message_type::ping:
         process_client_ping_message();
         break;
      case bus_message_type::publish:
         if (cluster_.can_send(msg_))
         {
            auto copy = msg_.to_vector_string(1);
            send_multipart_message_to_socket(pub_socket_, msg_, 2);
            health_.send_message_counter.increment();

            cluster_.broadcast(copy);
         }
         else
         {
            send_multipart_message_to_socket(pub_socket_, msg_, 2);
            health_.send_message_counter.increment();
         }
         break;
      case bus_message_type::publish_sticky:
      {
         auto parts = msg_.to_vector_string(3);
         if (storage_ != nullptr)
         {
            storage_->set_value(static_cast<std::string>(msg_[2]), convert_to_msgpack(parts));
         }
         fixed_models_[static_cast<std::string>(msg_[2])] = std::move(parts);

         if (cluster_.can_send(msg_))
         {
            auto copy = msg_.to_vector_string(1);
            send_multipart_message_to_socket(pub_socket_, msg_, 2);
            health_.send_message_counter.increment();

            cluster_.broadcast(copy);
         }
         else
         {
            send_multipart_message_to_socket(pub_socket_, msg_, 2);
            health_.send_message_counter.increment();
         }
         break;
      }
      default:

         LOG(WARNING) << "not implemented bus_message_type = " << static_cast<int>(type);
         trace_bus_message(type);
         break;
      }
   }
}

void bus::process_client_ping_message()
{
   auto ping_scope = health_.ping_section.increment_scope();
   std::string error;
   if (!directory_.validate_instance_name(static_cast<std::string>(msg_[2])))
   {
      auto id = static_cast<std::string>(msg_[0]);
      LOG(WARNING) << "invalid client key received : " << msg_[0];

      send_error_msg(id, commons::make_string("invalid client key received > server instance is '",
                                              directory_.get_instance_name(), "'"));
   }
   else
   {
      send_multipart_message_to_socket(router_socket_, msg_);
      health_.send_message_counter.increment();
   }
}

void bus::process_service_registration_message()
{
   auto health_scope = health_.service_registration_section.increment_scope();
   auto id = static_cast<std::string>(msg_[0]);
   auto msg_data = msg_[2];
   std::string error;

   service_registration_message registration_data;
   if (spider::serialization::messagepack::parse_msgpack(registration_data, msg_data.data(), msg_data.size()))
   {
      if (registration_data.service_id.empty())
      {
         error = "missing service_id of registration data";
      }
      else if (directory_.validate_instance_name(registration_data.vbus_instance_name))
      {
         if (auto reg_it = received_registrations_.find(registration_data.service_id);
             reg_it != received_registrations_.end())
         {
            reg_it->second.increment();
         }
         else
         {
            metrics_strings_.push_back(fmt::format("service=\"{}\"", registration_data.service_id));
            auto counter = metrics_.create<spider2::prometheus::counter>(
                {.name = "vbus_service_reg_counter",
                 .help = "it increments every time when it receives registration message for given service",
                 .labels = metrics_strings_.back()});
            counter.increment();
            received_registrations_.insert(std::make_pair(registration_data.service_id, std::move(counter)));
         }

         if (directory_.keep_service_registration(std::chrono::steady_clock::now(), registration_data) ==
             directory_change_result::directory_changed)
         {
            health_.service_registered.increment();
            health_.directory_changed.increment();

            broadcast_directory_change();
         }

         if (registration_data.cluster_visible && cluster_.can_send(msg_))
         {
            auto copy_of_registration_message = msg_.to_vector_string(1);
            cluster_.broadcast_service_registration(registration_data.service_id, copy_of_registration_message);
         }
      }
      else
      {
         LOG(WARNING) << "service registration rejected due an incorrect vbus_instance_name : " << id
                      << " with vbus_instance_name " << registration_data.vbus_instance_name;

         error = commons::make_string("invalid vbus_instance_name received > server instance is '",
                                      directory_.get_instance_name(), "'");

         health_.error_invalid_instance_name.increment();
      }
   }
   else
   {
      LOG(ERROR) << "unable to parse bus_message_type::service_registration data";
      error = "unable to parse bus_message_type::service_registration data";
   }

   if (!error.empty())
   {
      send_error_msg(id, error);
   }
   else
   {
      send_multipart_message_to_socket(router_socket_, msg_);
      health_.send_message_counter.increment();
   }
}

void bus::send_error_msg(std::string const &id, std::string const &error)
{
   multipart_message error_msg;

   error_msg.push_part(id);
   error_msg.push_empty_frame();

   error_msg.push_part(bus_message_type::error);
   auto error_msg_pack = spider::serialization::messagepack::stringify_msgpack(std::array<std::string, 1>{error});
   error_msg.push_part(error_msg_pack);

   send_multipart_message_to_socket(router_socket_, error_msg);
   health_.send_message_counter.increment();
}

void bus::broadcast_sticky()
{
   for (auto &[topic, model] : fixed_models_)
   {
      broadcast(topic, model);
   }
}

void bus::broadcast_directory_change()
{
   auto state = directory_.get_current_state();
   state->clock =
       std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch())
           .count();

   broadcast_directory("/dir", spider::serialization::messagepack::stringify_msgpack(*state));
}

void bus::broadcast(std::string const &topic, std::string const &message)
{
   auto broadcast_scope = health_.broadcast_section.increment_scope();

   pub_socket_.send(const_buffer{topic.c_str(), topic.size()}, send_flags::sndmore);
   pub_socket_.send(const_buffer{message.c_str(), message.size()});
   health_.send_message_counter.increment();
}

void bus::broadcast(std::string const &topic, std::vector<std::string> const &parts)
{
   auto broadcast_scope = health_.broadcast_section.increment_scope();
   pub_socket_.send(const_buffer{topic.c_str(), topic.size()}, send_flags::sndmore);

   for (auto it = parts.begin(), before_end = parts.end() - 1, end = parts.end(); it != end; ++it)
   {
      pub_socket_.send(const_buffer{it->c_str(), it->size()},
                       it == before_end ? send_flags::none : send_flags::sndmore);
   }

   health_.send_message_counter.increment();
}

void bus::broadcast_directory(std::string const &topic, std::string const &message)
{
   auto health_scope = health_.broadcast_directory_section.increment_scope();

   pub_socket_directory_.send(const_buffer{topic.c_str(), topic.size()}, send_flags::sndmore);
   pub_socket_directory_.send(const_buffer{message.c_str(), message.size()});

   health_.send_message_counter.increment();
}

void bus::broadcast_directory(std::string const &topic, std::vector<std::string> const &parts)
{
   auto health_scope = health_.broadcast_directory_section.increment_scope();

   pub_socket_directory_.send(const_buffer{topic.c_str(), topic.size()}, send_flags::sndmore);

   for (auto it = parts.begin(), before_end = parts.end() - 1, end = parts.end(); it != end; ++it)
   {
      pub_socket_directory_.send(const_buffer{it->c_str(), it->size()},
                                 it == before_end ? send_flags::none : send_flags::sndmore);
   }

   health_.send_message_counter.increment();
}

void bus::trace_bus_message(vbus::bus_message_type type)
{
   CLOG(TRACE, "messages") << "[" << bus::message_type_to_string(type) << "] " << format_tracing_message();
}

char const *bus::message_type_to_string(bus_message_type type)
{
   switch (type)
   {
   case bus_message_type::service_registration:
      return "service_registration";
   case bus_message_type::request:
      return "request";
   case bus_message_type::response:
      return "response";
   case bus_message_type::ping:
      return "ping";
   case bus_message_type::publish:
      return "publish";
   case bus_message_type::publish_sticky:
      return "publish_sticky";
   default:
      return "unknown";
   }
}

const auto is_ascii_alpha = [](char c) { return c > 0x1F && c < 0x7F; };

template <class T>
std::string print_ascii_or_hex(T const &val)
{
   using namespace std;
   if (all_of(begin(val), end(val), is_ascii_alpha))
   {
      return static_cast<std::string>(val);
   }
   else
   {
      return encoding::hex_encode(val);
   }
}

std::string bus::format_tracing_message()
{
   using namespace std;
   fmt::memory_buffer buf;
   fmt::format_to(std::back_inserter(buf), "[{}] id={} ", msg_.size(), print_ascii_or_hex(msg_[0]));

   for (std::size_t i = 2; i < msg_.size(); ++i)
   {
      auto part = msg_[i];

      if (all_of(begin(part), end(part), is_ascii_alpha))
      {
         fmt::format_to(std::back_inserter(buf), "\n[{}] raw     > {}", i, part);
      }
      else
      {
         bool traced = false;
         try
         {
            auto result = msgpack::unpack(part.data(), part.size());
            if (!result.get().is_nil())
            {
               auto str = boost::lexical_cast<string>(result.get());
               fmt::format_to(std::back_inserter(buf), "\n[{}] msgpack > {}", i, str);
               traced = true;
            }
            else
            {
            }
         }
         catch (...)
         {
         }

         if (!traced)
         {
            fmt::format_to(std::back_inserter(buf), "\n[{}] hex     > {}", i, encoding::hex_encode(part));
         }
      }
   }
   return {buf.data(), buf.size()};
}

void bus::on_after_poll()
{
   if (timer_decrement(directory_resend_timer_))
   {
      broadcast_directory_change();
      directory_resend_timer_.back().start(1000);
   }

   if (timer_decrement(sticky_resend_timer_))
   {
      broadcast_sticky();
      sticky_resend_timer_.back().start(1000);
   }
}
