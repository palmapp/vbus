//
// Created by jhrub on 13.03.2021.
//

#include "client_event_monitor.h"
#include <easylogging++.h>

void vbus::client_event_monitor::on_event_handshake_succeeded(const zmq_event_t &event_, const char *addr_)
{
   LOG(DEBUG) << info_ << ": on_event_handshake_succeeded : " << addr_;
}

void vbus::client_event_monitor::on_event_disconnected(const zmq_event_t &event_, const char *addr_)
{
   LOG(INFO) << info_ << ": on_event_disconnected : " << addr_;
}
void vbus::client_event_monitor::set_info(const std::string &info)
{
   info_ = info;
}

void vbus::client_event_monitor::on_event_connected(const zmq_event_t &event_, const char *addr_)
{
   LOG(INFO) << info_ << ": on_event_connected : " << addr_;
}

void vbus::client_event_monitor::on_event_connect_delayed(const zmq_event_t &event_, const char *addr_)
{
   LOG(DEBUG) << info_ << ": on_event_connect_delayed : " << addr_;
}

void vbus::client_event_monitor::on_event_connect_retried(const zmq_event_t &event_, const char *addr_)
{
   LOG(DEBUG) << info_ << ": on_event_connect_retried : " << addr_;
}

void vbus::client_event_monitor::on_event_handshake_failed_no_detail(const zmq_event_t &event_, const char *addr_)
{

   zmq::error_t err{};
   LOG(ERROR) << info_ << ": on_event_handshake_failed_no_detail : " << addr_ << ": errno: [" << err.num() << "] "
              << err.what();
}

void vbus::client_event_monitor::on_event_handshake_failed_protocol(const zmq_event_t &event_, const char *addr_)
{
   static const std::unordered_map<int32_t, std::string> errors{
       {ZMQ_PROTOCOL_ERROR_ZMTP_UNSPECIFIED, "ZMQ_PROTOCOL_ERROR_ZMTP_UNSPECIFIED"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_UNEXPECTED_COMMAND, "ZMQ_PROTOCOL_ERROR_ZMTP_UNEXPECTED_COMMAND"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_INVALID_SEQUENCE, "ZMQ_PROTOCOL_ERROR_ZMTP_INVALID_SEQUENCE"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_KEY_EXCHANGE, "ZMQ_PROTOCOL_ERROR_ZMTP_KEY_EXCHANGE"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_UNSPECIFIED, "ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_UNSPECIFIED"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_MESSAGE, "ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_MESSAGE"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_HELLO, "ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_HELLO"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_INITIATE, "ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_INITIATE"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_ERROR, "ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_ERROR"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_READY, "ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_READY"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_WELCOME, "ZMQ_PROTOCOL_ERROR_ZMTP_MALFORMED_COMMAND_WELCOME"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_INVALID_METADATA, "ZMQ_PROTOCOL_ERROR_ZMTP_INVALID_METADATA"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_CRYPTOGRAPHIC, "ZMQ_PROTOCOL_ERROR_ZMTP_CRYPTOGRAPHIC"},
       {ZMQ_PROTOCOL_ERROR_ZMTP_MECHANISM_MISMATCH, "ZMQ_PROTOCOL_ERROR_ZMTP_MECHANISM_MISMATCH"},
       {ZMQ_PROTOCOL_ERROR_ZAP_UNSPECIFIED, "ZMQ_PROTOCOL_ERROR_ZAP_UNSPECIFIED"},
       {ZMQ_PROTOCOL_ERROR_ZAP_MALFORMED_REPLY, "ZMQ_PROTOCOL_ERROR_ZAP_MALFORMED_REPLY"},
       {ZMQ_PROTOCOL_ERROR_ZAP_BAD_REQUEST_ID, "ZMQ_PROTOCOL_ERROR_ZAP_BAD_REQUEST_ID"},
       {ZMQ_PROTOCOL_ERROR_ZAP_BAD_VERSION, "ZMQ_PROTOCOL_ERROR_ZAP_BAD_VERSION"},
       {ZMQ_PROTOCOL_ERROR_ZAP_INVALID_STATUS_CODE, "ZMQ_PROTOCOL_ERROR_ZAP_INVALID_STATUS_CODE"},
       {ZMQ_PROTOCOL_ERROR_ZAP_INVALID_METADATA, "ZMQ_PROTOCOL_ERROR_ZAP_INVALID_METADATA"},
       {ZMQ_PROTOCOL_ERROR_WS_UNSPECIFIED, "ZMQ_PROTOCOL_ERROR_WS_UNSPECIFIED"}};

   std::string error;
   if (auto it = errors.find(event_.value); it != errors.end())
   {
      error = it->second;
   }
   else
   {
      error = "unknown";
   }

   LOG(ERROR) << info_ << ": on_event_handshake_failed_protocol : " << addr_ << ": error : " << error;
}

void vbus::client_event_monitor::on_event_handshake_failed_auth(const zmq_event_t &event_, const char *addr_)
{
   LOG(ERROR) << info_ << ": on_event_handshake_failed_auth : " << addr_ << ": ZAP status : " << event_.value;
}
vbus::client_event_monitor::~client_event_monitor() {}
