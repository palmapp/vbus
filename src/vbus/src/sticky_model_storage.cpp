#include "sticky_model_storage.h"
#include <easylogging++.h>

namespace vbus
{
   using sql = const char *;
   struct sticky_model_storage_dialect
   {
      sql create_kv_pair_table;
      sql replace_kv_pair;
      sql load_all;

      sticky_model_storage_dialect()
      {
         create_kv_pair_table =
             "CREATE TABLE IF NOT EXISTS public_sticky (topic TEXT NOT NULL, value TEXT NOT NULL, CONSTRAINT "
             "public_sticky_pk PRIMARY KEY(topic))";

         replace_kv_pair = "REPLACE INTO public_sticky (topic, value) VALUES (?,?)";
         load_all = "SELECT topic, value FROM public_sticky";
      }
   };
} // namespace vbus

using namespace vbus;

static sticky_model_storage_dialect sql_dialect = {};

sticky_model_storage::sticky_model_storage(std::string connection_string)
    : connection_string_(std::move(connection_string)), connection_(xdb::connection::open(connection_string_))
{
   sql_dialect_ = &sql_dialect;
   connection_.execute_query(sql_dialect_->create_kv_pair_table);
   replace_query_ = connection_.create_query(sql_dialect_->replace_kv_pair);
}

std::vector<std::pair<std::string, std::string>> sticky_model_storage::load_all_values()
{
   std::vector<std::pair<std::string, std::string>> result;
   auto connection = xdb::connection::open(connection_string_);

   auto query_result = connection.execute_query_for_result(sql_dialect_->load_all);

   for (auto &row : query_result)
   {
      result.emplace_back(row.get_string(0).value(), row.get_string(1).value());
   }

   return result;
}

void sticky_model_storage::set_value(std::string key, std::string value)
{
   post_message({key, value});
}

void sticky_model_storage::on_exception(std::pair<std::string, std::string> const &message,
                                        const std::exception &ex) noexcept
{
   LOG(ERROR) << "unhadled exception : " << ex.what();
}

void sticky_model_storage::on_unknown_exception(std::pair<std::string, std::string> const &message) noexcept
{
   LOG(ERROR) << "unhadled uknown exception";
}

void sticky_model_storage::on_message(std::pair<std::string, std::string> &kv_pair)
{
   replace_query_.bind(1, kv_pair.first);
   replace_query_.bind(2, kv_pair.second);

   replace_query_.execute();
}
