#include "multipart_message.h"


using namespace std;
using namespace vbus;

multipart_message::multipart_message()
{
}

multipart_message::multipart_message(multipart_message const &from)
{
   this->operator=(from);
}

multipart_message::multipart_message(multipart_message &&from)
   : parts(std::move(from.parts))
{
}

multipart_message &multipart_message::operator=(multipart_message const &from)
{
   for (auto &part : from.parts)
   {
      parts.emplace_back(part.data(), part.size());
   }

   return *this;
}

multipart_message &multipart_message::operator=(multipart_message &&from)
{
   parts = std::move(from.parts);
   return *this;
}

string_view multipart_message::operator[](size_t idx) const
{
   if (idx < size())
   {
      auto &part = parts[idx];
      return {reinterpret_cast<const char *>(part.data()), part.size()};
   }
   else
   {
      return {};
   }
}

std::vector<std::string> multipart_message::to_vector_string(int skip) const
{
   const auto size = static_cast<int>(this->size());
   std::vector<std::string> result;

   if (size > skip)
   {
      result.reserve(size - skip);
      for (int i = skip; i < size; ++i)
      {
         result.push_back(static_cast<std::string>(this->operator[](i)));
      }
   }

   return result;
}

bool multipart_message::swap(std::size_t a, std::size_t b)
{
   if (a < size() && b < size())
   {
      std::swap(parts[a], parts[b]);
      return true;
   }
   else
   {
      return false;
   }
}

std::size_t multipart_message::size() const
{
   return parts.size();
}

bool multipart_message::empty() const
{
   return parts.empty();
}

zmq::message_t &multipart_message::add_part()
{
   parts.push_back({});
   return parts.back();
}

void multipart_message::pop_front(size_t n)
{
   for (size_t i = 0; i < n; ++i)
      parts.pop_front();
}

void multipart_message::clear()
{
   parts.clear();
}

std::size_t multipart_message::total_byte_count() const
{
   std::size_t len = 0;
   for (auto &part : parts)
   {
      len += part.size();
   }
   return len;
}

bool vbus::recieve_multipart_message_from_socket(zmq::socket_t &socket, multipart_message &msg)
{
   msg.clear();

   bool result = true;

   while (result)
   {
      auto &part = msg.add_part();
      result = !!socket.recv(part, zmq::recv_flags::dontwait);

      if (result)
      {
         if (!part.more()) break;
      }
      else
      {
         msg.clear();
      }
   }

   return result;
}

void vbus::send_multipart_message_to_socket(zmq::socket_t &socket, multipart_message &msg, std::size_t skip_frames)
{

   const auto end_ptr = !msg.empty() ? &msg.parts.back() : nullptr;
   for (auto &part : msg.parts)
   {
      if (skip_frames != 0)
      {
         --skip_frames;
         continue;
      }

      if (&part == end_ptr)
         socket.send(part, zmq::send_flags::none);
      else
         socket.send(part, zmq::send_flags::sndmore);
   }
}

bool vbus::send_multipart_message_to_socket_noblock(zmq::socket_t &socket, multipart_message &msg,
                                                    std::size_t skip_frames)
{
   const auto msg_size = msg.size();

   const auto end_ptr = msg_size > 0 ? &msg.parts.back() : nullptr;
   const auto begin_ptr = msg_size > skip_frames ? &msg.parts[skip_frames] : nullptr;
   bool result = true;
   for (auto &part : msg.parts)
   {
      if (skip_frames != 0)
      {
         --skip_frames;
         continue;
      }

      const zmq::send_flags flags = &part == begin_ptr ? zmq::send_flags::dontwait : zmq::send_flags::none;

      if (&part == end_ptr)
      {
         if (!socket.send(part, flags))
         {
            result = false;
            break;
         }
      }
      else
      {
         if (!socket.send(part, zmq::send_flags::sndmore | flags))
         {
            result = false;
            break;
         }
      }
   }

   return result;
}
