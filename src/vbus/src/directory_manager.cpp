#include "directory_manager.h"
#include <easylogging++.h>

using namespace vbus;

directory_manager::directory_manager(std::string instance_name, duration expiration)
    : instance_name_(instance_name), expiration_(expiration)
{
   public_directory_state_ = std::make_shared<directory_state>(
       hub_connection_string_, std::vector<service_record>{}, instance_name, id_counter_++, pub_sub_connection_string_);
   last_tick_called_ = std::chrono::steady_clock::now();
}

std::shared_ptr<directory_state> directory_manager::get_current_state() const
{
   return public_directory_state_;
}

directory_change_result directory_manager::keep_service_registration(time_point now,
                                                                     service_registration_message const &msg)
{
   auto result = directory_change_result::no_change;

   if (auto it = service_tracking_.find(msg.service_id); it != service_tracking_.end())
   {
      auto &tracking_record = it->second;
      tracking_record.last_update = now;

      if (tracking_record.metadata != msg.metadata)
      {
         result = directory_change_result::directory_changed;
         tracking_record.metadata = msg.metadata;
         LOG(INFO) << "service metadata has changed: " << msg.service_id
                   << " metadata=" << msg.metadata.interface << "\ndata=" << msg.metadata.data;
      }

      if (tracking_record.connection_string != msg.connection_string)
      {
         tracking_record.connection_string = msg.connection_string;
         result = directory_change_result::directory_changed;

         LOG(INFO) << "service connection string has changed: " << msg.service_id << " " << msg.connection_string;
      }

      if (tracking_record.is_offline)
      {
         tracking_record.is_offline = false;
         result = directory_change_result::directory_changed;

         LOG(INFO) << "service is back online : " << msg.service_id;
      }
   }
   else
   {
      auto &tracking = service_tracking_[msg.service_id];
      tracking.connection_string = msg.connection_string;
      tracking.last_update = now;
      tracking.is_offline = false;
      tracking.metadata = msg.metadata;

      result = directory_change_result::directory_changed;
      LOG(INFO) << "service registered : " << msg.service_id;
   }

   if (result == directory_change_result::directory_changed)
   {
      create_new_public_state();
   }

   return result;
}

bool directory_manager::validate_instance_name(std::string const &instance_name) const
{
   return instance_name_ == instance_name;
}

directory_change_result directory_manager::tick(time_point now)
{
   using namespace std::chrono_literals;
   if (now - last_tick_called_ < 10s)
   {
      return directory_change_result::no_change;
   }
   last_tick_called_ = now;

   if (!refresh_expired_services(now))
   {
      return directory_change_result::no_change;
   }
   else
   {
      create_new_public_state();
      return directory_change_result::directory_changed;
   }
}

bool directory_manager::refresh_expired_services(time_point now)
{
   const auto expiration_time = now - expiration_;

   bool result = false;

   for (auto &[id, tracking] : service_tracking_)
   {
      if (!tracking.is_offline && tracking.last_update < expiration_time)
      {
         tracking.is_offline = true;
         result = true;
         LOG(WARNING) << "service connection is expired: " << id << "[" << id << "]";
      }
   }

   return result;
}

void directory_manager::create_new_public_state()
{
   std::vector<service_record> services;
   services.reserve(service_tracking_.size());

   for (auto const &[id, tracking] : service_tracking_)
   {
      services.push_back({id, tracking.connection_string, !tracking.is_offline, tracking.metadata});
   }

   public_directory_state_ = std::make_shared<directory_state>(
       hub_connection_string_, std::move(services), instance_name_, id_counter_++, pub_sub_connection_string_);
}