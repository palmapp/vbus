//
// Created by jhrub on 29.07.2022.
//

#include "cluster.h"
#include "data.h"
#include <boost/algorithm/string/predicate.hpp>
#include <easylogging++.h>
#include <fmt/format.h>
#include <serialization/msgpack.h>

using namespace spider::serialization;

vbus::cluster::cluster(spider2::prometheus::metrics_manager &mng, zmq::context_t &ctx, simple_poller &poller,
                       const vbus::bus_properties &props)

{
   auto id = fmt::format("cluster@{}:{}", props.remote_access_address, props.bind_string.port);

   clients_.reserve(props.cluster.size());

   for (auto &cs : props.cluster)
   {
      auto parsed_cs = connection_string::parse(cs).value();
      if (parsed_cs.host == props.remote_access_address && parsed_cs.port == props.bind_string.port)
      {
         continue;
      }

      auto client_ptr =
         std::unique_ptr<cluster_client>{new cluster_client{{fmt::format("cluster_connection=\"{}\"", cs), mng},
                                                            {ctx, zmq::socket_type::sub},
                                                            {ctx, zmq::socket_type::dealer},
                                                            {},
                                                            cs,
                                                            {}}};

      const std::string_view dir{"/dir"};
      client_ptr->directory.set(zmq::sockopt::subscribe, dir);
      client_ptr->directory.set(zmq::sockopt::rcvtimeo, 0);
      client_ptr->directory.set(zmq::sockopt::heartbeat_ivl, 10000);
      client_ptr->directory.set(zmq::sockopt::heartbeat_timeout, 1000);
      client_ptr->directory.set(zmq::sockopt::heartbeat_ttl, 20000);

      client_ptr->directory_monitor.emplace(fmt::format("cluster_connection=\"{}\",socket=\"directory\"", cs), ctx,
                                            mng);
      client_ptr->directory_monitor->set_info(fmt::format("cluster directory: {}", cs));
      client_ptr->directory_monitor->init(client_ptr->directory,
                                          commons::make_string("inproc://cluster_directory_",
                                                               reinterpret_cast<std::int64_t>(client_ptr.get())));

      client_ptr->directory.connect(cs);

      client_ptr->publish_socket.set(zmq::sockopt::sndhwm, 100000);
      client_ptr->publish_socket.set(zmq::sockopt::linger, 0);
      client_ptr->publish_socket.set(zmq::sockopt::routing_id, id);

      client_ptr->publish_socket_monitor.emplace(fmt::format("cluster_connection=\"{}\",socket=\"publish\"", cs), ctx,
                                                 mng);
      client_ptr->publish_socket_monitor->set_info(fmt::format("cluster publish: {}", cs));
      client_ptr->publish_socket_monitor->init(client_ptr->publish_socket,
                                               commons::make_string("inproc://cluster_publish_",
                                                                    reinterpret_cast<std::int64_t>(client_ptr.get())));

      poller.add(client_ptr->directory_monitor->get_monitor_socket(), zmq::event_flags::pollin,
                 [client = client_ptr.get()]()
                 {
                    client->directory_monitor->recv_events();
                 });

      poller.add(client_ptr->publish_socket_monitor->get_monitor_socket(), zmq::event_flags::pollin,
                 [client = client_ptr.get()]()
                 {
                    client->publish_socket_monitor->recv_events();
                 });

      poller.add(client_ptr->directory, zmq::event_flags::pollin,
                 [client = client_ptr.get(), this]()
                 {
                    auto parse_section_scope = client->metrics.parse_section.increment_scope();

                    if (!recieve_multipart_message_from_socket(client->directory, temp))
                    {
                       client->metrics.directory_state_unable_to_recv.increment();
                       LOG(WARNING) << "unable to receive cluster directory from " << client->main_connection_string;
                       return;
                    }

                    directory_state dir;
                    if (messagepack::parse_msgpack(dir, temp[1]))
                    {
                       check_clock(dir, *client);
                       if (ensure_publish_socket_connection(dir, *client))
                       {
                          flush_pending_registrations(*client);
                       }

                       client->metrics.directory_state_recv.increment();
                    }
                    else
                    {
                       client->metrics.directory_state_errors.increment();
                       LOG(ERROR) << "unable to parse data to directory_state structure / ensure that you are running "
                          "compatible VBUS instance";
                    }
                 });

      clients_.push_back(std::move(client_ptr));
   }
}

void vbus::cluster::broadcast(std::vector<std::string> const &msg)
{
   for (auto &client : clients_)
   {
      auto broadcast_section_scope = client->metrics.broadcast_section.increment_scope();
      send_message(*client, msg);
   }
}

void vbus::cluster::broadcast_service_registration(const std::string &service_id, const std::vector<std::string> &msg)
{

   for (auto &client : clients_)
   {
      auto broadcast_service_registration_section_scope =
         client->metrics.broadcast_service_registration_section.increment_scope();

      if (!send_message(*client, msg))
      {
         client->service_registration_messages_[service_id] = {msg, std::chrono::steady_clock::now()};
      }
   }
}

void vbus::cluster::prepare_to_send(const std::vector<std::string> &msg)
{
   temp.clear();
   for (auto const &part : msg)
   {
      temp.push_part(part);
   }
}

void vbus::cluster::check_clock(const vbus::directory_state &dir, vbus::cluster_client &client) const
{
   auto now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch())
      .count();
   auto real_clock_diff = now - dir.clock;
   auto clock_diff = std::abs(real_clock_diff);

   client.metrics.clock_difference.set(real_clock_diff);

   if (clock_diff > 1000)
   {
      LOG(WARNING) << "cluster clock difference is " << clock_diff << "ms with vbus server "
         << client.main_connection_string;
   }
   else
   {
      LOG(DEBUG) << "cluster clock difference is " << clock_diff << "ms with vbus server "
         << client.main_connection_string;
   }
}

bool vbus::cluster::ensure_publish_socket_connection(const vbus::directory_state &dir,
                                                     vbus::cluster_client &client) const
{
   if (client.hub_connection_string.empty())
   {
      LOG(INFO) << "cluster " << client.main_connection_string << " connecting to publish socket "
         << dir.hub_connection_string;
      client.publish_socket.connect(dir.hub_connection_string);
      client.hub_connection_string = dir.hub_connection_string;

      client.metrics.changed_connection_strings.increment();
   }
   else if (!dir.hub_connection_string.empty() && client.hub_connection_string != dir.hub_connection_string)
   {
      LOG(INFO) << "cluster " << client.main_connection_string << " disconnecting old publish socket at "
         << client.hub_connection_string;
      client.publish_socket.disconnect(client.hub_connection_string);

      LOG(INFO) << "cluster " << client.main_connection_string << " connecting to publish socket "
         << dir.hub_connection_string;
      client.publish_socket.connect(dir.hub_connection_string);
      client.hub_connection_string = dir.hub_connection_string;
      client.metrics.changed_connection_strings.increment();
   }
   else if (dir.hub_connection_string.empty())
   {
      LOG(WARNING) << "cluster " << client.main_connection_string
         << " directory has broadcast an empty publish socket connection string";
   }

   return !client.hub_connection_string.empty();
}

void vbus::cluster::flush_pending_registrations(vbus::cluster_client &client)
{
   if (!client.service_registration_messages_.empty())
   {
      auto now = std::chrono::steady_clock::now();
      for (auto &[service_id, data] : client.service_registration_messages_)
      {
         LOG_IF(now - data.updated < std::chrono::seconds{30}, WARNING)
             << "cluster: " << service_id << " might be obsolete to send";

         send_message(client, data.message);
      }

      client.service_registration_messages_.clear();
   }
}

bool vbus::cluster::send_message(vbus::cluster_client &client, const std::vector<std::string> &msg)
{

   if (client.hub_connection_string.empty())
   {
      LOG(WARNING) << "cluster socket not connected : " << client.main_connection_string;
      client.metrics.not_connected.increment();
      return false;
   }

   prepare_to_send(msg);
   if (!send_multipart_message_to_socket_noblock(client.publish_socket, temp))
   {
      client.metrics.blocked.increment();
      LOG(WARNING) << "cluster socket blocked : " << client.main_connection_string << " PUB "
         << client.hub_connection_string;
      return false;
   }
   return true;
}

vbus::monitoring::monitoring(std::string _labels, spider2::prometheus::metrics_manager &mng)
   : labels(std::move(_labels)), error_labels(labels),
     parse_section(mng.create<spider2::prometheus::gauge>(
        {.name = "vbus_cluster_parse_section", .help = "directory_state processing scope", .labels = labels})),
     broadcast_section(mng.create<spider2::prometheus::gauge>(
        {.name = "vbus_cluster_broadcast_section", .help = "cluster::broadcast scope", .labels = labels})),
     broadcast_service_registration_section(
        mng.create<spider2::prometheus::gauge>({.name = "vbus_cluster_broadcast_service_registration_section",
                                                .help = "cluster::broadcast_service_registration_section scope",
                                                .labels = labels})),
     directory_state_errors(
        mng.create<spider2::prometheus::counter>({.name = "vbus_error", .labels = error_labels.state_errors})),
     directory_state_unable_to_recv(
        mng.create<spider2::prometheus::counter>({.name = "vbus_error", .labels = error_labels.unable_to_recv})),
     blocked(mng.create<spider2::prometheus::counter>({.name = "vbus_error", .labels = error_labels.blocked})),
     not_connected(
        mng.create<spider2::prometheus::counter>({.name = "vbus_error", .labels = error_labels.not_connected})),
     changed_connection_strings(
        mng.create<spider2::prometheus::counter>({.name = "vbus_cluster_hub_connection_string_changed",
                                                  .help = "when vbus is restarted it rebinds to a different endpoint",
                                                  .labels = labels})),
     directory_state_recv(
        mng.create<spider2::prometheus::counter>({.name = "vbus_cluster_directory_state_recv",
                                                  .help = "it is incremented on directory message received",
                                                  .labels = labels})),
     clock_difference(mng.create<spider2::prometheus::gauge>({.name = "vbus_cluster_clock_difference",
                                                              .help = "clock difference between vbus instances",
                                                              .labels = labels}))
{
}

vbus::monitoring::derived_labels::derived_labels(const std::string &base_label)
   : blocked(base_label + ",type=\"blocked\""), not_connected(base_label + ",type=\"not_connected\""),
     state_errors(base_label + ",type=\"state_errors\""), unable_to_recv(base_label + ",type=\"unable_to_recv\"")
{
}
