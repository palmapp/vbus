#include "connection_string.h"
#include <boost/config/warning_disable.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/home/x3.hpp>

BOOST_FUSION_ADAPT_STRUCT(vbus::connection_string, protocol, host, port)

namespace x3 = boost::spirit::x3;
std::optional<vbus::connection_string> vbus::connection_string::parse(std::string_view string)
{
   using x3::char_;
   using x3::phrase_parse;
   const auto integer_parser = x3::ulong_;

   auto it = string.begin();
   auto end = string.end();

   vbus::connection_string output;
   const auto space = char_(" \t");

   const auto result =
       x3::phrase_parse(it, end, (+(char_ - ':') >> "://" >> +(char_ - ':') >> ':' >> integer_parser), space, output);

   if (result)
   {
      return {output};
   }

   return {};
}