//
// Created by jhrub on 26.01.2021.
//

#include "runner.h"
#include <bus.h>
#include <directory_manager.h>
#include <spider2/spider2.h>
#include <zmq.hpp>

using namespace std::chrono_literals;
namespace vbus
{

   class runner_impl
   {
      zmq::context_t ctx_;
      directory_manager dir_;
      bus bus_;
      bus_properties props_;

    public:
      runner_impl(std::string instance_id, bus_properties props)
          : dir_(instance_id, 60s), bus_(dir_, ctx_, props), props_(props)
      {
      }

      void run_forever(commons::cancellation_token ct)
      {
         auto metrics_server = std::jthread{
             [&](std::stop_token cancel)
             {
                using namespace spider2;
                if (props_.prometheus_bind_string.has_value())
                {
                   try
                   {
                      auto io_loop = io::io_context{};
                      auto app = app_context_base{.token = cancel};

                      auto &connection_string = props_.prometheus_bind_string.value();
                      connection_string.protocol = "http";

                      LOG(INFO) << "starting prometheus metrics at "<< connection_string.to_string() << "/metrics";
                      auto ip_address = io::ip::address::from_string(connection_string.host);
                      auto port = gsl::narrow_cast<std::uint16_t>(connection_string.port);

                      auto router =
                          begin_app() + on_get("/metrics", prometheus::create_prometheus_endpoint(bus_.metrics_));

                      run_web_app(io_loop, {.tcp_listen = {tcp::endpoint{ip_address, port}}, .thread_pool_threads = 0, .io_threads = 1}, app, router);
                   }
                   catch (std::exception const &ex)
                   {
                      LOG(ERROR) << "failed to run metrics service at "
                                 << props_.prometheus_bind_string.value().to_string() << " with " << ex.what();
                   }

                   LOG(INFO) << "prometheus metrics terminated";
                }
             }};

         auto run_counter = bus_.metrics_.create<spider2::prometheus::counter>(
             {.name = "vbus_run_counter", .help = "forever incrementing counter"});
         while (!ct.is_cancelled())
         {
            bus_.run_once();
            run_counter.increment();
         }
      }

      std::string const &get_bus_connection_string() const
      {
         return bus_.get_connection_string();
      }
   };
} // namespace vbus

vbus::runner::runner(std::string instance_id, bus_properties props)
    : impl_(new runner_impl(std::move(instance_id), std::move(props)))
{
}

vbus::runner::~runner()
{
   delete impl_;
}

void vbus::runner::run_forever(commons::cancellation_token ct)
{
   impl_->run_forever(std::move(ct));
}
const std::string &vbus::runner::get_bus_connection_string() const
{
   return impl_->get_bus_connection_string();
}
