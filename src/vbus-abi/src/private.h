//
// Created by jhrub on 8/5/2024.
//

#pragma once

namespace vbus
{
    void disable_logging_if_not_explicitly_enabled();
}

