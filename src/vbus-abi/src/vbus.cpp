//
// Created by jhrub on 29.01.2021.
//
#include "vbus.h"
#include "message_holder.h"
#include <boost/asio.hpp>
#include <commons/file_system.h>
#include <commons/guid.h>
#include <commons/run_safe.h>
#include <commons/time_utils.h>
#include <easylogging++.h>
#include <string_view>
#include <vbus/allocator.h>
#include <vbus/client.h>

#include "commons/logging.h"
#include "commons/thread_name.h"
#include "private.h"
#include "vbus_easylogging.hpp"
#include <filesystem>

#if !defined(VBUS_SKIP_INIT_EASYLOGGINGPP)
INITIALIZE_EASYLOGGINGPP
#endif

static std::atomic<bool> g_logger_initialized = false;

namespace
{
   struct directory_status_internal : public vbus_directory_status
   {
      explicit directory_status_internal(const vbus::directory_state &status) : vbus_directory_status()
      {
         status_id = status.id;
         instance_name = to_vbus_string(status.instance_name);
         for (const auto &[name, connection_string, is_online, metadata] : status.services)
         {
            status_list_.push_back({is_online, to_vbus_string(connection_string)});
            metadata_list_.push_back(
                {to_vbus_string(name), to_vbus_string(metadata.interface), to_vbus_string(metadata.data), false});
         }

         services = metadata_list_.data();
         statuses = status_list_.data();

         length = status.services.size();
      }

    private:
      vbus_string_t to_vbus_string(std::string const &str)
      {
         strings_.push_back(str);
         auto &buffer = strings_.back();
         return {buffer.size(), buffer.c_str()};
      }

      std::deque<std::string> strings_;
      std::vector<vbus_service_status> status_list_;
      std::vector<vbus_service_metadata> metadata_list_;
   };

   std::string to_std_string(vbus_string_t const &string)
   {
      std::string result;
      if (string.data != nullptr && string.length > 0)
      {
         result = std::string{string.data, string.length};
      }
      return result;
   }

   std::string_view to_std_string_view(vbus_string_t const &string)
   {
      if (string.data != nullptr && string.length > 0)
      {
         return std::string_view{string.data, string.length};
      }
      else
      {
         return {};
      }
   }

   vbus::multipart_message copy_to_multipart_message(const vbus_string_array_t &message_parts)
   {
      vbus::multipart_message message;
      for (auto i = 0ul; i < message_parts.length; ++i)
      {
         auto &part = message_parts.elements[i];
         message.push_part(to_std_string_view(part));
      }
      return message;
   }

   struct subscription_holder
   {
      subscription_callback callback = nullptr;
      void *userdata = nullptr;
   };

   struct directory_status_changed_callback_holder
   {
      vbus_directory_status_changed_callback callback = nullptr;
      void *userdata = nullptr;
   };

   struct sync_request_frame;

   class vbus_client : public vbus::message_handler
   {
    public:
      vbus_client(std::shared_ptr<vbus::client> client)
          : request_pool_(0xffff), string_pool_(0xffff), client_(std::move(client))
      {
      }

      ~vbus_client()
      {
         if (thread_pool_)
         {
            thread_pool_->stop();
            thread_pool_->join();
         }
      }

      vbus::allocator<vbus_request_t> request_pool_;
      vbus::allocator<std::array<char, 128>> string_pool_;
      vbus::allocator<sync_request_frame> sync_response_pool_;
      vbus::allocator<vbus::request> internal_request_pool_;

      std::unique_ptr<std::thread> running_thread_;
      std::unique_ptr<boost::asio::thread_pool> thread_pool_;

      commons::cancellation_token_source cancellation_token_;
      int64_t default_request_timeout_ms_ = 120000;

      std::atomic<int64_t> request_sequence_counter_ = 0;

      std::atomic<subscription_holder> subscription_callback_{};
      std::atomic<directory_status_changed_callback_holder> directory_callback_{};

      std::vector<vbus_string_t> msg_temp;
      std::shared_ptr<vbus::client> client_;

      vbus_string_array_t create_message_view(vbus::multipart_message &message)
      {
         msg_temp.clear();
         msg_temp.resize(message.size());
         for (std::size_t i = 0; i < message.size(); ++i)
         {
            auto &view = msg_temp[i];
            auto part_view = message[i];

            view.data = part_view.data();
            view.length = part_view.length();
         }

         vbus_string_array_t array;
         array.length = message.size();
         array.elements = msg_temp.data();

         return array;
      }

      void on_subscription_message(vbus::multipart_message &message) final
      {
         auto data = subscription_callback_.load();

         if (data.callback != nullptr)
         {
            data.callback(data.userdata, create_message_view(message));
         }
      }

      void on_response_received(vbus::request_ptr vbus_req, std::int64_t status, vbus::multipart_message &message) final
      {
         auto *abi_request = std::any_cast<vbus_request_t *>(vbus_req->user_data);
         if (abi_request->request_callback != nullptr)
         {
            vbus_response_t res;
            res.status = status;
            res.message = create_message_view(message);

            abi_request->request_callback(abi_request, &res);
         }
         destroy_request(abi_request);
      }

      void on_request_timed_out(vbus::request_ptr vbus_req) final
      {
         auto *abi_request = std::any_cast<vbus_request_t *>(vbus_req->user_data);
         if (abi_request->request_callback != nullptr)
         {
            vbus_response_t vbus_response;
            vbus_response.status = -503;
            vbus_response.message.length = 0;
            vbus_response.message.elements = nullptr;

            abi_request->request_callback(abi_request, &vbus_response);
         }

         destroy_request(abi_request);
      }

      void on_request_cancelled(vbus::request_ptr vbus_req) final
      {
         auto *abi_request = std::any_cast<vbus_request_t *>(vbus_req->user_data);
         if (abi_request->request_callback != nullptr)
         {
            vbus_response_t vbus_response;
            vbus_response.status = -504;
            vbus_response.message.length = 0;
            vbus_response.message.elements = nullptr;

            abi_request->request_callback(abi_request, &vbus_response);
         }

         destroy_request(abi_request);
      }

      void destroy_request(vbus_request_t *req)
      {
         auto *service_id_ptr = reinterpret_cast<typename decltype(string_pool_)::element_type *>(
             const_cast<char *>(req->service_id.data));

         if (string_pool_.is_from(service_id_ptr)) string_pool_.destroy(service_id_ptr);

         request_pool_.destroy(req);
      }

      void on_directory_changed(const vbus::directory_state &dir) override
      {

         auto data = directory_callback_.load();

         if (data.callback != nullptr)
         {
            data.callback(data.userdata, new directory_status_internal(dir));
         }
      }
   };

   class service_server_c_wrapper : public vbus::service_server
   {
    public:
      struct request_holder : public vbus_request_t
      {
         vbus::message_holder message_holder;
         service_server_c_wrapper *service_contex;
         vbus::client_pin_ptr pin_ptr;
      };

      service_server_c_wrapper(const vbus_service &spec, boost::asio::thread_pool *pool)
          : pool_(pool), user_data_(spec.user_data), service_callback_(spec.service_callback),
            id_(to_std_string(spec.metadata.id)), interface_(to_std_string(spec.metadata.interface)),
            data_(to_std_string(spec.metadata.data)), cluster_visible_(spec.metadata.cluster_visible), bind_port_(0)
      {
      }

      service_server_c_wrapper(const vbus_service_v3 &spec, boost::asio::thread_pool *pool)
          : pool_(pool), user_data_(spec.user_data), service_callback_(spec.service_callback),
            id_(to_std_string(spec.metadata.id)), interface_(to_std_string(spec.metadata.interface)),
            data_(to_std_string(spec.metadata.data)), cluster_visible_(spec.metadata.cluster_visible),
            bind_port_(spec.port)
      {
      }

      std::string init(vbus::response_sender &sender) override
      {
         sender_ = &sender;
         return id_;
      }

      void fill_service_metadata(vbus::service_registration_message &reg) override
      {
         reg.cluster_visible = cluster_visible_;
         auto &metadata = reg.metadata;
         metadata.interface = interface_;
         metadata.data = data_;
      }

      void on_request(vbus::request_ptr req, vbus::client_pin_ptr pin) override
      {
         auto *request_proc = request_pool_.construct_raw_ptr();

         request_proc->pin_ptr = std::move(pin);
         request_proc->message_holder.assign_message(req->message);
         request_proc->service_id.data = id_.c_str();
         request_proc->service_id.length = id_.size();

         request_proc->action_id = req->metadata.action_id;
         request_proc->sequence_number = req->metadata.sequence_number;
         std::copy(req->metadata.id.begin(), req->metadata.id.end(), request_proc->request_id.data);
         request_proc->timeout_ms = req->timeout.timeout_ms;
         request_proc->user_data = user_data_;
         request_proc->request_callback = nullptr;
         request_proc->service_contex = this;

         if (pool_)
         {
            boost::asio::post(*pool_, [this, request_proc]() { execute_handler(request_proc); });
         }
         else
         {
            execute_handler(request_proc);
         }
      }

      std::uint16_t get_bind_port() override
      {
         return bind_port_;
      }

      void execute_handler(request_holder *req)
      {
         if (service_callback_ != nullptr)
         {
            commons::run_safe([this, req]()
                              { service_callback_(user_data_, req, req, req->message_holder.get_view()); },
                              "execute_handler");
         }
      }

      void send_response(request_holder *req, int64_t status, vbus_string_array_t message_parts)
      {
         auto response = internal_response_pool_.construct();
         std::copy(req->request_id.data, req->request_id.data + 16, response->request_id.begin());
         response->status = status;
         response->message = copy_to_multipart_message(message_parts);

         sender_->send_response(std::move(response));

         auto pin = std::move(req->pin_ptr);
         request_pool_.destroy(req);
         pin.reset();
      }

      void send_no_response(request_holder *req)
      {
         auto pin = std::move(req->pin_ptr);
         request_pool_.destroy(req);
         pin.reset();
      }

    private:
      boost::asio::thread_pool *pool_;
      void *user_data_;
      vbus_service_callback_ service_callback_;

      std::string id_;
      std::string interface_;
      std::string data_;
      bool cluster_visible_;
      std::uint16_t bind_port_;
      vbus::allocator<request_holder> request_pool_;
      vbus::allocator<vbus::response> internal_response_pool_;

      vbus::response_sender *sender_ = nullptr;
   };

   std::vector<std::unique_ptr<vbus::service_server>> convert_to_service_servers(vbus_service_config config,
                                                                                 boost::asio::thread_pool *pool)
   {
      std::vector<std::unique_ptr<vbus::service_server>> services;
      services.reserve(config.services_length);

      for (std::size_t i = 0; i < config.services_length; ++i)
      {
         vbus_service const &cfg = config.services[i];
         services.emplace_back(new service_server_c_wrapper(cfg, pool));
      }

      return services;
   }

   std::vector<std::unique_ptr<vbus::service_server>> convert_to_service_servers(vbus_service_config_v3 config,
                                                                                 boost::asio::thread_pool *pool)
   {
      std::vector<std::unique_ptr<vbus::service_server>> services;
      services.reserve(config.services_length);

      for (std::size_t i = 0; i < config.services_length; ++i)
      {
         vbus_service_v3 const &cfg = config.services[i];
         services.emplace_back(new service_server_c_wrapper(cfg, pool));
      }

      return services;
   }

   vbus::connection_data parse_connection_data(std::string_view props)
   {
      vbus::connection_data connection_data;
      connection_data.private_ip = "127.0.0.1";
      connection_data.public_ip = "127.0.0.1";
      connection_data.vbus_instance_name = "inproc-dir";

      commons::parse_properties(props.begin(), props.end(),
                                [&connection_data](std::string const &key, std::string const &value) mutable
                                {
                                   if (key == "vbus.service.private.ip")
                                   {
                                      connection_data.private_ip = value;
                                   }
                                   else if (key == "vbus.service.public.ip")
                                   {
                                      connection_data.public_ip = value;
                                   }
                                   else if (key == "vbus.connection.string")
                                   {
                                      connection_data.connection_string = value;
                                   }
                                   else if (key == "vbus.instance" || key == "vbus.instance.name")
                                   {
                                      connection_data.vbus_instance_name = value;
                                   }
                                   else if (key == "vbus.min.recv.time")
                                   {
                                      connection_data.min_recv_time_ = atoi(value.c_str());
                                   }
                                   else if (key == "vbus.client.name")
                                   {
                                      connection_data.client_name = value;
                                   }
                                });
      return connection_data;
   }

   struct sync_request_frame : public vbus_response_t
   {
      std::mutex mutex;
      std::condition_variable cv;
      vbus::message_holder holder;

      void *user_data;
      void (*request_callback)(vbus_request_t *, vbus_response_t *);

      void resolve(vbus_request_t *req)
      {
         req->user_data = user_data;
         req->request_callback = request_callback;

         if (request_callback != nullptr)
         {
            request_callback(req, this);
         }

         cv.notify_one();
      }

      void wait()
      {
         std::unique_lock lk{mutex};
         cv.wait(lk);
      }

      static void on_request_resolved(vbus_request_t *req, vbus_response_t *res)
      {
         auto self = reinterpret_cast<sync_request_frame *>(req->user_data);
         {
            std::unique_lock lk{self->mutex};
            self->status = res->status;
            self->holder.assign_message(res->message);
            self->message = self->holder.get_view();
         }

         self->resolve(req);
      }
   };

   vbus_client *vc_cast(vbus_client_ptr ptr)
   {
      return reinterpret_cast<vbus_client *>(ptr);
   }

   void init_default_loggers()
   {
      el::Loggers::getLogger("service_client");
      el::Loggers::getLogger("service_server");
      el::Loggers::getLogger("publish_client");
      el::Loggers::getLogger("client");
      el::Loggers::getLogger("msg_socket");
   }

} // namespace

void vbus::disable_logging_if_not_explicitly_enabled()
{
   if (!g_logger_initialized)
   {
      init_default_loggers();

      el::Configurations conf;
      conf.setGlobally(el::ConfigurationType::Enabled, std::string("false"));
      el::Loggers::setDefaultConfigurations(conf, true);

      g_logger_initialized = true;
   }
}

void vbus_ignore_logging_initialization()
{
   init_default_loggers();
   g_logger_initialized = true;
}

void vbus_init_logger(vbus_string_t easylogging_properties)
{
   try
   {
      if (!g_logger_initialized)
      {
         init_default_loggers();
      }

      auto props = to_std_string(easylogging_properties);
      std::error_code ec{};
      if (std::filesystem::exists(props, ec))
      {
         el::Configurations conf(props);
         el::Loggers::setDefaultConfigurations(conf, true);

         if (conf.hasConfiguration(el::ConfigurationType::MaxLogFileSize))
         {
            commons::init_easylogging_rolling();
         }
      }
      else
      {
         el::Configurations conf;
         conf.setToDefault();
         conf.parseFromText(props);

         el::Loggers::setDefaultConfigurations(conf, true);

         if (conf.hasConfiguration(el::ConfigurationType::MaxLogFileSize))
         {
            commons::init_easylogging_rolling();
         }
      }

      g_logger_initialized = true;
   }
   catch (std::exception const &ex)
   {
      LOG(ERROR) << "vbus_init_logger failed with : " << ex.what();
   }
   catch (...)
   {
      LOG(ERROR) << "vbus_init_logger failed with : unknown exception";
   }
}

struct vbus_client_holder
{
   vbus_client *client = nullptr;

   ~vbus_client_holder()
   {
      delete client;
   }
};

vbus_client_ptr vbus_create_client(vbus_string_t properties)
{
   vbus::disable_logging_if_not_explicitly_enabled();

   auto holder_ptr = std::make_unique<vbus_client_holder>();
   auto cd = ::parse_connection_data({reinterpret_cast<const char *>(properties.data), properties.length});

   cd.on_destroy = [client_holder = holder_ptr.get()]() { delete client_holder; };

   auto *client_ptr = new vbus_client(vbus::client::make_service_client(std::move(cd)));
   holder_ptr.release()->client = client_ptr;

   return reinterpret_cast<void *>(client_ptr);
}

vbus_client_ptr vbus_create_service(vbus_string_t properties, vbus_service_config config)
{
   vbus::disable_logging_if_not_explicitly_enabled();

   std::unique_ptr<boost::asio::thread_pool> pool;
   if (config.thread_pool_count >= 1)
   {
      pool.reset(new boost::asio::thread_pool(config.thread_pool_count));
   }

   std::vector<std::unique_ptr<vbus::service_server>> services = convert_to_service_servers(config, pool.get());

   auto holder_ptr = std::make_unique<vbus_client_holder>();
   auto cd = ::parse_connection_data({reinterpret_cast<const char *>(properties.data), properties.length});
   cd.on_destroy = [client_holder = holder_ptr.get()]() { delete client_holder; };

   auto *client_ptr = new vbus_client(vbus::client::make_service_server(std::move(cd), std::move(services)));

   holder_ptr.release()->client = client_ptr;

   client_ptr->thread_pool_ = std::move(pool);

   return reinterpret_cast<void *>(client_ptr);
}

vbus_client_ptr vbus_create_service_v3(vbus_string_t properties, vbus_service_config_v3 config)
{
   vbus::disable_logging_if_not_explicitly_enabled();

   std::unique_ptr<boost::asio::thread_pool> pool;
   if (config.thread_pool_count >= 1)
   {
      pool.reset(new boost::asio::thread_pool(config.thread_pool_count));
   }

   std::vector<std::unique_ptr<vbus::service_server>> services = convert_to_service_servers(config, pool.get());

   auto holder_ptr = std::make_unique<vbus_client_holder>();
   auto cd = ::parse_connection_data({reinterpret_cast<const char *>(properties.data), properties.length});
   cd.on_destroy = [client_holder = holder_ptr.get()]() { delete client_holder; };

   auto *client_ptr = new vbus_client(vbus::client::make_service_server(std::move(cd), std::move(services)));

   holder_ptr.release()->client = client_ptr;

   client_ptr->thread_pool_ = std::move(pool);

   return reinterpret_cast<void *>(client_ptr);
}

void vbus_stop_client(vbus_client_ptr client_ptr)
{
   auto *client = vc_cast(client_ptr);

   if (client)
   {
      client->cancellation_token_.cancel();
   }
}

void vbus_free_client(vbus_client_ptr client_ptr)
{
   auto *client = vc_cast(client_ptr);

   if (client)
   {
      LOG(TRACE) << fmt::format("{}: cancelling : exceptions = {}", client->client_->get_client_id(),
                                std::uncaught_exceptions());

      client->cancellation_token_.cancel();

      if (client->running_thread_ != nullptr)
      {
         if (client->running_thread_->joinable())
         {
            client->running_thread_->join();
         }
      }

      client->client_.reset();
   }
}

void vbus_set_request_sequence_counter(vbus_client_ptr client_ptr, int64_t counter)
{
   vc_cast(client_ptr)->request_sequence_counter_ = counter;
}

vbus_request_t *vbus_create_request(vbus_client_ptr client_ptr, vbus_string_t service_id)
{
   if (service_id.length > 127)
   {
      LOG(ERROR) << "method called with service_id.length" << service_id.length << " > 127";
      return nullptr;
   }

   auto *client = vc_cast(client_ptr);
   auto ptr = client->request_pool_.construct_raw_ptr();

   auto request_id = commons::guid::generate_unsafe_uuid();
   std::copy(request_id.begin(), request_id.end(), ptr->request_id.data);

   ptr->sequence_number = client->request_sequence_counter_++;

   auto &str_buffer = *client->string_pool_.construct_raw_ptr();
   std::copy(service_id.data, service_id.data + service_id.length, str_buffer.begin());
   str_buffer[service_id.length] = '\0';

   ptr->service_id.data = str_buffer.data();
   ptr->service_id.length = service_id.length;

   if (client->default_request_timeout_ms_ > 0)
   {
      ptr->timeout_ms = client->default_request_timeout_ms_;
   }
   else
   {
      ptr->timeout_ms = 0;
   }

   return ptr;
}

bool vbus_run_async(vbus_client_ptr client_ptr)
{
   auto *client = vc_cast(client_ptr);
   if (client->running_thread_ == nullptr)
   {
      client->running_thread_ = std::make_unique<std::thread>(
          [client]()
          {
             commons::set_this_thread_name("vbus_run_async");
             commons::run_safe(
                 [client]()
                 { client->client_->run_forever(*client, client->cancellation_token_.get_cancellation_token()); },
                 "vbus_run_async");
          });
   }

   return true;
}

bool vbus_run_step(vbus_client_ptr client_ptr, int64_t ms)
{
   auto *client = vc_cast(client_ptr);
   if (client)
   {
      client->client_->run_for(*client, std::chrono::milliseconds{ms},
                               client->cancellation_token_.get_cancellation_token());
      return true;
   }
   return false;
}

void vbus_free_request(vbus_client_ptr client_ptr, vbus_request_t *vbus_request)
{
   vc_cast(client_ptr)->destroy_request(vbus_request);
}

bool vbus_send_request(vbus_client_ptr client_ptr, vbus_request_t *vbus_request, vbus_string_array_t message_parts)
{
   auto *client_instance = vc_cast(client_ptr);
   if (client_instance == nullptr || vbus_request == nullptr)
   {
      LOG(ERROR) << "invalid arguments";
      return false;
   }

   vbus::request_ptr req = client_instance->internal_request_pool_.construct();

   req->metadata.sequence_number = vbus_request->sequence_number;
   std::copy(vbus_request->request_id.data, vbus_request->request_id.data + 16, req->metadata.id.begin());
   req->metadata.action_id = vbus_request->action_id;

   for (auto i = 0ul; i < message_parts.length; ++i)
   {
      auto &part = message_parts.elements[i];
      req->message.push_part(to_std_string_view(part));
   }

   req->timeout.timeout_ms = vbus_request->timeout_ms;
   req->user_data = vbus_request;

   auto &client = client_instance->client_->get_service_client(to_std_string(vbus_request->service_id));

   try
   {
      client.send_request(std::move(req));
      return true;
   }
   catch (...)
   {
      return false;
   }
}

vbus_response_t *vbus_send_request_sync(vbus_client_ptr client_ptr, vbus_request_t *req,
                                        vbus_string_array_t message_parts)
{
   auto *client_instance = vc_cast(client_ptr);

   auto *vbus_response = client_instance->sync_response_pool_.construct_raw_ptr();
   if (vbus_response == nullptr)
   {
      LOG(ERROR) << "unable to allocate response frame";
      return nullptr;
   }

   vbus_response->user_data = req->user_data;
   vbus_response->request_callback = req->request_callback;

   req->user_data = vbus_response;
   req->request_callback = &sync_request_frame::on_request_resolved;

   if (vbus_send_request(client_ptr, req, message_parts))
   {
      vbus_response->wait();

      return vbus_response;
   }
   else
   {
      vbus_response->status = -504;
   }
   return vbus_response;
}

void vbus_free_response(vbus_client_ptr client_ptr, vbus_response_t *resp)
{
   auto *client_instance = vc_cast(client_ptr);
   client_instance->sync_response_pool_.destroy(static_cast<sync_request_frame *>(resp));
}

void vbus_cancel_request(vbus_client_ptr client_ptr, vbus_request_t *vbus_request)
{
   // not implemented
}

void vbus_subscribe(vbus_client_ptr client_ptr, vbus_string_t channel)
{
   auto *client = vc_cast(client_ptr);
   client->client_->subscribe(to_std_string_view(channel));
}

void vbus_set_subscription_callback(vbus_client_ptr client_ptr, subscription_callback callback, void *userdata)
{
   auto *client = vc_cast(client_ptr);

   subscription_holder holder;
   holder.callback = callback;
   holder.userdata = userdata;

   client->subscription_callback_ = holder;
}

void vbus_set_default_request_timeout(vbus_client_ptr client_ptr, int64_t timeout_in_ms)
{
   auto *client = vc_cast(client_ptr);
   client->default_request_timeout_ms_ = timeout_in_ms;
}

bool vbus_publish(vbus_client_ptr client_ptr, vbus_string_array_t message_parts, bool sticky)
{
   try
   {
      auto *client = vc_cast(client_ptr);
      return client->client_->get_publish_client().publish(copy_to_multipart_message(message_parts),
                                                           sticky ? vbus::publish_client::options::sticky
                                                                  : vbus::publish_client::options::normal);
   }
   catch (...)
   {
      return false;
   }
}

void vbus_send_response(vbus_message_sender_ptr sender, int64_t status, vbus_string_array_t message_parts)
{
   auto *holder = static_cast<service_server_c_wrapper::request_holder *>(sender);
   holder->service_contex->send_response(holder, status, message_parts);
}

void vbus_send_no_response(vbus_message_sender_ptr sender)
{
   auto *holder = static_cast<service_server_c_wrapper::request_holder *>(sender);
   holder->service_contex->send_no_response(holder);
}

vbus_directory_status *vbus_get_directory_status(vbus_client_ptr client_ptr)
{
   auto *client = vc_cast(client_ptr);
   return new directory_status_internal(client->client_->get_current_directory());
}

void vbus_free_directory_status(vbus_directory_status *directory_ptr)
{
   delete static_cast<directory_status_internal *>(directory_ptr);
}

void vbus_set_directory_status_changed_callback(vbus_client_ptr client_ptr,
                                                vbus_directory_status_changed_callback callback, void *userdata)
{
   auto *client = vc_cast(client_ptr);

   directory_status_changed_callback_holder holder;
   holder.callback = callback;
   holder.userdata = userdata;

   client->directory_callback_ = holder;
}

el::base::type::StoragePointer vbus::abi::vbus_get_easylogging_share_ptr()
{
   return el::Helpers::storage();
}