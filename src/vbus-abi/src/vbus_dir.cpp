//
// Created by jhrub on 07.03.2021.
//
#include "vbus.h"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <commons/file_system.h>
#include <commons/properties_parser.h>
#include <commons/thread_name.h>
#include <thread>
#include <vbus/cfg.h>
#include <vbus/connection_string.h>
#include <vbus/runner.h>
#include <xdb/connection.h>
#include <xdb/sqlite/sqlite_driver.h>

#include "private.h"

#include <gsl/narrow>

namespace
{
   struct driver_initializer
   {
      driver_initializer()
      {
         xdb::connection::register_driver(std::make_unique<xdb::backend::sqlite::driver>());
      }
   };

   struct props : public vbus::bus_properties
   {
      std::string instance_name;
   };

   props parse_connection_data(std::string_view props)
   {
      struct props connection_data;
      connection_data.bind_string = {"tcp", "127.0.0.1", 0};
      connection_data.instance_name = "inproc-dir";

      std::string client_connection_string;
      std::string pubsub_bind;
      std::string remote_address;
      std::vector<std::string> vbus_cluster_list{};
      uint16_t pub_sub_bind_port = 0;
      uint16_t router_bind_port = 0;
      commons::parse_properties(props.begin(), props.end(),
                                [&](std::string const &key, std::string const &value) mutable
                                {
                                   if (key == "vbus.server.bind")
                                   {
                                      pubsub_bind = value;
                                   }
                                   else if (key == "vbus.pubsub.port")
                                   {
                                      pub_sub_bind_port = gsl::narrow<uint16_t>(atoi(value.c_str()));
                                   }
                                   else if (key == "vbus.router.port")
                                   {
                                      router_bind_port = gsl::narrow<uint16_t>(atoi(value.c_str()));
                                   }
                                   else if (key == "vbus.remote.address")
                                   {
                                      remote_address = value;
                                   }
                                   else if (key == "vbus.db.connection")
                                   {
                                      connection_data.db_connection_string = value;
                                   }
                                   else if (key == "vbus.instance" || key == "vbus.instance.name")
                                   {
                                      connection_data.instance_name = value;
                                   }
                                   else if (key == "vbus.min.recv.time")
                                   {
                                      connection_data.recv_timeout_milliseconds = atoi(value.c_str());
                                   }
                                   else if (key == "vbus.connection.string")
                                   {
                                      client_connection_string = value;
                                   }
                                   else if (key == "vbus.tracing")
                                   {
                                      if (value == "true" || value == "1")
                                      {
                                         connection_data.tracing_enabled = true;
                                      }
                                   }
                                   else if (key == "vubs.metrics.bind" && !value.empty())
                                   {
                                      connection_data.prometheus_bind_string = vbus::connection_string::parse(value);
                                   }
                                   else if (key == "vbus.cluster.file")
                                   {
                                      if (!value.empty())
                                      {
                                         auto cluster_file = value;
                                         if (boost::filesystem::exists(value))
                                         {
                                            cluster_file = commons::read_file(cluster_file);
                                         }

                                         std::vector<std::string> connection_strings;
                                         boost::split(connection_strings, cluster_file, boost::is_any_of("\n;,"));

                                         for (auto &val : connection_strings)
                                         {
                                            boost::trim(val);
                                            if (vbus::connection_string::parse(val).has_value())
                                            {
                                               vbus_cluster_list.push_back(std::move(val));
                                            }
                                         }
                                      }
                                   }
                                });

      vbus::cfg::fill_default_values(client_connection_string, pubsub_bind, remote_address);

      auto conn_str = vbus::connection_string::parse(pubsub_bind);
      if (conn_str.has_value()) connection_data.bind_string = conn_str.value();

      connection_data.remote_access_address = remote_address;
      connection_data.cluster = std::move(vbus_cluster_list);
      connection_data.pub_sub_bind_port = pub_sub_bind_port;

      connection_data.router_bind_port = router_bind_port;

      return connection_data;
   }

   class vbus_directory
   {
    public:
      vbus_directory(props p) : runner_(p.instance_name, p), props_(p)
      {
         thread_ = std::thread(
             [this]()
             {
                commons::set_this_thread_name("vbus_directory");
                run();
             });
      }

      ~vbus_directory()
      {
         cts_.cancel();
         if (thread_.joinable()) thread_.join();
      }

      void run()
      {
         runner_.run_forever(cts_.get_cancellation_token());
      }

      vbus::runner runner_;
      props props_;

      commons::cancellation_token_source cts_;
      std::thread thread_;
   };
} // namespace

vbus_directory_ptr vbus_run_directory(vbus_string properties)
{

   vbus::disable_logging_if_not_explicitly_enabled();

   static driver_initializer initalizer{};

   auto cfg = parse_connection_data({properties.data, properties.length});

   return static_cast<void *>(new vbus_directory(std::move(cfg)));
}

vbus_string vbus_get_connection_string(vbus_directory_ptr directory)
{
   static std::string temp;
   auto dir_ptr = static_cast<vbus_directory *>(directory);
   auto str = dir_ptr->runner_.get_bus_connection_string();

   auto cs = vbus::connection_string::parse(str);
   if (cs.has_value())
   {
      auto val = cs.value();
      val.host = dir_ptr->props_.bind_string.host;
      temp = val.to_string();
   }
   else
   {
      vbus::connection_string val = dir_ptr->props_.bind_string;
      if (auto sep = str.find_last_of(":"); sep != std::string::npos)
      {
         val.port = atoi(&str.at(sep + 1));
      }

      temp = val.to_string();
   }

   return {temp.size(), temp.data()};
}

void vbus_free_directory(vbus_directory_ptr directory)
{
   delete static_cast<vbus_directory *>(directory);
}