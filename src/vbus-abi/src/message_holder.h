//
// Created by jhrub on 27.02.2021.
//

#pragma once

#include <string_view>
#include <vbus/allocator.h>
#include <vbus/client.h>

namespace vbus
{
   struct message_holder
   {
      using string_buf = std::vector<char>;
      using vbus_string_buf = std::vector<vbus_string>;

      message_holder()
      {
         view.length = 0;
         view.elements = nullptr;
      }

      message_holder(vbus::multipart_message &message)
      {
         assign_message(message);
      }

      message_holder(vbus_string_array_t const &message)
      {
         assign_message(message);
      }

      void assign_message(vbus::multipart_message &message)
      {
         character_buffer.resize(message.total_byte_count() + message.size());
         string_list.resize(message.size());

         char *data_ptr = character_buffer.data();
         for (std::size_t i = 0; i < message.size(); ++i)
         {
            auto part_view = message[i];
            auto &string = string_list[i];

            string.length = part_view.length();
            string.data = data_ptr;

            std::copy(part_view.begin(), part_view.end(), data_ptr);

            data_ptr += part_view.length();
            *data_ptr = '\0';
            ++data_ptr;
         }

         view.length = message.size();
         view.elements = string_list.data();
      }

      void assign_message(vbus_string_array_t const &message)
      {

         character_buffer.resize(count_bytes(message) + message.length);
         string_list.resize(message.length);

         char *data_ptr = character_buffer.data();
         for (std::size_t i = 0; i < message.length; ++i)
         {
            auto &part_view = message.elements[i];
            auto &string = string_list[i];

            string.length = part_view.length;
            string.data = data_ptr;

            std::copy(part_view.data, part_view.data + part_view.length, data_ptr);

            data_ptr += part_view.length;
            *data_ptr = '\0';
            ++data_ptr;
         }

         view.length = message.length;
         view.elements = string_list.data();
      }

      vbus_string_array_t const &get_view() const
      {
         return view;
      }

   private:
      vbus_string_array_t view;

      string_buf character_buffer;
      vbus_string_buf string_list;

      std::size_t count_bytes(vbus_string_array_t const &message)
      {
         std::size_t size = 0;
         for (std::size_t i = 0, len = message.length; i < len; ++i)
         {
            size += message.elements[i].length;
         }

         return size;
      }
   };
} // namespace vbus