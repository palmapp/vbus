//
// Created by jhrub on 05.04.2022.
//

#pragma once

#include "vbus.h"
#include <easylogging++.h>
namespace vbus::abi
{
   VBUS_API el::base::type::StoragePointer vbus_get_easylogging_share_ptr();
}

#if defined(VBUS_STATIC)
#define SHARE_EASYLOGGINGPP_WITH_VBUS
#else
#define SHARE_EASYLOGGINGPP_WITH_VBUS SHARE_EASYLOGGINGPP(vbus::abi::vbus_get_easylogging_share_ptr())
#endif