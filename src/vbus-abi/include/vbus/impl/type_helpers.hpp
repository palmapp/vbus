//
// Created by jhrub on 06.03.2021.
//

#pragma once
#include "../vbus.h"
#include <memory>
#include <string>
namespace vbus::abi
{

   struct request_deleter
   {
      vbus_client_ptr client_;
      inline void operator()(vbus_request_t *req)
      {
         if (req != nullptr) vbus_free_request(client_, req);
      }
   };

   struct response_deleter
   {
      vbus_client_ptr client_;

      inline void operator()(vbus_response_t *res)
      {
         if (res != nullptr) vbus_free_response(client_, res);
      }
   };

   struct directory_status_deleter
   {
      inline void operator()(vbus_directory_status *status)
      {
         if (status != nullptr) vbus_free_directory_status(status);
      }
   };

   using request_ptr = std::unique_ptr<vbus_request_t, request_deleter>;
   using response_ptr = std::unique_ptr<vbus_response_t, response_deleter>;
   using directory_status_ptr = std::unique_ptr<vbus_directory_status, directory_status_deleter>;

   inline std::string to_string(vbus_uuid_t const &id)
   {
      std::string str;
      str.resize(16);

      std::copy(id.data, id.data + 16, reinterpret_cast<uint8_t *>(str.data()));
      return str;
   }

} // namespace vbus::abi
