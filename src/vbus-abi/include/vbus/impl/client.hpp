//
// Created by jhrub on 06.03.2021.
//

#pragma once
#include "directory_status_handler.hpp"
#include "response_handler.hpp"
#include "string_array.hpp"
#include "subcription_handler.hpp"
#include "type_helpers.hpp"

namespace vbus::abi
{

   class client
   {
    public:
      inline static void init_easylogging(std::string_view view)
      {
         vbus_init_logger(vbus_string_t{view.size(), view.data()});
      }

      inline explicit client(vbus_client_ptr client) : client_ptr_(client) {}

      inline client(std::string_view properties) : client_ptr_(vbus_create_client(to_vbus_string_view(properties))) {}

      inline client(std::string_view properties, vbus_service_config_v3 const &config)
          : client_ptr_(vbus_create_service_v3(to_vbus_string_view(properties), config))
      {
      }

      inline client(client &&cl) : client_ptr_(cl.client_ptr_)
      {
         cl.client_ptr_ = nullptr;
      }

      inline client()
      {
         client_ptr_ = nullptr;
      }

      inline client &operator=(client &&cl)
      {
         if (client_ptr_)
         {
            vbus_free_client(client_ptr_);
         }

         client_ptr_ = cl.client_ptr_;
         cl.client_ptr_ = nullptr;
         return *this;
      }

      client(const client &) = delete;

      inline ~client()
      {
         if (client_ptr_ != nullptr) vbus_free_client(client_ptr_);
      }

      inline void set_sequence_counter(std::int64_t sequence_counter)
      {
         vbus_set_request_sequence_counter(client_ptr_, sequence_counter);
      }

      inline void set_subscription_handler(subscription_handler &handler)
      {
         vbus_set_subscription_callback(client_ptr_, &client::on_subscription_message, &handler);
      }

      inline void set_directory_status_handler(directory_status_handler &handler)
      {
         vbus_set_directory_status_changed_callback(client_ptr_, &client::on_directory_status_changed, &handler);
      }

      inline void subscribe(std::string_view channel)
      {
         vbus_subscribe(client_ptr_, to_vbus_string_view(channel));
      }

      inline void run_async()
      {
         if (!vbus_run_async(client_ptr_))
         {
            throw std::logic_error{"vbus_run_async call has failed"};
         }
      }

      inline directory_status_ptr get_directory_status()
      {
         return {vbus_get_directory_status(client_ptr_), {}};
      }

      inline request_ptr create_request(std::string_view service_id)
      {
         return {vbus_create_request(client_ptr_, to_vbus_string_view(service_id)), {client_ptr_}};
      }

      inline request_ptr create_request(std::string_view service_id, response_handler &handler)
      {
         auto request = request_ptr{vbus_create_request(client_ptr_, to_vbus_string_view(service_id)), {client_ptr_}};

         request->user_data = &handler;
         request->request_callback = &client::on_response_received;

         return request;
      }

      inline bool publish(vbus_string_array message_parts, bool sticky)
      {
         return vbus_publish(client_ptr_, message_parts, sticky);
      }

      template <class MessageT>
      bool publish(MessageT const &msg, bool sticky)
      {
         bool result = false;
         to_vbus_string_array([this, sticky, &result](vbus_string_array const &array)
                              { result = vbus_publish(client_ptr_, array, sticky); }, msg);
         return result;
      }

      inline vbus_client_ptr native_client_ptr() const
      {
         return client_ptr_;
      }

      inline void set_default_request_timeout(std::int64_t timeout_in_ms)
      {
         vbus_set_default_request_timeout(client_ptr_, timeout_in_ms);
      }

      template <class MessageT>
      void send_request(request_ptr req, MessageT const &msg)
      {
         to_vbus_string_array([this, mv_req = std::move(req)](vbus_string_array const &array) mutable
                              { vbus_send_request(client_ptr_, mv_req.release(), array); }, msg);
      }

      void send_request(request_ptr req)
      {
         vbus_send_request(client_ptr_, req.release(), {0, nullptr});
      }

      template <class MessageT>
      response_ptr send_request_sync(request_ptr req, MessageT &msg)
      {
         vbus_response_t *response = nullptr;
         to_vbus_string_array([this, &response, mv_req = std::move(req)](vbus_string_array const &array) mutable
                              { response = vbus_send_request_sync(client_ptr_, mv_req.release(), array); }, msg);

         return {response, {client_ptr_}};
      }

      inline response_ptr send_request_sync(request_ptr req)
      {
         vbus_response_t *response = vbus_send_request_sync(client_ptr_, req.release(), {0, nullptr});

         return {response, {client_ptr_}};
      }

    protected:
      static void on_subscription_message(void *userdata, vbus_string_array message) noexcept
      {
         static_cast<subscription_handler *>(userdata)->on_subscription_message(string_array{message});
      }

      static void on_response_received(vbus_request_t *request, vbus_response_t *response) noexcept
      {
         static_cast<response_handler *>(request->user_data)->on_response(*request, *response);
      }

      static void on_directory_status_changed(void *userdata, vbus_directory_status *status) noexcept
      {
         static_cast<directory_status_handler *>(userdata)->on_directory_changed({status, {}});
      }

      vbus_client_ptr client_ptr_;
   };
} // namespace vbus::abi