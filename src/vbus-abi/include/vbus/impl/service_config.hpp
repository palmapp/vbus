//
// Created by jhrub on 07.03.2021.
//

#pragma once
#include "string_array.hpp"
#include "type_helpers.hpp"
#include <deque>
#include <vector>

namespace vbus::abi
{
   class response_sender
   {
    public:
      inline response_sender(vbus_message_sender_ptr sender = nullptr) noexcept : sender_(sender) {}

      inline response_sender(response_sender &&other) noexcept : sender_(other.sender_)
      {
         other.sender_ = nullptr;
      }

      inline response_sender(response_sender const &) = delete;

      response_sender &operator=(response_sender &&other) noexcept
      {
         std::swap(other.sender_, sender_);
         std::swap(other.response_sent_, response_sent_);
         return *this;
      }

      response_sender &operator=(const response_sender &) = delete;

      inline ~response_sender()
      {
         if (!response_sent_ && sender_ != nullptr) vbus_send_no_response(sender_);
      }

      inline void send_response(std::int64_t status, vbus_string_array message_parts) noexcept
      {
         vbus_send_response(sender_, status, message_parts);
         response_sent_ = true;
      }

      template <class MessageT>
      void send_response(std::int64_t status, const MessageT &message)
      {
         to_vbus_string_array([this, status](vbus_string_array message_parts)
                              { this->send_response(status, message_parts); }, message);
      }

      inline void send_response(std::int64_t status) noexcept
      {
         vbus_string_array message_parts{0, nullptr};
         vbus_send_response(sender_, status, message_parts);
         response_sent_ = true;
      }

      inline void swap(response_sender &other)
      {
         std::swap(other.sender_, sender_);
         std::swap(other.response_sent_, response_sent_);
      }

    private:
      vbus_message_sender_ptr sender_;
      bool response_sent_ = false;
   };

   class service
   {
    public:
      virtual ~service() = default;
      virtual void on_request(response_sender &sender, vbus_request_t *request,
                              string_array const &message) noexcept = 0;
   };

   template <class Funct>
   class functor_service : public service
   {
    public:
      functor_service(Funct &&funct) : funct_(std::forward<Funct>(funct)) {}

      inline void on_request(response_sender &sender, vbus_request_t *request,
                             string_array const &message) noexcept final
      {
         funct_(sender, request, message);
      }

    private:
      Funct funct_;
   };

   template <class Funct>
   functor_service<Funct> make_service(Funct &&funct)
   {
      return functor_service<Funct>{std::forward<Funct>(funct)};
   }

   template <class Funct>
   std::shared_ptr<functor_service<Funct>> make_shared_service(Funct &&funct)
   {
      return std::make_shared<functor_service<Funct>>(std::forward<Funct>(funct));
   }

   struct service_metadata
   {
      std::string id;
      std::string interface = {};
      std::string data = {};
      bool cluster_visible = false;
      std::uint16_t port = 0;
   };

   class service_config
   {
    public:
      inline service_config() {}

      inline service_config &use_thread_pool(std::size_t count)
      {
         thread_pool_size_ = count;
         return *this;
      }

      inline vbus_service_config_v3 config_view()
      {
         return {services_.size(), services_.data(), thread_pool_size_};
      }

      inline service_config &register_service(service_metadata metadata, service &service_impl)
      {
         metadata_.push_back(std::move(metadata));
         auto &metadata_ref = metadata_.back();

         services_.push_back(vbus_service_v3{
             vbus_service_metadata{to_vbus_string_view(metadata_ref.id), to_vbus_string_view(metadata_ref.interface),
                                   to_vbus_string_view(metadata_ref.data), metadata.cluster_visible},
             static_cast<void *>(&service_impl), &service_config::on_request, metadata.port});
         return *this;
      }

    private:
      inline static void on_request(void *context, vbus_message_sender_ptr sender_context, vbus_request_t *request,
                                    vbus_string_array message) noexcept
      {
         auto *service_ptr = static_cast<service *>(context);
         response_sender sender{sender_context};
         service_ptr->on_request(sender, request, string_array{message});
      }

      std::deque<service_metadata> metadata_;
      std::vector<vbus_service_v3> services_;
      std::size_t thread_pool_size_ = 0;
   };

} // namespace vbus::abi