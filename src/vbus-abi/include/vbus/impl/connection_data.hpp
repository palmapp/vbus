//
// Created by jhrub on 07.03.2021.
//

#pragma once
#include <iostream>

#include <sstream>
#include <string>
#include <utility>

namespace vbus::abi
{
   struct connection_data
   {
      std::string connection_string;
      std::string vbus_instance_name;

      std::string public_ip = {};
      std::string private_ip = {};

      int min_recv_time_ = -1;

      inline static connection_data make_client(std::string connection_string, std::string vbus_instance_name)
      {
         return {std::move(connection_string), std::move(vbus_instance_name)};
      }

      inline static connection_data make_service(std::string connection_string, std::string vbus_instance_name,
                                                 std::string public_ip, std::string private_ip)
      {
         return {std::move(connection_string), std::move(vbus_instance_name), std::move(public_ip),
                 std::move(private_ip)};
      }

      inline std::string to_properies() const
      {
         std::stringstream ss;
         ss << "vbus.connection.string = " << connection_string << "\n";
         ss << "vbus.instance = " << vbus_instance_name << "\n";

         if (!public_ip.empty()) ss << "vbus.service.public.ip = " << public_ip << "\n";

         if (!public_ip.empty()) ss << "vbus.service.private.ip = " << private_ip << "\n";
         if (min_recv_time_ >= 0) ss << "vbus.min.recv.time = " << min_recv_time_ << "\n";

         return ss.str();
      }
   };
} // namespace vbus::abi