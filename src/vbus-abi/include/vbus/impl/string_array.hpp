//
// Created by jhrub on 06.03.2021.
//

#pragma once
#include "../vbus.h"
#include <algorithm>
#include <array>
#include <stdexcept>
#include <string_view>

namespace vbus::abi
{
   class string_array
   {
    public:
      inline string_array(vbus_string_array_t const &array) : array_(array) {}

      struct iterator
      {
         using iterator_category = std::forward_iterator_tag;
         using value_type = std::string_view;
         using difference_type = std::ptrdiff_t;
         using pointer = std::string_view *;
         using reference = std::string_view &;

         inline void operator++()
         {
            ++ptr;
         }

         inline std::string_view operator*() const
         {
            if (ptr != nullptr && ptr->length > 0 && ptr->data != nullptr)
               return {ptr->data, ptr->length};
            else
               return {};
         }
         inline bool operator!=(iterator const &it) const
         {
            return ptr != it.ptr;
         }
         inline bool operator==(iterator const &it) const
         {
            return ptr == it.ptr;
         }

         inline iterator(vbus_string_t const *_ptr) : ptr(_ptr) {}
         inline iterator(const iterator &it) : ptr(it.ptr) {}
         inline const iterator &operator=(const iterator &it)
         {
            ptr = it.ptr;
            return *this;
         }

         vbus_string_t const *ptr;
      };

      inline iterator begin() const
      {
         return {array_.elements};
      }

      inline iterator end() const
      {
         return {array_.elements + array_.length};
      }

      inline std::size_t size() const
      {
         return array_.length;
      }

      inline std::string_view operator[](std::size_t idx) const
      {
         if (idx < size())
         {
            auto const &str = array_.elements[idx];
            if (str.length == 0 || str.data == nullptr)
            {
               return {};
            }
            else
            {
               return {str.data, str.length};
            }
         }
         else
         {
            return {};
         }
      }

    private:
      vbus_string_array_t const &array_;
   };

   inline vbus_string_t to_vbus_string_view(std::string_view str)
   {
      return {str.length(), str.data()};
   }

   inline std::string_view to_std_string_view(vbus_string_t const &str)
   {
      return {str.data, str.length};
   }

   template <class FunctT>
   void to_vbus_string_array(FunctT funct, std::string_view str)
   {
      auto view = to_vbus_string_view(str);
      vbus_string_array array{1, &view};

      funct(array);
   }

   template <class FunctT, class ContainerT>
   void to_vbus_string_array(FunctT funct, ContainerT &cnt)
   {
      std::array<vbus_string, 1024> string_views;
      vbus_string_array array;

      if constexpr (std::is_same_v<std::string_view, std::decay_t<ContainerT>> ||
                    std::is_same_v<std::string, std::decay_t<ContainerT>>)
      {
         array.length = 1;
         array.elements = string_views.data();

         auto &view = string_views[0];
         view.length = cnt.size();
         view.data = cnt.data();
      }
      else
      {
         array.length = std::size(cnt);
         array.elements = string_views.data();

         if (array.length > string_views.size())
         {
            throw std::logic_error("unable to fit container to vbus_string_array_t with max capacity 1024");
         }

         std::size_t i = 0;
         for (auto &str : cnt)
         {
            auto &view = string_views[i++];
            view.length = std::size(str);
            view.data = str.data();
         }
      }

      funct(array);
   }

} // namespace vbus::abi