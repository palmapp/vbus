//
// Created by jhrub on 07.03.2021.
//

#pragma once
#include "../vbus.h"
#include "string_array.hpp"
#include <memory>
#include <sstream>

namespace vbus::abi
{

   class directory_service
   {
    public:
      inline directory_service(std::string_view props) : directory_ptr_(vbus_run_directory(to_vbus_string_view(props)))
      {
      }
      inline directory_service() : directory_ptr_(vbus_run_directory({0, nullptr})) {}
      inline ~directory_service()
      {
         if (directory_ptr_ != nullptr) vbus_free_directory(directory_ptr_);
      }
      inline directory_service(directory_service &&other) : directory_ptr_(other.directory_ptr_)
      {
         other.directory_ptr_ = nullptr;
      }

      directory_service(const directory_service &) = delete;
      directory_service &operator=(directory_service const &other) = delete;
      inline directory_service &operator=(directory_service &&other)
      {
         std::swap(directory_ptr_, other.directory_ptr_);
         return *this;
      }

      inline std::string_view get_connection_string()
      {
         vbus_string str = vbus_get_connection_string(directory_ptr_);

         return {str.data, str.length};
      }

      inline std::string get_inprocess_connection_props()
      {
         std::stringstream ss;
         ss << "vbus.instance.name = inproc-dir\nvbus.connection.string=";
         ss << get_connection_string();

         return ss.str();
      }

    private:
      vbus_directory_ptr directory_ptr_;
   };

} // namespace vbus::abi