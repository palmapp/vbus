//
// Created by jhrub on 07.03.2021.
//

#pragma once
#include "../vbus.h"
#include "string_array.hpp"

namespace vbus::abi
{
   class subscription_handler
   {
    public:
      virtual ~subscription_handler() = default;
      virtual void on_subscription_message(const string_array &message) noexcept = 0;
   };

   template <class Funct>
   class functor_subscription_handler : public subscription_handler
   {
    public:
      functor_subscription_handler(Funct &&funct) : funct_(std::forward<Funct>(funct)) {}

      inline void on_subscription_message(const string_array &message) noexcept final
      {
         funct_(message);
      }

    private:
      Funct funct_;
   };

   template <class Funct>
   functor_subscription_handler<Funct> make_subscription_handler(Funct &&funct)
   {
      return functor_subscription_handler<Funct>{std::forward<Funct>(funct)};
   }
} // namespace vbus::abi