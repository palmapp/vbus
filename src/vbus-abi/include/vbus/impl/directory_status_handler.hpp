//
// Created by jhrub on 07.03.2021.
//

#pragma once
#include "../vbus.h"
#include "string_array.hpp"
#include "type_helpers.hpp"

namespace vbus::abi
{

   class directory_status_handler
   {
    public:
      virtual ~directory_status_handler() = default;
      virtual void on_directory_changed(directory_status_ptr status_ptr) noexcept = 0;
   };

   template <class Funct>
   class functor_directory_status_handler : public directory_status_handler
   {
    public:
      functor_directory_status_handler(Funct &&funct) : funct_(std::forward<Funct>(funct)) {}

      inline void on_directory_changed(directory_status_ptr status_ptr) noexcept final
      {
         funct_(std::move(status_ptr));
      }

    private:
      Funct funct_;
   };

   template <class Funct>
   functor_directory_status_handler<Funct> make_directory_status_handler(Funct &&funct)
   {
      return functor_directory_status_handler<Funct>{std::forward<Funct>(funct)};
   }
} // namespace vbus::abi