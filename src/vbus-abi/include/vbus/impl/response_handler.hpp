//
// Created by jhrub on 07.03.2021.
//

#pragma once
#include "../vbus.h"

namespace vbus::abi
{
   class response_handler
   {
    public:
      virtual ~response_handler() = default;
      virtual void on_response(vbus_request_t &request, vbus_response_t &response) noexcept = 0;
   };

   template <class Funct>
   class functor_response_handler : public response_handler
   {
    public:
      functor_response_handler(Funct &&funct) : funct_(std::forward<Funct>(funct)) {}

      inline void on_response(vbus_request_t &request, vbus_response_t &response) noexcept final
      {
         funct_(request, response);
      }

    private:
      Funct funct_;
   };

   template <class Funct>
   functor_response_handler<Funct> make_response_handler(Funct &&funct)
   {
      return functor_response_handler<Funct>{std::forward<Funct>(funct)};
   }
} // namespace vbus::abi