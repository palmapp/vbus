//
// Created by jhrub on 07.05.2021.
//

#pragma once
#include "type_helpers.hpp"
#include <stdexcept>

namespace vbus::abi
{
   class directory_status_iterator
   {
    public:
      inline directory_status_iterator(vbus_directory_status const &status) : status_(status) {}

      struct service_status_view
      {
         vbus_service_metadata &metadata;
         vbus_service_status &status;
      };

      struct iterator
      {
         inline void operator++()
         {
            ++index;
         }

         inline service_status_view operator*() const
         {
            return {status_.services[index], status_.statuses[index]};
         }

         inline bool operator!=(iterator const &it) const
         {
            return index != it.index;
         }
         inline bool operator==(iterator const &it) const
         {
            return index == it.index;
         }

         size_t index;
         vbus_directory_status const &status_;
      };

      inline iterator begin() const
      {
         return {0, status_};
      }

      inline iterator end() const
      {
         return {status_.length, status_};
      }

      inline std::size_t size() const
      {
         return status_.length;
      }

      inline service_status_view operator[](std::size_t idx) const
      {
         if (idx < size())
         {
            return {status_.services[idx], status_.statuses[idx]};
         }
         else
         {
            throw std::out_of_range("vbus_directory_status index is out of bounds");
         }
      }

    private:
      vbus_directory_status const &status_;
   };

} // namespace vbus::abi
