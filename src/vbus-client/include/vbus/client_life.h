//
// Created by jhrub on 01.11.2021.
//

#pragma once

#include <memory>
namespace vbus
{
   /// tag interface to extend vbus client life
   class client_life
   {
    public:
      virtual ~client_life() = default;
   };

   using client_weak_ptr = std::weak_ptr<client_life>;
   using client_pin_ptr = std::shared_ptr<client_life>;

} // namespace vbus