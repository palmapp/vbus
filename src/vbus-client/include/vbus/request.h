//
// Created by jhrub on 17.01.2021.
//

#pragma once
#include <any>
#include <chrono>
#include <commons/cancellation_token.h>
#include <cstdint>
#include <vbus/data.h>
#include <vbus/multipart_message.h>

#include "allocator.h"

namespace vbus
{
   struct request
   {
      request_metadata_frame metadata{};
      multipart_message message{};

      request_timeout_frame timeout{};

      commons::cancellation_token ct{};
      std::any user_data{};
   };

   using request_ptr = typename allocator<request>::ptr_type;
} // namespace vbus