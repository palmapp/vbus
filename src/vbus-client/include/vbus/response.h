//
// Created by jhrub on 25.01.2021.
//

#pragma once
#include "allocator.h"
#include <vbus/data.h>
#include <vbus/multipart_message.h>
namespace vbus
{
   struct response
   {
      std::array<std::uint8_t, 16> request_id{};
      multipart_message message{};
      std::int64_t status = 0;
   };

   using response_ptr = typename allocator<response>::ptr_type;
} // namespace vbus