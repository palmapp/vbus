//
// Created by jhrub on 29.07.2022.
//

#pragma once

#include <atomic>
#include <boost/unordered_map.hpp>
#include <chrono>
#include <mutex>
namespace vbus
{
   class sync_clock
   {
    public:
      std::int64_t to_remote(std::chrono::steady_clock::time_point local);
      std::chrono::steady_clock::time_point to_local(std::int64_t remote);

      std::int64_t remote_now();
      void sync(std::int64_t remote_clock,
                std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now());

      sync_clock(std::int64_t remote_clock = 0);

    private:
      struct sync_state
      {
         std::chrono::steady_clock::time_point synced_at;
         std::int64_t sync_time_ms;
      };

      std::mutex m_;
      sync_state state_;

      sync_state get_state();
   };

} // namespace vbus