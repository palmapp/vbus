#pragma once
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_hash.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <map>
#include <memory>
#include <string>
#include <string_view>
#include <vbus/multipart_message.h>
#include <vector>
#include <zmq.hpp>

namespace vbus
{
   using socket_ptr = std::shared_ptr<zmq::socket_t>;
   using uuid = boost::uuids::uuid;

} // namespace vbus