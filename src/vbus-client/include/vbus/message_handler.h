//
// Created by jhrub on 21.01.2021.
//

#pragma once

#include "request.h"
#include <any>
#include <vbus/data.h>
#include <vbus/multipart_message.h>

namespace vbus
{
   class message_handler
   {
    public:
      virtual ~message_handler() = default;

      virtual void on_subscription_message(multipart_message &message) = 0;

      virtual void on_response_received(request_ptr request, std::int64_t status, multipart_message &message) = 0;
      virtual void on_request_timed_out(request_ptr request) = 0;
      virtual void on_request_cancelled(request_ptr request) = 0;

      virtual void on_directory_changed(directory_state const &dir) {}
   };
} // namespace vbus