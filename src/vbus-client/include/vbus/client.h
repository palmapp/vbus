//
// Created by jhrub on 10.12.2020.
//

#pragma once

#include <commons/application_properties.h>
#include <commons/cancellation_token.h>
#include <string_view>

#include "default_types.h"
#include "message_handler.h"
#include "publish_client.h"
#include "service_client.h"
#include "service_server.h"

#include "client_life.h"
#include "sync_clock.h"
#include <functional>
#include <serialization/serialization.h>
#include <vbus/simple_poller.h>
#include <zmq.hpp>

namespace vbus
{
   class service_client_impl;
   class publish_client_impl;
   class service_server_impl;

   struct connection_data
   {
      std::string connection_string;
      std::string vbus_instance_name;

      std::string public_ip;
      std::string private_ip;

      int min_recv_time_ = -1;

      std::function<void()> on_destroy{};

      std::string client_name;
   };

   class client_event_monitor;

   class client : public client_life, public std::enable_shared_from_this<client>
   {
   public:
      static std::shared_ptr<client> make_service_client(connection_data data);
      static std::shared_ptr<client> make_service_server(connection_data data,
                                                         std::vector<std::unique_ptr<service_server>> services);
      ~client() final;

      void shutdown();

      std::vector<service_client *> get_service_clients_by_interface(std::string_view interface,
                                                                     bool must_be_online = true);
      directory_state get_current_directory() const;

      service_client &get_service_client(std::string_view service_id);
      publish_client &get_publish_client();

      void subscribe(std::string_view channel);

      void run_for(message_handler &handler, std::chrono::steady_clock::duration duration,
                   commons::cancellation_token token);

      void run_forever(message_handler &handler, commons::cancellation_token token);

      inline std::string get_client_id() const
      {
         return client_id_;
      }

   private:
      explicit client(connection_data data);
      void connect();

      void register_services(std::vector<std::unique_ptr<service_server>> services);
      bool try_process_directory_changes(const multipart_message &message);

      service_client_impl &get_service_client_internal(std::string const &service_id);
      service_client_impl *try_get_service_client_internal(std::string const &service_id);

      std::vector<service_client_impl *> &get_clients_to_process();

      void subscribe(zmq::socket_t &socket, std::string_view channel);

      void on_read_directory_socket();
      void on_read_subscription_socket();
      mutable std::mutex lock_;

      sync_clock clock_;
      bool connected_ = false;
      bool pub_sub_connected_ = false;
      bool got_subscriptions_ = false;

      zmq::context_t context_;
      zmq::socket_t directory_socket_;
      zmq::socket_t subscription_socket_;

      connection_data data_;

      std::unique_ptr<client_event_monitor> event_monitor_;
      std::unique_ptr<publish_client_impl> publish_client_;


      std::string client_id_;

      directory_state directory_;

      std::condition_variable cv_;
      std::atomic<bool> purge_finished_ = true;

      std::unordered_map<std::string, std::unique_ptr<service_client_impl>> clients_;
      std::unordered_map<std::string, std::string> client_connection_strings_;

      std::vector<service_client_impl *> client_list_;
      std::size_t last_client_list_size_ = 0;
      std::vector<service_client_impl *> tmp_client_list_;
      std::vector<service_client_impl *> rcv_client_list_;

      std::vector<std::unique_ptr<service_server_impl>> services_;
      std::vector<service_registration_message> keep_alive_service_messages_;

      simple_poller poller_;
      std::chrono::steady_clock::time_point last_keep_alive_message_ = {};
      fmt::memory_buffer dir_msg_buffer_;

      std::int64_t state_counter_ = 0;

      vbus::message_handler *temp_message_handler_ = nullptr;
      multipart_message temp_message_;

      void install_poller_service_client_handlers(std::vector<service_client_impl *> &clients);

      std::shared_ptr<client_life> connected_object{};
   };
} // namespace vbus