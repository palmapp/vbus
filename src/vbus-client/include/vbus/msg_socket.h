//
// Created by jhrub on 22.01.2021.
//

#pragma once
#include <zmq.hpp>
namespace vbus
{
   /// this class sets common properties on socket and have the reconnection logic
   /// when services changes
   struct msg_socket
   {
      msg_socket(zmq::context_t &ctx, zmq::socket_type type);
      virtual ~msg_socket() = default;
      void set_connection_string(std::string connection_string);

      bool try_connect();
      bool has_connection_string() const;

      inline zmq::socket_t &get_socket()
      {
         return socket;
      }

    protected:
      zmq::socket_t socket;
      bool connected_ = false;

    private:
      std::string connection_string_;
   };
} // namespace vbus