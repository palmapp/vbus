//
// Created by jhrub on 21.01.2021.
//

#pragma once
#include <boost/pool/object_pool.hpp>
#include <memory>
#include <mutex>
#include <thread>

namespace vbus
{

   template <class ObjectT, class MutexT = std::mutex>
   class allocator
   {
    public:
      struct object_dealocator
      {
         allocator<ObjectT> *allocator_ptr;

         inline void operator()(ObjectT *req)
         {
            allocator_ptr->destroy(req);
         }
      };
      using ptr_type = std::unique_ptr<ObjectT, object_dealocator>;
      using element_type = ObjectT;

      allocator(std::size_t count = 0xffff) : pool_(count) {}

      ptr_type construct()
      {
         std::lock_guard lock{mutex_};
         return {pool_.construct(), object_dealocator{this}};
      }

      ObjectT *construct_raw_ptr()
      {
         std::lock_guard lock{mutex_};
         return pool_.construct();
      }

      void destroy(ObjectT *ptr)
      {
         std::lock_guard lock{mutex_};
         return pool_.destroy(ptr);
      }

      bool is_from(ObjectT *ptr) const
      {
         std::lock_guard lock{mutex_};
         return pool_.is_from(ptr);
      }

    private:
      mutable MutexT mutex_;
      boost::object_pool<ObjectT> pool_;
   };
} // namespace vbus
