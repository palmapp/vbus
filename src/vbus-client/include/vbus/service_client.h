//
// Created by jhrub on 14.12.2020.
//

#pragma once
#include "request.h"
#include <commons/cancellation_token.h>
#include <optional>
#include <vbus/multipart_message.h>

namespace vbus
{
   class service_client
   {
    public:
      virtual ~service_client() = default;

      virtual void send_request(request_ptr request) = 0;
   };
} // namespace vbus