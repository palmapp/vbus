//
// Created by jhrub on 22.01.2021.
//

#pragma once
#include <vbus/multipart_message.h>

namespace vbus
{
   class publish_client
   {
    public:
      enum class options
      {
         normal,
         sticky
      };
      virtual ~publish_client() = default;
      virtual bool publish(multipart_message message, options op = options::normal) = 0;
   };
} // namespace vbus