//
// Created by jhrub on 25.01.2021.
//

#pragma once
#include "client_life.h"
#include "response.h"

namespace vbus
{

   class response_sender
   {
    public:
      virtual ~response_sender() = default;
      virtual void send_response(response_ptr response) = 0;
   };

   class service_server
   {
    public:
      virtual ~service_server() = default;

      /// this returns service_id so it can be identified
      virtual std::string init(response_sender &sender) = 0;

      virtual void fill_service_metadata(service_registration_message &data) {}
      virtual void on_request(request_ptr req, client_pin_ptr pin) = 0;
      virtual std::uint16_t get_bind_port() = 0;
   };
} // namespace vbus