//
// Created by jhrub on 25.01.2021.
//

#pragma once
#include "client_life.h"
#include "default_types.h"
#include "lock_free_socket_sender.h"
#include "msg_socket.h"
#include "request.h"
#include "response.h"
#include "service_server.h"
#include "service_server_impl.h"
#include <boost/unordered_map.hpp>

namespace vbus
{
   class service_server;
   class sync_clock;
   class service_server_impl : public response_sender, public msg_socket
   {
    public:
      service_server_impl(client_weak_ptr weak_ptr, zmq::context_t &ctx, std::unique_ptr<service_server> handler,
                          std::string bind_ip, uint16_t bind_port,  sync_clock &clock);

      void receive_requests();
      void timeout_requests();
      inline void send_response_messages()
      {
         queue_.transfer_messages();
      }
      void send_response(response_ptr response) final;

      inline int get_port() const
      {
         return port_;
      }
      inline const std::string &get_service_id() const
      {
         return service_id_;
      }

      void fill_service_metadata(service_registration_message &metadata);

      inline zmq::socket_t &get_main_thread_pass_through_socket()
      {
         return queue_.get_main_thread_pass_through_socket();
      }

      void shutdown();

    private:
      struct client_data
      {
         std::optional<std::chrono::steady_clock::time_point> timeout_at;
         std::string client_id;
         commons::cancellation_token_source *cts = nullptr;
      };

      struct response_wrapper
      {
         using response_ptr_type = typename response_ptr::element_type *;
         using response_deleter_type = typename response_ptr::deleter_type;

         response_ptr_type response;
         response_deleter_type deleter;
      };

      using request_map = boost::unordered_map<uuid, client_data, boost::hash<uuid>>;

      bool process_request_message();

      void remove_active_request(request_map::iterator it);
      void remove_request_data(uuid uuid);
      void consume_send_message(zmq::socket_t &send_socket, response_wrapper msg);

      client_weak_ptr weak_ptr_;

      std::unique_ptr<service_server> handler_;
      std::string service_id_;

      int port_ = 0;

      lock_free_socket_sender<boost::lockfree::queue<response_wrapper>> queue_;

      request_map active_requests_;
      std::vector<request_map::iterator> requests_to_delete_;

      allocator<request> request_allocator_;
      boost::object_pool<commons::cancellation_token_source> cts_pool_;

      multipart_message recv_message_;

      constexpr static int max_number_messages_per_recv = 20;
      std::mutex state_lock_;
      sync_clock &clock_;
   };
} // namespace vbus