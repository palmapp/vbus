//
// Created by jhrub on 22.01.2021.
//

#include "msg_socket.h"
#include <easylogging++.h>

vbus::msg_socket::msg_socket(zmq::context_t &ctx, zmq::socket_type type)
   : socket(ctx, type)
{
   socket.set(zmq::sockopt::heartbeat_ivl, 10000);
   socket.set(zmq::sockopt::heartbeat_timeout, 5000);
   socket.set(zmq::sockopt::rcvtimeo, 0);

   if (type == zmq::socket_type::router)
   {
      socket.set(zmq::sockopt::router_handover, 1);
   }
   socket.set(zmq::sockopt::linger, 0);
}

void vbus::msg_socket::set_connection_string(std::string connection_string)
{
   if (connected_)
   {
      if (connection_string != connection_string_ && !connection_string.empty())
      {
         socket.disconnect(connection_string_);
         socket.connect(connection_string);

         LOG(DEBUG) << "connection string has changed from \"" << connection_string_ << "\" to \"" << connection_string
            << "\"";

         connection_string_ = std::move(connection_string);
      }
   }
   else
   {
      connection_string_ = std::move(connection_string);
   }
}

bool vbus::msg_socket::try_connect()
{
   if (!connected_)
   {
      // this is debug level logging because it can be normal to
      if (connection_string_.empty())
      {
         //         CLOG(DEBUG, "msg_socket") << "connection_string_ is empty";
         return false;
      }
      else
      {
         socket.connect(connection_string_);
         connected_ = true;
      }
   }

   return true;
}

bool vbus::msg_socket::has_connection_string() const
{
   return !connection_string_.empty();
}
