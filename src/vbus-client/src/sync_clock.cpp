//
// Created by jhrub on 29.07.2022.
//
#include "sync_clock.h"

using namespace std::chrono;

std::int64_t vbus::sync_clock::to_remote(steady_clock::time_point local)
{
   auto state = get_state();
   auto diff = local - state.synced_at;
   return state.sync_time_ms + duration_cast<milliseconds>(diff).count();
}

std::chrono::steady_clock::time_point vbus::sync_clock::to_local(std::int64_t remote)
{
   auto state = get_state();
   auto diff = remote - state.sync_time_ms;
   return state.synced_at + milliseconds{diff};
}

std::int64_t vbus::sync_clock::remote_now()
{
   return to_remote(steady_clock::now());
}

void vbus::sync_clock::sync(std::int64_t remote_clock, steady_clock::time_point now)
{
   std::lock_guard lk{m_};
   state_ = sync_state{now, remote_clock};
}

vbus::sync_clock::sync_clock(std::int64_t remote_clock)
{
   sync(remote_clock);
}

vbus::sync_clock::sync_state vbus::sync_clock::get_state()
{
   std::lock_guard lk{m_};
   return state_;
}
