//
// Created by jhrub on 14.12.2020.
//
#include "service_client_impl.h"
#include "client.h"
#include "sync_clock.h"
#include <algorithm>
#include <boost/scope_exit.hpp>
#include <commons/guid.h>
#include <commons/make_string.h>
#include <commons/run_safe.h>
#include <commons/time_utils.h>
#include <easylogging++.h>
#include <fmt/format.h>
#include <serialization/msgpack.h>

#define CLOG_SERVICE_CLIENT(level) CLOG(level, "service_client") << service_id_ << ":"

vbus::service_client_impl::service_client_impl(zmq::context_t &context, std::string service_id, std::string client_id,
                                               sync_clock &clock)
   : msg_socket(context, zmq::socket_type::dealer), service_id_(service_id),
     queue_([this](zmq::socket_t &send_socket, request_wrapper msg) mutable
            {
               this->consume_send_message(send_socket, msg);
            },
            context, socket, fmt::format("{}@{}", client_id, service_id_)),
     event_monitor_(new client_event_monitor(context)), clock_(clock)
{
   socket.set(zmq::sockopt::routing_id, client_id);
   socket.set(zmq::sockopt::sndhwm, 1000);

   active_requests_.reserve(0xffff);
   requests_list_.reserve(0xffff);

   queue_.begin_send_handler = {[this]() mutable
   {
      state_lock_.lock();
      tmp_now_ = std::chrono::steady_clock::now();
   }};

   queue_.end_send_handler = {[this]() mutable
   {
      bool result = false;
      if (!requeue_list_.empty())
      {
         requeue_list();
         // we have changed the queue so we want to yield for some time to free the thread
         result = true;
      }

      state_lock_.unlock();

      return result;
   }};

   event_monitor_->init(socket, commons::make_string("inproc://socket_monitor_", reinterpret_cast<std::int64_t>(this)));
   event_monitor_->set_info(commons::make_string(client_id, "@", service_id_));
}

vbus::service_client_impl::~service_client_impl()
{
}

void vbus::service_client_impl::send_request(request_ptr req)
{
   auto now = std::chrono::steady_clock::now();
   if (req->timeout.timeout_ms > 0 && !req->timeout.timeout_at.has_value())
   {
      req->timeout.timeout_at = now + std::chrono::milliseconds{req->timeout.timeout_ms};
      req->timeout.local_sent_time_point = now;
   }

   if (cancelling_)
   {
      throw std::logic_error{"unable to push request to the queue service_client_impl::send_request"};
   }

   auto *ptr = req.release();
   if (!queue_.push({ptr, req.get_deleter()}))
   {
      delete ptr;
      throw std::logic_error{"unable to push request to the queue service_client_impl::send_request"};
   }
}

void vbus::service_client_impl::process_timeouts(message_handler &handler)
{
   if (try_lock())
   {
      // clang-format off
      BOOST_SCOPE_EXIT(&state_lock_)
         {
            state_lock_.unlock();
         }

      BOOST_SCOPE_EXIT_END

      // clang-format on

      if (cancelling_)
      {
         cancel_request_list(handler);
      }

      if (!connected_)
      {
         timeout_request_list(handler);
      }
      else
      {
         do_timeout(handler);
      }
   }
}

void vbus::service_client_impl::receive_responses(vbus::message_handler &handler)
{
   if (try_lock())
   {
      // clang-format off
      BOOST_SCOPE_EXIT(&state_lock_)
         {
            state_lock_.unlock();
         }

      BOOST_SCOPE_EXIT_END
      // clang-format on

      do_receive(handler);
   }
}

void vbus::service_client_impl::requeue_list()
{
   for (auto &ptr : this->requeue_list_)
   {
      this->send_request(std::move(ptr));
   }
   this->requeue_list_.clear();
}

void vbus::service_client_impl::do_receive(vbus::message_handler &handler)
{
   if (!this->active_requests_.empty())
   {
      int received = 0;

      while (received < vbus::service_client_impl::max_number_messages_per_recv)
      {
         bool has_received_message = vbus::recieve_multipart_message_from_socket(socket, this->recv_message_);
         if (has_received_message)
         {
            bool result = this->process_message(handler);

            if (!result) break;
            ++received;
         }
         else
         {
            break;
         }
      }
   }
}

bool vbus::service_client_impl::process_message(message_handler &handler)
{
   bool result = commons::run_safe(
      [this, &handler]()
      {
         if (this->recv_message_.empty())
         {
            CLOG_SERVICE_CLIENT(WARNING) << "received an empty message (zero frames)";
         }
         else if (this->recv_message_.size() < 3)
         {
            CLOG_SERVICE_CLIENT(WARNING)
               << "received not enough frames to process the message \n"
               "(min frames are 3 bus_message_type::response, request_metadata, int64 operation_status) : "
               << this->recv_message_.size();
         }
         else
         {
            auto type = vbus::bus_message_type::unknown;
            if (this->recv_message_.try_interpret_part_as(type, 0))
            {
               if (type == vbus::bus_message_type::response)
               {
                  auto id = this->recv_message_[1];
                  if (id.size() == sizeof(uuid))
                  {
                     uuid request_id;
                     std::copy(id.begin(), id.end(), request_id.begin());

                     int64_t operation_status = 0;
                     if (this->recv_message_.try_interpret_part_as(operation_status, 2))
                     {
                        this->recv_message_.pop_front(3);

                        handle_response(handler, request_id, operation_status);
                     }
                     else
                     {
                        CLOG_SERVICE_CLIENT(WARNING) << "unable to parse operation_status";
                     }
                  }
                  else
                  {
                     CLOG_SERVICE_CLIENT(WARNING) << "unable to parse the request_id frame";
                  }
               }
               else
               {
                  CLOG_SERVICE_CLIENT(WARNING) << "unexpected message type: " << static_cast<int>(type);
               }
            }
            else
            {
               CLOG_SERVICE_CLIENT(WARNING)
                  << "unable to extract message type from frame 0 : len= " << this->recv_message_[0].size();
            }
         }
      },
      "message_recv");
   return result;
}

void vbus::service_client_impl::handle_response(vbus::message_handler &handler, uuid const &request_id,
                                                std::int64_t status)
{

   if (auto it = active_requests_.find(request_id); it != active_requests_.end())
   {
      auto original_request_ptr = std::move(it->second);
      active_requests_.erase(it);

      handler.on_response_received(std::move(original_request_ptr), status, recv_message_);
   }
   else
   {
      CLOG_SERVICE_CLIENT(WARNING) << "unable to find matching request - request timed out before response receive : "
         << boost::lexical_cast<std::string>(request_id);
   }
}

void vbus::service_client_impl::do_send(zmq::socket_t &send_socket, vbus::request_ptr request)
{
   if (request == nullptr)
   {
      throw std::logic_error{"request is nullptr"};
   }

   auto &req = *request;
   const auto cmd = bus_message_type::request;
   const bool should_send_message = !req.message.empty();
   const auto metadata = spider::serialization::messagepack::stringify_msgpack(req.metadata);

   if (req.timeout.timeout_at.has_value())
   {
      req.timeout.sent_time_point = clock_.to_remote(request->timeout.local_sent_time_point.value());
   }
   else
   {
      req.timeout.sent_time_point = clock_.to_remote(std::chrono::steady_clock::now());
   }

   const auto timeout = spider::serialization::messagepack::stringify_msgpack(req.timeout);

   send_socket.send(zmq::const_buffer{&cmd, sizeof(cmd)}, zmq::send_flags::sndmore);
   send_socket.send(zmq::const_buffer{service_id_.data(), service_id_.size()}, zmq::send_flags::sndmore);
   send_socket.send(zmq::const_buffer{timeout.data(), timeout.size()}, zmq::send_flags::sndmore);

   if (should_send_message)
   {
      send_socket.send(zmq::const_buffer{metadata.data(), metadata.size()}, zmq::send_flags::sndmore);
      send_multipart_message_to_socket(send_socket, req.message);
   }
   else
   {
      send_socket.send(zmq::const_buffer
         {metadata.data(), metadata.size()});
   }
   /*
   #ifndef NDEBUG
      CLOG_SERVICE_CLIENT(DEBUG) << "do_send : " << req.metadata.action_id << " " << req.metadata.sequence_number;
   #endif*/

   active_requests_[commons::guid::convert_uuid(req.metadata.id)] = std::move(request);
}

void vbus::service_client_impl::do_timeout(vbus::message_handler &handler)
{
   const auto now = std::chrono::steady_clock::now();

   for (auto &[key, value] : active_requests_)
   {
      if (is_timed_out(now, value->timeout.timeout_at))
      {
         requests_list_.push_back(std::move(value));
      }
   }

   timeout_request_list(handler);
}

bool vbus::service_client_impl::is_timed_out(std::chrono::steady_clock::time_point now,
                                             std::optional<std::chrono::steady_clock::time_point> timeout_at) const
{
   return timeout_at.has_value() && timeout_at.value() < now;
}

void vbus::service_client_impl::timeout_request_list(vbus::message_handler &handler)
{
   for (auto &ptr : this->requests_list_)
   {
      this->active_requests_.erase(commons::guid::convert_uuid(ptr->metadata.id));
      commons::run_safe([&]
      {
         handler.on_request_timed_out(std::move(ptr));
      }, "on_request_timed_out");
   }

   this->requests_list_.clear();
}

void vbus::service_client_impl::cancel_request_list(vbus::message_handler &handler)
{

   for (auto &ptr : this->requests_list_)
   {
      this->active_requests_.erase(commons::guid::convert_uuid(ptr->metadata.id));
      commons::run_safe([&]
      {
         handler.on_request_cancelled(std::move(ptr));
      }, "on_request_cancelled");
   }
   this->requests_list_.clear();

   for (auto &pair : this->active_requests_)
   {
      commons::run_safe([&]
      {
         handler.on_request_cancelled(std::move(pair.second));
      }, "on_request_cancelled");
   }
   this->active_requests_.clear();
}

void vbus::service_client_impl::consume_send_message(zmq::socket_t &send_socket, request_wrapper cmd)
{
   commons::run_safe(
      [&, this]
      {
         /*
#ifndef NDEBUG
         CLOG_SERVICE_CLIENT(DEBUG) << "consume send message : " << cmd.req->metadata.action_id << " "
                                    << cmd.req->metadata.sequence_number;
#endif*/
         if (connected_)
         {
            do_send(send_socket, request_ptr{cmd.req, cmd.deleter});
         }
         else
         {
            if (is_timed_out(tmp_now_, cmd.req->timeout.timeout_at))
            {
               requests_list_.emplace_back(cmd.req, cmd.deleter);
            }
            else
            {
               requeue_list_.emplace_back(cmd.req, cmd.deleter);
            }
         }
      },
      "service_client_impl::consume_send_message");
}

void vbus::service_client_impl::shutdown()
{
   cancelling_ = true;
   queue_.shutdown();
}

zmq::socket_t &vbus::service_client_impl::get_monitor_socket()
{
   return event_monitor_->get_monitor_socket();
}

void vbus::service_client_impl::recv_monitor_events()
{
   event_monitor_->recv_events();
}

bool vbus::service_client_impl::try_lock()
{
   auto attempts = 999;
   auto locked = false;
   do
   {
      locked = state_lock_.try_lock();
      if (!locked & ((attempts % 50) == 0))
      {
         std::this_thread::yield();
      }
   } while (!locked && --attempts > 0);

   return locked;
}
