#include "client.h"
#include "publish_client_impl.h"
#include "service_client_impl.h"
#include "service_server_impl.h"

#include <boost/algorithm/string.hpp>
#include <commons/guid.h>
#include <commons/make_string.h>
#include <easylogging++.h>
#include <fmt/format.h>
#include <memory>
#include <serialization/msgpack.h>
#include <vbus/client_event_monitor.h>

using namespace std::string_literals;
using namespace std::string_view_literals;
using namespace spider::serialization;
using namespace std::chrono_literals;

#define CLOG_CLIENT(level) CLOG(level, "client") << client_id_ << " => "

vbus::client::client(connection_data data)
    : directory_socket_(context_, zmq::socket_type::sub), subscription_socket_(context_, zmq::socket_type::sub),
      data_(std::move(data)), event_monitor_(new client_event_monitor(context_))
{
   if (data_.connection_string.empty())
   {
      throw std::logic_error{fmt::format("unable to create vbus client: connection_string is empty")};
   }

   el::Loggers::getLogger("service_client");
   el::Loggers::getLogger("service_server");
   el::Loggers::getLogger("publish_client");
   el::Loggers::getLogger("client");
   el::Loggers::getLogger("msg_socket");

   subscribe(directory_socket_, "/dir");

   directory_socket_.set(zmq::sockopt::rcvtimeo, 0);
   subscription_socket_.set(zmq::sockopt::rcvtimeo, 0);

   dir_msg_buffer_.reserve(0xffff);
   event_monitor_->init(directory_socket_,
                        commons::make_string("inproc://monitor_", reinterpret_cast<std::int64_t>(this)));

   if (!data_.client_name.empty())
   {
      client_id_ = commons::make_string("cli:"s + commons::guid::generate_unsafe(), ":", data_.client_name);
   }
   else
   {
      client_id_ = commons::make_string("cli:"s + commons::guid::generate_unsafe(), ":", std::ios_base::hex,
                                        reinterpret_cast<std::int64_t>(this));
   }

   publish_client_ = std::make_unique<publish_client_impl>(context_, client_id_);

   directory_.id = -1;
   event_monitor_->set_info(client_id_);

   poller_.add(directory_socket_, zmq::event_flags::pollin, [this]() mutable { on_read_directory_socket(); });
   poller_.add(event_monitor_->get_monitor_socket(), zmq::event_flags::pollin,
               [this]() { event_monitor_->recv_events(); });
}

vbus::client::~client()
{
   if (data_.on_destroy)
   {
      try
      {
         data_.on_destroy();
         data_.on_destroy = nullptr;
      }
      catch (...)
      {
      }
   }

   try
   {
      if (!purge_finished_)
      {
         std::unique_lock lk{lock_};
         cv_.wait(lk, [&]() { return purge_finished_.load(); });
      }
   }
   catch (...)
   {
   }
   zmq_ctx_shutdown(context_.operator void *());
}

void vbus::client::on_read_directory_socket()
{
   bool directory_has_changed = false;

   if (recieve_multipart_message_from_socket(directory_socket_, temp_message_))
   {
      state_counter_ = std::max(state_counter_, static_cast<std::int64_t>(1));
      directory_has_changed = try_process_directory_changes(temp_message_);
   }

   if (directory_has_changed)
   {
      temp_message_handler_->on_directory_changed(directory_);
   }
}

void vbus::client::on_read_subscription_socket()
{
   if (recieve_multipart_message_from_socket(subscription_socket_, temp_message_))
   {
      temp_message_handler_->on_subscription_message(temp_message_);
   }
}

std::shared_ptr<vbus::client> vbus::client::make_service_client(connection_data data)
{
   return std::shared_ptr<client>(new client(std::move(data)));
}

std::shared_ptr<vbus::client> vbus::client::make_service_server(connection_data data,
                                                                std::vector<std::unique_ptr<service_server>> services)
{
   auto result = std::shared_ptr<client>(new client(std::move(data)));
   if (!services.empty())
   {
      result->register_services(std::move(services));
      result->client_id_ = commons::make_string(result->client_id_, ": in server role");
      result->event_monitor_->set_info(result->client_id_);
   }

   return result;
}

void vbus::client::subscribe(std::string_view channel)
{
   subscribe(subscription_socket_, channel);
   got_subscriptions_ = true;
}

void vbus::client::subscribe(zmq::socket_t &socket, std::string_view channel)
{
   socket.set(zmq::sockopt::subscribe, channel);
}

void vbus::client::run_for(vbus::message_handler &handler, std::chrono::steady_clock::duration duration,
                           commons::cancellation_token token)
{
   auto purge_on_cancel = [&]() mutable
   {
      if (token.is_cancelled())
      {
         shutdown();

         CLOG_CLIENT(TRACE) << "purging on going requests";

         for (auto *service : get_clients_to_process())
         {
            service->process_timeouts(handler);
         }

         if (publish_client_->should_process())
         {
            publish_client_->process({});
         }

         {
            std::unique_lock lk{lock_};
            purge_finished_ = true;
         }
         cv_.notify_all();
      }
   };

   auto end_time = std::chrono::steady_clock::now() + duration;
   auto step_duration = std::min<std::chrono::steady_clock::duration>(duration, 100ms);

   if (!connected_)
   {
      connect();
      int connection_counter = 0;
      while (!connected_ && !token.is_cancelled())
      {
         if ((++connection_counter % 50) != 0)
         {
            connect();
            if (connected_) break;
         }

         for (auto *service : get_clients_to_process())
         {
            service->process_timeouts(handler);
         }

         if (end_time >= std::chrono::steady_clock::now())
         {
            std::this_thread::sleep_for(step_duration);
         }
         else
         {
            purge_on_cancel();
            return;
         }
      }
   }

   temp_message_handler_ = &handler;

   while (!token.is_cancelled())
   {
      auto &clients = get_clients_to_process();
      if (last_client_list_size_ != clients.size())
      {
         install_poller_service_client_handlers(clients);
      }

      poller_.wait(std::chrono::duration_cast<std::chrono::milliseconds>(step_duration));

      for (auto &service : services_)
      {
         service->timeout_requests();
      }

      for (auto *service : get_clients_to_process())
      {
         service->process_timeouts(handler);
      }

      auto now = std::chrono::steady_clock::now();
      bool add_service_keep_alive_messages =
          !keep_alive_service_messages_.empty() && last_keep_alive_message_ + 10s < now;
      bool should_process_publish_client = publish_client_->should_process() || add_service_keep_alive_messages;

      if (should_process_publish_client)
      {
         if (add_service_keep_alive_messages)
         {
            std::vector<multipart_message> keepalive;
            keepalive.resize(keep_alive_service_messages_.size());

            std::transform(keep_alive_service_messages_.begin(), keep_alive_service_messages_.end(),
                           std::back_inserter(keepalive),
                           [](service_registration_message const &reg)
                           {
                              multipart_message msg;
                              msg.push_part(bus_message_type::service_registration);

                              auto registration_data = spider::serialization::messagepack::stringify_msgpack(reg);
                              msg.push_part(registration_data);

                              return msg;
                           });

            if (publish_client_->process(std::move(keepalive)))
            {
               last_keep_alive_message_ = now;
            }
         }
         else
         {
            publish_client_->process({});
         }
      }

      if (end_time < std::chrono::steady_clock::now())
      {
         break;
      }
   }

   purge_on_cancel();
}

void vbus::client::install_poller_service_client_handlers(std::vector<service_client_impl *> &clients)
{
   int failed = 0;
   for (auto *service_client : clients)
   {
      if (std::find(rcv_client_list_.begin(), rcv_client_list_.end(), service_client) != rcv_client_list_.end())
      {
         continue;
      }

      if (service_client->try_connect())
      {
         try
         {
            poller_.add(service_client->get_socket(), zmq::event_flags::pollin, [this, service_client]() mutable
                        { service_client->receive_responses(*temp_message_handler_); });

            poller_.add(service_client->get_main_thread_pass_through_socket(), zmq::event_flags::pollin,
                        [this, service_client]() mutable { service_client->send_request_messages(); });

            poller_.add(service_client->get_monitor_socket(), zmq::event_flags::pollin,
                        [service_client]() mutable { service_client->recv_monitor_events(); });

            rcv_client_list_.push_back(service_client);
         }
         catch (...)
         {
            ++failed;
         }
      }
      else
      {
         ++failed;
      }
   }

   last_client_list_size_ = clients.size() - failed;
}

bool vbus::client::try_process_directory_changes(const vbus::multipart_message &message)
{
   auto now = std::chrono::steady_clock::now();
   bool result = false;
   if (message.size() == 2)
   {
      if (message[0] == "/dir")
      {
         state_counter_ = std::max(state_counter_, static_cast<std::int64_t>(2));

         vbus::directory_state dir;
         if (messagepack::parse_msgpack(dir, message[1]))
         {
            state_counter_ = std::max(state_counter_, static_cast<std::int64_t>(3));

            if (dir.instance_name != data_.vbus_instance_name)
            {
               CLOG_CLIENT(ERROR) << "you are connected to an incorrect instance expected = "
                                  << data_.vbus_instance_name << " server sent = " << dir.instance_name;
               state_counter_ = -4ll;

               throw std::logic_error{"invalid instance"};
            }

            // I sync the clock with the time as soon as possible
            clock_.sync(dir.clock, now);

            if (dir.id != directory_.id)
            {
               CLOG_CLIENT(TRACE) << "directory has changed -> updating from " << directory_.id << " to " << dir.id;
               state_counter_ = std::max(state_counter_, static_cast<std::int64_t>(4));

               if (dir.hub_connection_string.empty())
               {
                  state_counter_ = static_cast<std::int64_t>(-5);

                  CLOG_CLIENT(ERROR) << "dir.hub_connection_string is empty / this might be an issue on vbus site";
                  return false;
               }

               std::lock_guard lk{lock_};

               if (got_subscriptions_)
               {
                  if (directory_.pub_sub_connection_string != dir.pub_sub_connection_string)
                  {
                     if (!pub_sub_connected_)
                     {
                        poller_.add(subscription_socket_, zmq::event_flags::pollin,
                                    [this]() mutable { on_read_subscription_socket(); });
                     }

                     if (pub_sub_connected_)
                     {
                        subscription_socket_.disconnect(directory_.pub_sub_connection_string);
                     }

                     subscription_socket_.connect(dir.pub_sub_connection_string);
                     pub_sub_connected_ = true;
                  }

                  if (dir.pub_sub_connection_string.empty())
                  {
                     CLOG_CLIENT(WARNING) << "old vbus detected : dir.pub_sub_connection_string is an empty string";
                  }
               }

               publish_client_->set_connection_string(dir.hub_connection_string);
               dir_msg_buffer_.clear();
               fmt::format_to(std::back_inserter(dir_msg_buffer_),
                              "{} : connection string \"{}\" id \"{}\"\npub-sub {}\n", "directory has changed",
                              dir.hub_connection_string, dir.id, dir.pub_sub_connection_string);

               for (auto &service : dir.services)
               {
                  std::string_view connection_string = [&]() -> std::string_view
                  {
                     if (service.connection_string.empty())
                     {
                        return dir.hub_connection_string;
                     }
                     else
                     {
                        return service.connection_string;
                     }
                  }();

                  client_connection_strings_[service.name] = static_cast<std::string>(connection_string);

                  if (auto *client = try_get_service_client_internal(service.name); client != nullptr)
                  {
                     client->set_connection_string(static_cast<std::string>(connection_string));
                     fmt::format_to(std::back_inserter(dir_msg_buffer_), "\t{} : {} : {}\n", service.name,
                                    service.connection_string, service.is_online ? "online (used)" : "offline (used)");
                  }
                  else
                  {
                     fmt::format_to(std::back_inserter(dir_msg_buffer_), "\t{} : {} : {}\n", service.name,
                                    service.connection_string, service.is_online ? "online" : "offline");
                  }
               }

               const auto dir_msg_str = std::string_view{dir_msg_buffer_.data(), dir_msg_buffer_.size()};
               CLOG_CLIENT(INFO) << dir_msg_str;

               state_counter_ = std::max(state_counter_, static_cast<std::int64_t>(5));

               directory_ = dir;
               result = true;
            }
            else
            {
               state_counter_ = std::max(state_counter_, static_cast<std::int64_t>(10));

               CLOG_CLIENT(TRACE) << "directory is the same: no update";
            }
         }
         else
         {
            state_counter_ = static_cast<std::int64_t>(-10);

            CLOG_CLIENT(WARNING) << "unable to extract directory information";
         }
      }
   }

   return result;
}

vbus::service_client_impl &vbus::client::get_service_client_internal(std::string const &service_id)
{
   if (auto client_ptr = try_get_service_client_internal(service_id); client_ptr != nullptr)
   {
      return *client_ptr;
   }
   else
   {
      auto client = std::make_unique<service_client_impl>(context_, service_id,
                                                          commons::make_string(client_id_, ":", service_id), clock_);

      if (auto connection_string_it = client_connection_strings_.find(service_id);
          connection_string_it != client_connection_strings_.end())
      {
         client->set_connection_string(connection_string_it->second);
      }

      auto *result = client.get();
      clients_[service_id] = std::move(client);
      client_list_.push_back(result);

      return *result;
   }
}

vbus::service_client_impl *vbus::client::try_get_service_client_internal(const std::string &service_id)
{
   if (auto it = clients_.find(service_id); it != clients_.end())
   {
      return it->second.get();
   }
   return nullptr;
}

void vbus::client::run_forever(vbus::message_handler &handler, commons::cancellation_token token)
{
   while (!token.is_cancelled())
   {
      run_for(handler, 10s, token);
   }

   LOG(TRACE) << "token.is_cancelled = " << (token.is_cancelled() ? "true" : "false");
}

vbus::service_client &vbus::client::get_service_client(std::string_view service_id)
{
   std::string service_id_copy = static_cast<std::string>(service_id);

   std::lock_guard lk{lock_};
   return get_service_client_internal(service_id_copy);
}

vbus::publish_client &vbus::client::get_publish_client()
{
   return *publish_client_;
}

std::vector<vbus::service_client_impl *> &vbus::client::get_clients_to_process()
{
   std::lock_guard lk{lock_};
   tmp_client_list_ = client_list_;

   return tmp_client_list_;
}

void vbus::client::register_services(std::vector<std::unique_ptr<service_server>> services)
{
   if (data_.private_ip.empty()) throw std::logic_error{"private_ip is required (127.0.0.1 or 0.0.0.0)"};
   if (data_.public_ip.empty()) throw std::logic_error{"public_ip is required (127.0.0.1 or your service public IP)"};

   services_.reserve(services.size());
   keep_alive_service_messages_.reserve(services.size());

   std::ranges::transform(services, std::back_inserter(services_),
                          [this](std::unique_ptr<service_server> &service)
                          {
                             return std::make_unique<service_server_impl>(weak_from_this(), context_,
                                                                          std::move(service), data_.private_ip,
                                                                          service->get_bind_port(), clock_);
                          });

   std::ranges::transform(services_, std::back_inserter(keep_alive_service_messages_),
                          [this](std::unique_ptr<service_server_impl> const &service)
                          {
                             auto id = service->get_service_id();

                             service_registration_message reg;
                             reg.service_id = id;
                             reg.connection_string =
                                 commons::make_string("tcp://", data_.public_ip, ":", service->get_port());
                             reg.vbus_instance_name = data_.vbus_instance_name;

                             service->fill_service_metadata(reg);

                             return reg;
                          });

   for (auto &service : services_)
   {

      service_server_impl *service_ptr = service.get();
      poller_.add(service->get_socket(), zmq::event_flags::pollin,
                  [service_ptr]() { service_ptr->receive_requests(); });

      poller_.add(service->get_main_thread_pass_through_socket(), zmq::event_flags::pollin,
                  [service_ptr]() { service_ptr->send_response_messages(); });
   }
}

std::vector<vbus::service_client *> vbus::client::get_service_clients_by_interface(std::string_view interface,
                                                                                   bool must_be_online)
{
   std::vector<vbus::service_client *> clients;
   std::vector<std::string> parts;
   parts.reserve(10);

   std::lock_guard lock{this->lock_};

   for (auto &service : directory_.services)
   {
      if (((must_be_online && service.is_online) || !must_be_online) && !service.metadata.interface.empty())
      {
         boost::algorithm::split(parts, service.metadata.interface, boost::is_any_of(",;"));

         if (std::find(parts.begin(), parts.end(), interface) != parts.end())
         {
            clients.push_back(&get_service_client_internal(service.name));
         }
      }
   }

   return clients;
}

vbus::directory_state vbus::client::get_current_directory() const
{
   std::lock_guard lock{this->lock_};

   return directory_;
}

void vbus::client::connect()
{
   if (connected_) throw std::logic_error{"connect can be called only once"};

   try
   {
      directory_socket_.connect(data_.connection_string);

      connected_ = true;
   }
   catch (...)
   {
      CLOG_CLIENT(WARNING) << fmt::format("vbus connection failed with connection string : {}",
                                          data_.connection_string);
   }
}

void vbus::client::shutdown()
{
   CLOG_CLIENT(TRACE) << "shutting down";
   purge_finished_ = false;

   for (auto *client : client_list_)
   {
      client->shutdown();
   }

   for (auto &service : services_)
   {
      service->shutdown();
   }

   publish_client_->shutdown();
}
