
// Created by jhrub on 14.12.2020.
//

#pragma once
#include "client_event_monitor.h"
#include "default_types.h"
#include "lock_free_socket_sender.h"
#include "message_handler.h"
#include "msg_socket.h"
#include "request.h"
#include "service_client.h"
#include <boost/functional/hash.hpp>
#include <boost/lockfree/queue.hpp>
#include <boost/pool/object_pool.hpp>
#include <boost/unordered_map.hpp>
#include <variant>

namespace vbus
{
   class sync_clock;

   class service_client_impl : public service_client, public msg_socket
   {
    public:
      service_client_impl(zmq::context_t &context, std::string service_id, std::string client_id, sync_clock &clock);
      ~service_client_impl();

      void send_request(request_ptr req) final;

      void receive_responses(message_handler &handler);
      void process_timeouts(message_handler &handler);
      inline void send_request_messages()
      {
         queue_.transfer_messages();
      }

      inline zmq::socket_t &get_main_thread_pass_through_socket()
      {
         return queue_.get_main_thread_pass_through_socket();
      }

      void shutdown();
      zmq::socket_t &get_monitor_socket();
      void recv_monitor_events();

    private:
      std::mutex state_lock_;
      std::atomic<bool> cancelling_ = false;

      bool process_message(message_handler &handler);
      void handle_response(message_handler &handler, uuid const &request_id, std::int64_t status);
      void timeout_request_list(vbus::message_handler &handler);
      void cancel_request_list(vbus::message_handler &handler);

      bool try_lock();

      void do_receive(vbus::message_handler &handler);
      void do_send(zmq::socket_t &socket, request_ptr req);

      std::string service_id_;

      struct request_wrapper
      {
         using request_ptr_type = typename request_ptr::element_type *;
         using request_deleter_type = typename request_ptr::deleter_type;

         request_ptr_type req;
         request_deleter_type deleter;
      };
      lock_free_socket_sender<boost::lockfree::queue<request_wrapper>> queue_;

      boost::unordered_map<uuid, request_ptr, boost::hash<uuid>> active_requests_;

      std::vector<request_ptr> requests_list_;
      std::vector<request_ptr> requeue_list_;

      multipart_message recv_message_;

      std::chrono::steady_clock::time_point tmp_now_ = {};
      std::unique_ptr<client_event_monitor> event_monitor_;
      sync_clock &clock_;

      constexpr static int max_number_messages_per_recv = 20;
      void do_timeout(message_handler &handler);
      bool is_timed_out(std::chrono::steady_clock::time_point now,
                        std::optional<std::chrono::steady_clock::time_point> timeout_at) const;
      void requeue_list();

      void consume_send_message(zmq::socket_t &send_socket, request_wrapper msg);
   };
} // namespace vbus