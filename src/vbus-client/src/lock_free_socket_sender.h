//
// Created by jhrub on 04.07.2021.
//

#pragma once

#ifndef NDEBUG
#include <easylogging++.h>
#endif

#include "commons/thread_name.h"
#include <atomic>
#include <boost/lockfree/queue.hpp>
#include <chrono>
#include <condition_variable>
#include <cstdint>
#include <fmt/format.h>
#include <mutex>
#include <optional>
#include <thread>
#include <type_traits>
#include <zmq.hpp>

namespace vbus
{
   template <class Tag>
   struct instance_counter
   {
      static int next_instance()
      {
         static std::atomic<int> s_instance_counter_ = 0;
         return ++s_instance_counter_;
      }
   };

   struct lock_free_socket_sender_tag
   {
   };

   template <class Queue>
   class lock_free_socket_sender
   {
   public:
      using value_type = typename Queue::value_type;
      using message_sender_function = std::function<void(zmq::socket_t &, value_type value)>;
      using send_begin_funct = std::function<void()>;
      using send_end_funct = std::function<bool()>;

      lock_free_socket_sender(message_sender_function func, zmq::context_t &ctx, zmq::socket_t &main_thread_send_socket,
                              std::string name = "lock_free_socket_sender")
         : func_(std::move(func)), q_(0xffff), main_thread_send_socket_(main_thread_send_socket),
           main_thread_pass_through_socket_(ctx, zmq::socket_type::pull),
           send_thread_send_socket_(ctx, zmq::socket_type::push), name_(std::move(name))

      {
         auto instance = instance_counter<lock_free_socket_sender_tag>::next_instance();
         auto connection_string = fmt::format("inproc://lock_free_socket_sender_{}", instance);

         main_thread_pass_through_socket_.bind(connection_string);
         send_thread_send_socket_.connect(connection_string);
         thread_ = std::thread(
            [this]() mutable
            {
               commons::set_this_thread_name(name_);
               run_thread();
            });
      }

      ~lock_free_socket_sender()
      {
         shutdown();
      }

      void shutdown()
      {
         {
            std::unique_lock lk{running_mutex_};
            running_ = false;
         }
         cv_.notify_one();

         if (thread_.joinable()) thread_.join();
      }

      bool push(value_type &&item)
      {
         bool result = q_.push(std::forward<value_type>(item));
         /* auto push_counter =*/
         ++push_counter_;
         cv_.notify_one();
         /*
         #ifndef NDEBUG
                  auto total_counter = ++queued_total_counter_;
                  LOG(DEBUG) << "push_counter= " << push_counter << " total_counter=" << total_counter;
         #endif
         */
         return result;
      }

      void transfer_messages();

      zmq::socket_t &get_main_thread_pass_through_socket()
      {
         return main_thread_pass_through_socket_;
      }

      send_begin_funct begin_send_handler;
      send_end_funct end_send_handler;

   private:
      message_sender_function func_;

      Queue q_;
      std::atomic<std::size_t> push_counter_ = {0};
      bool running_ = {true};

      std::mutex running_mutex_;
      std::condition_variable cv_;

      std::thread thread_;
      zmq::socket_t &main_thread_send_socket_;

      zmq::socket_t main_thread_pass_through_socket_;
      zmq::socket_t send_thread_send_socket_;
      std::string name_;
      void run_thread();

      std::atomic<int> pushed_counter_;
      std::atomic<int> transferred_counter_;
      std::atomic<int> transferred_begin_counter_;
      std::atomic<int> queued_total_counter_;
      std::atomic<int> throw_away_messages_;
   };

   template <class Queue>
   void lock_free_socket_sender<Queue>::run_thread()
   {
      using namespace std::chrono_literals;

      std::size_t counter = 0, current_push_counter = 0;
      while (running_)
      {
         while (counter != (current_push_counter = push_counter_.load()))
         {
            if (begin_send_handler) begin_send_handler();

            std::size_t dec_counter = 100;
            while (q_.consume_one(
                      [this](auto &&item)
                      {
                         func_(send_thread_send_socket_, std::forward<std::decay_t<decltype(item)>>(item));
                      }) &&
                   (--dec_counter) > 0)
            {
#ifndef NDEBUG
               ++pushed_counter_;
#endif
            }

            counter = counter + (100 - dec_counter);

            if (end_send_handler && end_send_handler())
            {
               std::unique_lock lk{running_mutex_};
               cv_.wait_for(lk, 5ms, [&counter, this]()
               {
                  return !running_;
               });
            }
         }

         std::unique_lock lk{running_mutex_};
         cv_.wait_for(lk, 100ms, [&counter, this]()
         {
            return !running_ || counter != push_counter_;
         });

         if (!running_)
         {
            if (begin_send_handler)
            {
               begin_send_handler();
            }

            while (
               q_.consume_one([this](auto &&item)
               {
                  func_(send_thread_send_socket_, std::forward<std::decay_t<decltype(item)>>(item));
               }))
            {
#ifndef NDEBUG
               ++pushed_counter_;
#endif
            }

            counter = current_push_counter;

            if (end_send_handler)
            {
               static_cast<void>(end_send_handler());
            }
         }
      }
   }

   template <class Queue>
   void lock_free_socket_sender<Queue>::transfer_messages()
   {
      std::size_t msg_counter = 20;
      zmq::message_t msg_part{};
      bool result = true;
      bool has_more = false;
      bool begin = true;

      while (result && msg_counter > 0)
      {
         result = !!main_thread_pass_through_socket_.recv(
            msg_part, has_more ? zmq::recv_flags::none : zmq::recv_flags::dontwait);
         if (result)
         {
#ifndef NDEBUG
            ++transferred_begin_counter_;
#endif
            has_more = msg_part.more();
            if (has_more)
            {
               if (main_thread_send_socket_.send(
                  msg_part, zmq::send_flags::sndmore | (begin ? zmq::send_flags::dontwait : zmq::send_flags::none)))
               {
                  begin = false;
               }
               else
               {
                  // consume and forget
                  while (msg_part.more())
                  {
                     if (!main_thread_pass_through_socket_.recv(msg_part, zmq::recv_flags::none))
                     {
                        continue;
                     }
                  }

                  ++throw_away_messages_;
                  break;
               }
            }
            else
            {
               main_thread_send_socket_.send(msg_part, zmq::send_flags::none);
               --msg_counter;
               begin = true;
            }

#ifndef NDEBUG
            ++transferred_counter_;
#endif
         }
      }
   }
} // namespace vbus