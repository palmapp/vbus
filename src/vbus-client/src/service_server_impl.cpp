//
// Created by jhrub on 25.01.2021.
//

#include "service_server_impl.h"
#include "sync_clock.h"
#include <commons/guid.h>
#include <commons/make_string.h>
#include <commons/run_safe.h>
#include <commons/time_utils.h>
#include <easylogging++.h>
#include <serialization/msgpack.h>
#include <serialization/serialization.h>
#include <vbus/connection_string.h>

#define CLOG_SERVICE_SERVER(level) CLOG(level, "service_server") << service_id_ << ":"

namespace
{
   std::string get_last_endpoint(zmq::socket_t &s)
   {
      return s.get(zmq::sockopt::last_endpoint);
   }
} // namespace

vbus::service_server_impl::service_server_impl(client_weak_ptr weak_ptr, zmq::context_t &ctx,
                                               std::unique_ptr<service_server> handler, std::string bind_ip,
                                               uint16_t bind_port,
                                               sync_clock &clock)
   : msg_socket(ctx, zmq::socket_type::router), weak_ptr_(weak_ptr), handler_(std::move(handler)),
     queue_([this](zmq::socket_t &send_socket, response_wrapper msg) mutable
            {
               this->consume_send_message(send_socket, msg);
            },
            ctx, socket),
     clock_(clock)
{
   if (bind_port == 0)
   {
      socket.bind(fmt::format("tcp://{}:*", bind_ip));
      auto bind_string = get_last_endpoint(socket);
      CLOG_SERVICE_SERVER(DEBUG) << "bind_string=" << bind_string;

      auto connection_string = connection_string::parse(bind_string);
      if (connection_string)
      {
         port_ = connection_string.value().port;
      }
      else
      {
         CLOG_SERVICE_SERVER(ERROR) << "unable to parse bind_string=" << bind_string;
         throw zmq::error_t();
      }
   }
   else
   {
      const auto bind_string = fmt::format("tcp://{}:{}", bind_ip, bind_port);
      socket.bind(bind_string);
      port_ =  bind_port;
      CLOG_SERVICE_SERVER(DEBUG) << "bind_string=" << bind_string;
   }

   service_id_ = handler_->init(*this);
   if (service_id_.empty()) throw std::logic_error{"service handler init must return not empty service id"};
   queue_.begin_send_handler = {[this]() mutable
   {
      state_lock_.lock();
   }};
   queue_.end_send_handler = {[this]() mutable
   {
      state_lock_.unlock();
      return false;
   }};
}

void vbus::service_server_impl::send_response(vbus::response_ptr response)
{
   if (!queue_.push({response.release(), response.get_deleter()}))
   {
      throw std::logic_error("unable to push service_server_impl::send_response to queue");
   }
}

void vbus::service_server_impl::receive_requests()
{
   std::lock_guard lk{state_lock_};
   int received = 0;

   while (received < service_server_impl::max_number_messages_per_recv)
   {
      bool has_received_message = vbus::recieve_multipart_message_from_socket(socket, this->recv_message_);
      if (has_received_message)
      {
         bool result = process_request_message();

         if (!result) break;

         ++received;
      }
      else
      {
         break;
      }
   }
}

bool vbus::service_server_impl::process_request_message()
{

   return commons::run_safe(
      [this]()
      {
         auto request = request_allocator_.construct();
         client_data data;

         if (recv_message_.size() >= 4)
         {
            data.client_id = static_cast<std::string>(recv_message_[0]);
            if (data.client_id.empty())
            {
               CLOG_SERVICE_SERVER(WARNING) << "message contains an empty client_id frame";
               return;
            }

            bus_message_type type = bus_message_type::unknown;
            if (recv_message_.try_interpret_part_as(type, 1) && type == bus_message_type::request)
            {
               request_timeout_frame timeout_frame;

               if (recv_message_[2] != service_id_)
               {
                  CLOG_SERVICE_SERVER(WARNING) << "received request is meant to be for a different service "
                     << recv_message_[2] << " expected " << service_id_ << ": ignoring";
                  return;
               }

               if (spider::serialization::messagepack::parse_msgpack(timeout_frame, recv_message_[3]))
               {
                  if (timeout_frame.timeout_ms > 0)
                  {
                     if (timeout_frame.sent_time_point == 0)
                     {
                        data.timeout_at =
                           std::chrono::steady_clock::now() + std::chrono::milliseconds{timeout_frame.timeout_ms};
                        CLOG_SERVICE_SERVER(WARNING)
                           << "request : from old vbus client does not contain sent_time_point";
                     }
                     else
                     {
                        data.timeout_at = clock_.to_local(timeout_frame.sent_time_point + timeout_frame.timeout_ms);
                     }

                     if (data.timeout_at.value() < std::chrono::steady_clock::now())
                     {
                        if (spider::serialization::messagepack::parse_msgpack(request->metadata, recv_message_[4]))
                        {
                           CLOG_SERVICE_SERVER(WARNING)
                              << "request : " << service_id_ << " [" << request->metadata.action_id << "] seq "
                              << request->metadata.sequence_number << " "
                              << commons::guid::convert_uuid(request->metadata.id)
                              << " is already timed out: skipping";
                        }

                        return;
                     }
                  }
               }
               else
               {
                  CLOG_SERVICE_SERVER(WARNING) << "unable to parse request_timeout_frame";
                  return;
               }

               if (spider::serialization::messagepack::parse_msgpack(request->metadata, recv_message_[4]))
               {

                  recv_message_.pop_front(5);
                  std::swap(request->message, recv_message_);

                  data.cts = cts_pool_.construct();
                  request->ct = data.cts->get_cancellation_token();

                  auto id = commons::guid::convert_uuid(request->metadata.id);
                  active_requests_[id] = std::move(data);
                  bool has_error = false;
                  try
                  {
                     handler_->on_request(std::move(request), weak_ptr_.lock());
                  }
                  catch (std::exception const &ex)
                  {
                     CLOG_SERVICE_SERVER(WARNING) << "unable to process request " << id << ": " << ex.what();
                     has_error = true;
                  }
                  catch (...)
                  {
                     CLOG_SERVICE_SERVER(WARNING) << "unable to process request " << id << ": unknown exception";
                     has_error = true;
                  }

                  if (has_error)
                  {
                     remove_request_data(id);
                  }
               }
               else
               {
                  CLOG_SERVICE_SERVER(WARNING) << "unable to parse request_metadata";
               }
            }
            else
            {
               CLOG_SERVICE_SERVER(WARNING) << "message has an invalid value " << static_cast<int>(type)
                  << " it should be bus_message_type::request = 2";
            }
         }
         else
         {
            CLOG_SERVICE_SERVER(WARNING) << "message contains an invalid number of frames: " << recv_message_.size();
         }
      },
      "process_request_message");
}

void vbus::service_server_impl::remove_request_data(vbus::uuid request_id)
{
   if (auto it = active_requests_.find(request_id); it != active_requests_.end())
   {
      remove_active_request(it);
   }
}

void vbus::service_server_impl::remove_active_request(request_map::iterator it)
{
   this->cts_pool_.destroy(it->second.cts);
   it->second.cts = nullptr;
   this->active_requests_.erase(it);
}

void vbus::service_server_impl::timeout_requests()
{
   std::lock_guard lk{state_lock_};

   auto now = std::chrono::steady_clock::now();

   auto end = active_requests_.end();
   for (auto it = active_requests_.begin(); it != end; ++it)
   {
      auto timeout_at = it->second.timeout_at;
      if (timeout_at.has_value() && timeout_at.value() < now)
      {
         it->second.cts->cancel();
         requests_to_delete_.push_back(it);
      }
   }

   for (auto it : requests_to_delete_)
   {
      remove_active_request(it);
   }

   requests_to_delete_.clear();
}

void vbus::service_server_impl::fill_service_metadata(vbus::service_registration_message &data)
{
   if (handler_)
   {
      handler_->fill_service_metadata(data);
   }
}

void vbus::service_server_impl::consume_send_message(zmq::socket_t &send_socket,
                                                     vbus::service_server_impl::response_wrapper msg)
{
   commons::run_safe(
      [&, this]
      {
         auto response = vbus::response_ptr{msg.response, msg.deleter};
         auto id = commons::guid::convert_uuid(response->request_id);

         if (auto it = this->active_requests_.find(id); it != this->active_requests_.end())
         {
            vbus::bus_message_type type = vbus::bus_message_type::response;
            send_socket.send(zmq::const_buffer{it->second.client_id.data(), it->second.client_id.size()},
                             zmq::send_flags::sndmore);
            send_socket.send(zmq::const_buffer{&type, sizeof(type)}, zmq::send_flags::sndmore);
            send_socket.send(zmq::const_buffer{response->request_id.data(), response->request_id.size()},
                             zmq::send_flags::sndmore);

            if (response->message.empty())
            {
               send_socket.send(zmq::const_buffer
                  {&response->status, sizeof(decltype(response->status))});
            }
            else
            {
               send_socket.send(zmq::const_buffer{&response->status, sizeof(decltype(response->status))},
                                zmq::send_flags::sndmore);
               send_multipart_message_to_socket(send_socket, response->message);
            }

            remove_active_request(it);
         }
         else
         {
            CLOG_SERVICE_SERVER(WARNING)
               << "unable to find request with id = " << id << " / it has probably timed out";
         }
      },
      "service_server_impl::consume_send_message");
}

void vbus::service_server_impl::shutdown()
{
   queue_.shutdown();
}
