//
// Created by jhrub on 22.01.2021.
//

#pragma once

#include "allocator.h"
#include "msg_socket.h"
#include "publish_client.h"
#include <boost/lockfree/queue.hpp>
#include <boost/pool/object_pool.hpp>
#include <optional>
#include <vbus/data.h>

namespace vbus
{
   class publish_client_impl : public publish_client, public msg_socket
   {
    public:
      publish_client_impl(zmq::context_t &ctx, std::string client_id);
      ~publish_client_impl();

      bool publish(multipart_message message, options op = options::normal) final;
      void shutdown();

      bool should_process();
      bool process(std::optional<std::vector<multipart_message>> services);

    private:
      struct msg_wrapper
      {
         multipart_message message;
         options opts;
      };

      vbus::bus_message_type get_vbus_message_type(const vbus::publish_client_impl::msg_wrapper *msg) const;

      allocator<msg_wrapper> pool_;
      boost::lockfree::queue<msg_wrapper *> queue_;

      multipart_message recv_message_;

      std::atomic<bool> shutting_down = false;
   };
} // namespace vbus
