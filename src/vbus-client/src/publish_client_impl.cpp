//
// Created by jhrub on 22.01.2021.
//

#include "publish_client_impl.h"
#include <easylogging++.h>

#include "commons/stop_watch.h"

#include <serialization/msgpack.h>

#define CLOG_PUBLISH_CLIENT(level) CLOG(level, "publish_client")

using namespace std::chrono_literals;

vbus::publish_client_impl::publish_client_impl(zmq::context_t &ctx, std::string client_id)
   : msg_socket(ctx, zmq::socket_type::dealer), pool_(0xffff), queue_(0xffff)
{
   socket.set(zmq::sockopt::routing_id, client_id);
   socket.set(zmq::sockopt::sndhwm, 100000);
}

vbus::publish_client_impl::~publish_client_impl()
{
   if (!queue_.empty())
      CLOG_PUBLISH_CLIENT(WARNING) << "there are not sent messages in the queue_";
   else
      CLOG_PUBLISH_CLIENT(DEBUG) << "destroyed publish_client_impl";
}

bool vbus::publish_client_impl::should_process()
{
   return has_connection_string() && !queue_.empty();
}

bool vbus::publish_client_impl::publish(vbus::multipart_message message, vbus::publish_client::options op)
{
   if (shutting_down)
   {
      return false;
   }

   auto *ptr = pool_.construct_raw_ptr();
   ptr->message = std::move(message);
   ptr->opts = op;

   queue_.push(ptr);
   return true;
}

vbus::bus_message_type
vbus::publish_client_impl::get_vbus_message_type(const vbus::publish_client_impl::msg_wrapper *msg) const
{
   vbus::bus_message_type type;
   switch (msg->opts)
   {
   case vbus::publish_client::options::normal:
      type = vbus::bus_message_type::publish;
      break;
   case vbus::publish_client::options::sticky:
      type = vbus::bus_message_type::publish_sticky;
      break;
   default:
      type = vbus::bus_message_type::publish;
   }
   return type;
}

bool vbus::publish_client_impl::process(std::optional<std::vector<multipart_message>> services)
{
   if (try_connect())
   {
      if (services.has_value())
      {
         auto perf = commons::stop_watch{};
         for (auto &registration : services.value())
         {
            // zmq requires the send method working on non const references
            send_multipart_message_to_socket(socket, registration);
         }

         if (perf.get_elapsed() > 10s)
         {
            LOG(WARNING) << "service registration is too slow:" << perf.to_string();
         }
      }

      auto process_queue = [this]()
      {
         msg_wrapper *msg = nullptr;
         if (queue_.pop(msg) && msg != nullptr)
         {
            vbus::bus_message_type type = get_vbus_message_type(msg);

            if (socket.send(zmq::const_buffer{&type, sizeof(bus_message_type)},
                            zmq::send_flags::sndmore | zmq::send_flags::dontwait))
            {
               send_multipart_message_to_socket(socket, msg->message);
               pool_.destroy(msg);

               return true;
            }
            else
            {
               queue_.push(msg);
               return false;
            }
         }
         else
         {
            return false;
         }
      };

      if (shutting_down)
      {
         auto shutdown_process_queue = [this]()
         {
            msg_wrapper *msg = nullptr;
            if (queue_.pop(msg) && msg != nullptr)
            {
               vbus::bus_message_type type = get_vbus_message_type(msg);

               if (socket.send(zmq::const_buffer{&type, sizeof(bus_message_type)},
                               zmq::send_flags::sndmore | zmq::send_flags::dontwait))
               {
                  send_multipart_message_to_socket(socket, msg->message);
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               return false;
            }
         };

         while (shutdown_process_queue())
         {
         }
      }
      else
      {
         for (int i = 0; i != 1000; ++i)
         {
            if (!process_queue())
            {
               break;
            }
         }
      }
      bool receiving = true;
      while (receiving)
      {
         receiving = recieve_multipart_message_from_socket(socket, recv_message_);
         if (receiving)
         {
            vbus::bus_message_type type = bus_message_type::unknown;
            if (recv_message_.size() >= 2 && recv_message_.try_interpret_part_as(type, 0))
            {
               if (type == bus_message_type::error)
               {
                  CLOG_PUBLISH_CLIENT(ERROR) << "server sent error: " << recv_message_[1];
               }
               else if (type == bus_message_type::service_registration)
               {
                  service_registration_message msg;
                  spider::serialization::messagepack::parse_msgpack(msg, recv_message_[1]);
                  // CLOG_PUBLISH_CLIENT(TRACE) << "registration done : " << msg.service_id;
               }
            }
            else
            {
               CLOG_PUBLISH_CLIENT(TRACE) << "invalid message type : " << static_cast<int>(type);
            }
         }
      }

      return true;
   }
   return false;
}

void vbus::publish_client_impl::shutdown()
{
   shutting_down = true;
}
