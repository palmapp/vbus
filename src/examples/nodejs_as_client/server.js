const zmq = require('zeromq/v5-compat');
const msgpack = require('msgpack');
const process = require('process');
const _ = require("underscore");

let directory = [];
let service = null;
const id = "client:"+process.pid;

const directorySocket = new zmq.socket("sub");
directorySocket.subscribe("/dir")
directorySocket.connect("tcp://127.0.0.1:50000");
directorySocket.on("message", function(topic, message){
   directory = msgpack.unpack(message);
   console.log("directory status: " + JSON.stringify(directory));
   var echoServiceConfig = _.find(directory[1], service => service[0] == "echoservice");
   if (echoServiceConfig)
   {
      var connectionString = directory[0];
      if (!_.isEmpty(echoServiceConfig[1])) {
         connectionString = echoServiceConfig[1];
      }
      if (service == null) {
         service = new Service(connectionString);
      }
      else if (service.connectionString != connectionString) {
         service.close()
         service = new Service(connectionString);
      }
   }
   else if (service)
   {
      service.close();
   }
   else
   {
      console.log("unable to find service: echoservice");
   }
});

const COMMAND_RESPONSE = 3;
const COMMAND_REQUEST = Buffer.alloc(1, 2);

var i = 0;

class Service
{
   constructor(connectionString){
      this.connectionString = connectionString;
      this.socket = new zmq.socket("dealer");
      this.socket.identity = id;

      this.socket.connect(connectionString);
      
      this.interval= setInterval(()=>this.sendRequest(), 100);
      this.socket.on("message", function(cmd, data, payload){
         if (cmd[0] == COMMAND_RESPONSE) {
            console.log("response: " + payload.toString("utf-8"));
         }       
      });

      console.log("opening connection : " + this.connectionString);
   }

   close(){
      clearInterval(this.interval);
      this.socket.close();      
      console.log("closing connection : "+this.connectionString)
   }

   sendRequest()
   {
      this.socket.send([COMMAND_REQUEST,"echoservice", "i= "+(i++)]);
   }
}
