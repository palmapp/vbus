# /bin/python
import time
import zmq
import sys
import msgpack
import threading

context = zmq.Context()
socket = context.socket(zmq.SUB)

#print("topic delimiter:" + zmq.log.handlers.TOPIC_DELIM)

socket.subscribe('/dir')
socket.connect("tcp://127.0.0.1:50000")


def directory_thread_fnc():
    while True:
        message = socket.recv_multipart()
        directory = msgpack.unpackb(message[1])
        print("directory: %s" % directory)

        #  Do some 'work'
        time.sleep(1)


dt = threading.Thread(target=directory_thread_fnc)


dt.join()
