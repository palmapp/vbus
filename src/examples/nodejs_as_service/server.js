const zmq = require('zeromq/v5-compat');
const msgpack = require('msgpack');
const process = require('process');

let directory = [];
let service = null;
const id = "echoservice";

const directorySocket = new zmq.socket("sub");
directorySocket.subscribe("/dir")
directorySocket.connect("tcp://127.0.0.1:50000");
directorySocket.on("message", function(topic, message){
   directory = msgpack.unpack(message);


   console.log("directory status: " + JSON.stringify(directory));

   if (service == null)
   {
      service = new Service(directory[0]);
   }
   else if (service.connectionString != directory[0])
   {
      service.close()
      service = new Service(directory[0]);
   }
});

const SERVICE_REGISTRATION = Buffer.alloc(1,1);
const COMMAND_ERROR = 0;
const COMMAND_REQUEST = 2;
const COMMAND_RESPONSE = Buffer.alloc(1, 3);

class Service
{
   constructor(connectionString){
      this.connectionString = connectionString;
      this.socket = new zmq.socket("dealer");
      this.socket.identity = id;

      this.socket.connect(connectionString);
      
      this.socket.on("message", (cmd, data, payload)=>{
         const command = cmd[0];
         if (COMMAND_ERROR == command) 
         {
            console.error("registration failed: " + msgpack.unpack(data));
         }
         else if (COMMAND_REQUEST == command)
         {
            console.log("request from "+ data + " "+ payload);
            this.socket.send([COMMAND_RESPONSE, data, "echo: " +payload.toString('utf8')]);
         }
         else if (command !== 1)
         {
            console.log("uknown data "+data.toString("utf-8"));
         }
      });

      this.interval = setInterval(() => this.sendPing(), 5000);
      this.sendPing();
      console.log("opening connection : " + this.connectionString);
   }

   close(){
      clearInterval(this.interval);
      this.socket.close();      
      console.log("closing connection : "+this.connectionString)
   }

   sendPing()
   {
      this.socket.send([SERVICE_REGISTRATION, this.formatPing()]);
   }

   formatPing()
   {
      return msgpack.pack(["secret", ""]);
   }
}
