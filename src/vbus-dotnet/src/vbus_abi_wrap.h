//
// Created by jhrub on 15.07.2021.
//

#pragma once

#include <vbus/vbus.hpp>

using namespace vbus::abi;

class client_wrap : public client
{
   inline explicit client_wrap(vbus_client_ptr client_ptr) : client()
   {
      client_ptr_ = client_ptr;
   }

   inline ~client_wrap()
   {
      client_ptr_ = nullptr;
   }
};
