#include "vbus_abi_wrap.h"
#include <chrono>
#include <gsl/narrow>
#include <memory>
#include <vcclr.h>
#include <vector>

using namespace System;
using namespace System::Diagnostics;

namespace Vbus
{
   inline std::int64_t now_in_millis()
   {
      using namespace std::chrono;
      return static_cast<std::int64_t>(duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count());
   }

 public
   ref class VbusServiceCallTimeoutException : public Exception
   {
    public:
      VbusServiceCallTimeoutException() : Exception() {}
   };

   ref class VbusClient;
   class access_handle
   {
    public:
      access_handle(VbusClient ^ client);
      ~access_handle();

    private:
      gcroot<VbusClient ^> client_;
   };

 private
   ref class Utils sealed
   {
    public:
      static array<Byte> ^
          ToUtf8(String ^ str) { return Text::Encoding::UTF8->GetBytes(str); }

          static String
          ^
          ToString(std::string const &value) {
             if (value.empty())
             {
                return String::Empty;
             }
             else
             {
                array<Byte> ^ buffer = gcnew array<Byte>(gsl::narrow<int>(value.size()));
                pin_ptr<Byte> buffer_ptr = &buffer[0];

                std::copy(value.begin(), value.end(), reinterpret_cast<char *>(buffer_ptr));
                return Text::Encoding::UTF8->GetString(buffer);
             }
          }

          static String
          ^ ToString(char const *ptr) {
               std::string value = {ptr};
               return ToString(value);
            }
   };
   std::string to_string(String ^ str)
   {
      if (str == nullptr || str->Length == 0)
      {
         return {};
      }

      array<Byte> ^ data = Utils::ToUtf8(str);
      pin_ptr<Byte> data_ptr = &data[0];

      return {reinterpret_cast<const char *>(data_ptr), gsl::narrow<size_t>(data->Length)};
   }

 public
   ref class MessagePart
   {
    public:
      MessagePart() : Data(gcnew array<Byte>(0)) {}

      MessagePart(array<Byte> ^ data) : Data(data) {}

      MessagePart(String ^ data) : Data(Utils::ToUtf8(data)) {}

      MessagePart(vbus_string_t str)
      {
         if (str.length > 0)
         {
            Data = gcnew array<Byte>(gsl::narrow<int>(str.length));
            pin_ptr<Byte> dataPtr = &Data[0];

            std::copy(str.data, str.data + str.length, reinterpret_cast<char *>(dataPtr));
         }
         else
         {
            Data = gcnew array<Byte>(0);
         }
      }

      String ^ AsString() { return Text::Encoding::UTF8->GetString(Data); } array<Byte> ^
          AsBytes() { return Data; }

          property int ByteLength
      {
         int get()
         {
            return Data->Length;
         }
      }

    private:
      array<Byte> ^ Data;
   };

 public
   ref class Message
   {
    public:
      Message() : Parts(gcnew array<MessagePart ^>(0)) {}

      Message(array<MessagePart ^> ^ parts) : Parts(parts) {}

      Message(array<array<Byte> ^> ^ parts) : Parts(gcnew array<MessagePart ^>(parts->Length))
      {

         for (int i = 0, len = parts->Length; i != len; ++i)
         {
            Parts[i] = gcnew MessagePart(parts[i]);
         }
      }

      Message(array<String ^> ^ parts) : Parts(gcnew array<MessagePart ^>(parts->Length))
      {

         for (int i = 0, len = parts->Length; i != len; ++i)
         {
            String ^ str = parts[i];
            Parts[i] = gcnew MessagePart(str);
         }
      }

      Message(vbus_string_array_t message) : Parts(gcnew array<MessagePart ^>(gsl::narrow<int>(message.length)))
      {
         for (int i = 0, len = gsl::narrow<int>(message.length); i != len; ++i)
         {
            Parts[i] = gcnew MessagePart(message.elements[i]);
         }
      }

      String ^ AsString(int i) { return Parts[i]->AsString(); } array<Byte> ^
          AsBytes(int i) { return Parts[i]->AsBytes(); }

          property int Length
      {
         int get()
         {
            return Parts->Length;
         }
      }

    private:
      array<MessagePart ^> ^ Parts;
   };

 public
   ref class Request
   {
    public:
      String ^ ServiceId;
      UInt16 ActionId;
      int TimeoutMiliseconds;
      Message ^ Data;
   };

 public
   ref class Response
   {
    public:
      Int64 Status;
      Message ^ Data;
   };

   ref class ResponseCallBack
   {
    public:
      ResponseCallBack(Response ^ response, Threading::Tasks::TaskCompletionSource<Response ^> ^ source)
          : response_(response), source_(source)
      {
      }

      void OnDone(Object ^ state)
      {
         if (response_->Status == -503)
         {
            source_->SetException(gcnew VbusServiceCallTimeoutException());
         }
         else if (response_->Status == -504)
         {
            source_->SetCanceled();
         }
         else
         {
            source_->SetResult(response_);
         }
      }

    private:
      Response ^ response_;
      Threading::Tasks::TaskCompletionSource<Response ^> ^ source_;
   };

   class message_wrapper
   {
    public:
      message_wrapper(Message ^ message)
      {
         if (message != nullptr && message->Length > 0)
         {
            part_views.reserve(gsl::narrow_cast<size_t>(message->Length));
            for (int i = 0, len = message->Length; i != len; ++i)
            {
               array<Byte> ^ part = message->AsBytes(i);
               if (part != nullptr && part->Length > 0)
               {
                  // message part must be copied
                  pin_ptr<Byte> part_ptr = &part[0];
                  if (part_ptr != nullptr)
                  {
                     parts.emplace_back(reinterpret_cast<const char *>(part_ptr),
                                        gsl::narrow_cast<size_t>(part->Length));
                  }
                  else
                  {
                     parts.push_back({});
                  }
               }
               else
               {
                  parts.push_back({});
               }

               auto &part_str = parts.back();
               part_views.push_back({part_str.size(), part_str.c_str()});
            }
         }
      }

      vbus_string_array_t get_array_view()
      {
         auto len = part_views.size();
         return {len, len == 0 ? nullptr : part_views.data()};
      }

    private:
      std::deque<std::string> parts;
      std::vector<vbus_string_t> part_views;
   };

 public
   ref class ResponseSender
   {
    public:
      ResponseSender(vbus_message_sender_ptr sender) : sender_context_((IntPtr)sender) {}

      void SendResponse(Response ^ response)
      {

         vbus_message_sender_ptr context =
             (vbus_message_sender_ptr)Threading::Interlocked::Exchange(sender_context_, IntPtr::Zero);
         if (context != nullptr)
         {
            GC::SuppressFinalize(this);

            message_wrapper msg{response->Data};

            /* if (IsClientDisposed()) return;
             access_handle handle(client_);*/
            vbus_send_response(context, response->Status, msg.get_array_view());
         }
      }

      ~ResponseSender()
      {
         SuppressFinalizeFree();
      }

      !ResponseSender()
      {
         Free();
      }

      void SuppressFinalizeFree()
      {
         GC::SuppressFinalize(this);
         Free();
      }
      // bool IsClientDisposed();

    private:
      void Free()
      {
         vbus_message_sender_ptr context =
             (vbus_message_sender_ptr)Threading::Interlocked::Exchange(sender_context_, IntPtr::Zero);

         if (context != nullptr)
         {
            // if (IsClientDisposed()) return;

            // access_handle handle(client_);
            vbus_send_no_response(context);
         }
      }

      IntPtr sender_context_;
   };

   using ServiceHandler = Func<Request ^, Threading::CancellationToken, Threading::Tasks::Task<Response ^> ^>;

 public
   ref class VbusServiceData
   {
    public:
      String ^ Id;
      String ^ Interface;
      String ^ Metadata;
      bool ClusterVisible;
   };

 public
   ref class VbusServiceConfig : public VbusServiceData
   {
    public:
      ServiceHandler ^ Handler;
      bool RunOnThreadPool;
   };

 public
   ref class VbusConfig
   {
    public:
      String ^ Properties;
      Collections::Generic::List<String ^> ^ ChannelSubscritions;
      Action<Message ^> ^ SubscriptionCallback;

      Collections::Generic::List<VbusServiceConfig ^> ^ ExportedServices;
   };

   vbus_string_t make_str_view(Byte *ptr, int len)
   {
      return vbus_string_t{gsl::narrow<size_t>(len), reinterpret_cast<char *>(ptr)};
   }

   class subscription_callback
   {
    public:
      subscription_callback(Action<Message ^> ^ callback)
      {
         Callback = callback;
      }

      static void callback(void *ptr, vbus_string_array_t message)
      {
         try
         {
            auto *self = static_cast<subscription_callback *>(ptr);
            self->Callback->Invoke(gcnew Message(message));
         }
         catch (Exception ^)
         {
         }
      }

    private:
      gcroot<Action<Message ^> ^> Callback;
   };

   ref class ThreadPoolRequestContext
   {
    public:
      ThreadPoolRequestContext(Threading::CancellationTokenSource ^ cts, Request ^ request, ResponseSender ^ sender,
                               ServiceHandler ^ handler)
          : cts_(cts), request_(request), sender_(sender), handler_(handler)
      {
      }

      Threading::CancellationToken token_;
      Request ^ request_;
      ResponseSender ^ sender_;
      ServiceHandler ^ handler_;
      Threading::CancellationTokenSource ^ cts_;

      ~ThreadPoolRequestContext()
      {
         GC::SuppressFinalize(this);
         delete cts_;
      }

      !ThreadPoolRequestContext()
      {
         delete cts_;
      }

      static void Run(Object ^ state)
      {
         // Diagnostics::Trace::AutoFlush = true;

         ThreadPoolRequestContext ^ ctx = (ThreadPoolRequestContext ^) state;
         ctx->InvokeHandler();
      }

      void OnCompleted(Threading::Tasks::Task<Response ^> ^ response)
      {
         if (response->IsCanceled || cts_->IsCancellationRequested) return;

         try
         {
            sender_->SendResponse(response->Result);
         }
         catch (AggregateException ^ ex)
         {
            Collections::ObjectModel::ReadOnlyCollection<Exception ^> ^ exceptions = ex->Flatten()->InnerExceptions;
            if (exceptions->Count > 0)
            {
               sender_->SendResponse(ThreadPoolRequestContext::FormatExceptionResponse(exceptions[0]));
            }
            else
            {
               Response ^ response = gcnew Response();
               response->Status = -501;
               sender_->SendResponse(response);
            }
         }
         catch (Exception ^ ex)
         {
            sender_->SendResponse(ThreadPoolRequestContext::FormatExceptionResponse(ex));
         }
      };

      void InvokeHandler()
      {
         try
         {
            if (request_->TimeoutMiliseconds > 0)
            {
               cts_->CancelAfter((int)request_->TimeoutMiliseconds);
            }

            Threading::Tasks::Task<Response ^> ^ response_task = handler_->Invoke(request_, cts_->Token);
            if (response_task->IsCompleted)
            {
               OnCompleted(response_task);
            }
            else
            {
               Action<Threading::Tasks::Task<Response ^> ^> ^ action =
                   gcnew Action<Threading::Tasks::Task<Response ^> ^>(this, &ThreadPoolRequestContext::OnCompleted);

               response_task->ContinueWith(action);
            }
         }
         catch (AggregateException ^ ex)
         {
            Collections::ObjectModel::ReadOnlyCollection<Exception ^> ^ exceptions = ex->Flatten()->InnerExceptions;
            if (exceptions->Count > 0)
            {
               sender_->SendResponse(ThreadPoolRequestContext::FormatExceptionResponse(exceptions[0]));
            }
            else
            {
               Response ^ response = gcnew Response();
               response->Status = -501;
               sender_->SendResponse(response);
            }
         }
         catch (Exception ^ ex)
         {
            sender_->SendResponse(ThreadPoolRequestContext::FormatExceptionResponse(ex));
         }
      }

      static Response ^ FormatExceptionResponse(Exception ^ ex) {
         String ^ name = ex->GetType()->FullName;
         String ^ message = ex->Message;
         String ^ stack = ex->StackTrace;
         array<String ^> ^ result = gcnew array<String ^>(3);
         result[0] = name;
         result[1] = message;
         result[2] = stack;

         Response ^ response = gcnew Response();
         response->Status = -500;
         response->Data = gcnew Message(result);

         return response;
      }
   };

 public
   ref class ServiceCallback : public MarshalByRefObject
   {
    public:
      ServiceCallback(Threading::CancellationToken token, ServiceHandler ^ callback, bool use_thread_pool)
          : token_(token), callback_(callback), use_thread_pool_(use_thread_pool)
      {
      }

      void OnRequest(vbus_message_sender_ptr sender_context, vbus_request_t *request, vbus_string_array message)
      {

         ResponseSender ^ sender = gcnew ResponseSender(sender_context);
         try
         {
            Request ^ dotnet_request = gcnew Request();

            dotnet_request->ActionId = request->action_id;
            if (request->timeout_ms > 0)
            {
               dotnet_request->TimeoutMiliseconds = gsl::narrow<int>(request->timeout_ms);
               if (dotnet_request->TimeoutMiliseconds <= 0)
               {
                  // request is timedout already
                  sender->SuppressFinalizeFree();
                  return;
               }
            }

            Message ^ dotnet_Message = gcnew Message(message);
            dotnet_request->Data = dotnet_Message;

            ThreadPoolRequestContext ^ call_context = gcnew ThreadPoolRequestContext(
                Threading::CancellationTokenSource::CreateLinkedTokenSource(token_), dotnet_request, sender, callback_);

            if (use_thread_pool_)
            {
               Threading::ThreadPool::UnsafeQueueUserWorkItem(
                   gcnew Threading::WaitCallback(&ThreadPoolRequestContext::Run), call_context);
            }
            else
            {
               call_context->InvokeHandler();
            }
         }
         catch (Exception ^ ex)
         {
            sender->SendResponse(ThreadPoolRequestContext::FormatExceptionResponse(ex));
         }
         catch (...)
         {
            Response ^ resp = gcnew Response();
            resp->Status = -505;
            sender->SendResponse(resp);
         }
      }

    private:
      Threading::CancellationToken token_;
      ServiceHandler ^ callback_;
      bool use_thread_pool_;
   };

   class service_callback
   {
    public:
      service_callback(ServiceCallback ^ handler) : Callback(handler) {}

      static void on_request(void *context, vbus_message_sender_ptr sender_context, vbus_request_t *request,
                             vbus_string_array message)
      {
         service_callback *self = reinterpret_cast<service_callback *>(context);
         self->Callback->OnRequest(sender_context, request, message);
      }

      gcroot<ServiceCallback ^> Callback;
   };

   class async_request_data
   {
    public:
      async_request_data(VbusClient ^ client, Threading::Tasks::TaskCompletionSource<Response ^> ^ source)
          : client_(client), task_completion_source_(source), context_(Threading::SynchronizationContext::Current)
      {
      }

      static void request_callback(vbus_request_t *request, vbus_response_t *response);

    private:
      gcroot<VbusClient ^> client_;
      gcroot<Threading::Tasks::TaskCompletionSource<Response ^> ^> task_completion_source_;
      gcroot<Threading::SynchronizationContext ^> context_;
   };

 public
   ref class VbusClient sealed
   {
    public:
      static void InitEasyLogging(String ^ logCfg)
      {
         auto cfg = to_string(logCfg);
         vbus_init_logger(vbus_string_t{cfg.size(), cfg.data()});
      }

      VbusClient(VbusConfig ^ config)
      {
         thread_ = gcnew Threading::Thread(gcnew Threading::ThreadStart(this, &VbusClient::DoRun));
         thread_->Name = "VbusThread";

         cts_ = gcnew Threading::CancellationTokenSource();

         access_counter_ = 0;
         client_ = IntPtr::Zero;
         subscription_callback_ = nullptr;
         services_ = nullptr;

         if (config == nullptr)
         {
            throw gcnew ArgumentNullException("config", "VBus config must be provided");
         }

         if (config->Properties == nullptr)
         {
            throw gcnew ArgumentNullException("config.Properties", "VBus config.Properties must be provided");
         }
         config_ = config;

         auto properties = to_string(config->Properties);

         if (config->ExportedServices != nullptr && config->ExportedServices->Count > 0)
         {
            services_ = new std::deque<service_callback>();

            std::deque<vbus::abi::service_metadata> metadata;
            std::vector<vbus_service> services;

            for each (VbusServiceConfig ^ service in config->ExportedServices)
            {

               metadata.push_back(
                   {to_string(service->Id), to_string(service->Interface), to_string(service->Metadata)});
               services_->push_back({gcnew ServiceCallback(cts_->Token, service->Handler, service->RunOnThreadPool)});

               auto &metadata_ref = metadata.back();
               services.push_back(vbus_service{
                   vbus_service_metadata{vbus::abi::to_vbus_string_view(metadata_ref.id),
                                         vbus::abi::to_vbus_string_view(metadata_ref.interface),
                                         vbus::abi::to_vbus_string_view(metadata_ref.data), service->ClusterVisible},
                   static_cast<void *>(&services_->back()), &service_callback::on_request});
            }

            client_ = (IntPtr)vbus_create_service(vbus::abi::to_vbus_string_view(properties),
                                                  {services.size(), services.data(), false});
         }
         else
         {
            client_ = (IntPtr)vbus_create_client(vbus::abi::to_vbus_string_view(properties));
         }
         if (config->SubscriptionCallback != nullptr && config->ChannelSubscritions != nullptr &&
             config->ChannelSubscritions->Count > 0)
         {
            for each (String ^ channel_str in config->ChannelSubscritions)
            {
               auto channel_bytes = to_string(channel_str);
               vbus_subscribe(GetClient(), vbus::abi::to_vbus_string_view(channel_bytes));
            }

            subscription_callback_ = new subscription_callback(config->SubscriptionCallback);
            vbus_set_subscription_callback(GetClient(), &subscription_callback::callback, subscription_callback_);
         }
      }

      ~VbusClient()
      {
         GC::SuppressFinalize(this);
         Free();
      }

      !VbusClient()
      {
         Free();
      }

      void RunAsync()
      {
         thread_->Start();
      }

      void SetDefaultRequestTimeout(Int64 ms)
      {
         access_handle handle(this);
         try
         {
            vbus_set_default_request_timeout(GetClient(), ms);
         }
         catch (Exception ^)
         {

            throw;
         }
         catch (std::exception const &ex)
         {

            throw gcnew Exception(Utils::ToString(ex.what()));
         }
         catch (...)
         {

            throw gcnew Exception();
         }
      }

      void Publish(Message ^ message, bool sticky)
      {
         access_handle handle(this);

         try
         {
            vbus_client_ptr client = GetClient();

            message_wrapper wrapped_data{message};
            if (!vbus_publish(client, wrapped_data.get_array_view(), sticky))
            {
               throw gcnew OperationCanceledException(
                   Utils::ToString("unable to publish message client is shutting down"));
            }
         }
         catch (Exception ^)
         {

            throw;
         }
         catch (std::exception const &ex)
         {

            throw gcnew Exception(Utils::ToString(ex.what()));
         }
         catch (...)
         {

            throw gcnew Exception();
         }
      }

      vbus_client_ptr GetClient()
      {
         CheckDisposed();
         return (vbus_client_ptr)client_;
      }

      Response ^
          SendRequestSync(Request ^ request) {
             if (request == nullptr) throw gcnew ArgumentNullException("request");
             if (request->ServiceId == nullptr) throw gcnew ArgumentNullException("request.ServiceId");
             if (request->ServiceId->Length == 0 || request->ServiceId->Length > 127)
                throw gcnew ArgumentException("request.ServiceId.Length is longer than 127 or empty");

             array<Byte> ^ service_id_bytes = Utils::ToUtf8(request->ServiceId);
             pin_ptr<Byte> service_ptr = &service_id_bytes[0];

             access_handle handle(this);

             try
             {
                vbus_client_ptr client = GetClient();

                auto *vbus_req = vbus_create_request(client, make_str_view(service_ptr, service_id_bytes->Length));
                if (vbus_req == nullptr)
                {

                   throw gcnew ArgumentException("vbus refused to create request");
                }

                if (request->TimeoutMiliseconds > 0)
                {
                   vbus_req->timeout_ms = request->TimeoutMiliseconds;
                }

                vbus_req->action_id = request->ActionId;

                message_wrapper wrapped_data{request->Data};
                vbus_response_t *response = vbus_send_request_sync(client, vbus_req, wrapped_data.get_array_view());

                if (response != nullptr)
                {
                   if (response->status == -503)
                   {
                      vbus_free_response(client, response);

                      throw gcnew VbusServiceCallTimeoutException();
                   }
                   else
                   {
                      Response ^ result = gcnew Response();
                      result->Status = response->status;
                      result->Data = gcnew Message(response->message);

                      vbus_free_response(client, response);

                      return result;
                   }
                }
                else
                {
                   throw gcnew OperationCanceledException("vbus client returned nullptr as the response (unexpected)");
                }
             }
             catch (Exception ^)
             {

                throw;
             }
             catch (std::exception const &ex)
             {

                throw gcnew Exception(Utils::ToString(ex.what()));
             }
             catch (...)
             {

                throw gcnew Exception();
             }
          } Threading::Tasks::Task<Response ^> ^
          SendRequestAsync(Request ^ request) {
             if (request == nullptr) throw gcnew ArgumentNullException("request");
             if (request->ServiceId == nullptr) throw gcnew ArgumentNullException("request.ServiceId");
             if (request->ServiceId->Length == 0 || request->ServiceId->Length > 127)
                throw gcnew ArgumentException("request.ServiceId.Length is longer than 127 or empty");

             array<Byte> ^ service_id_bytes = Utils::ToUtf8(request->ServiceId);
             pin_ptr<Byte> service_ptr = &service_id_bytes[0];

             access_handle handle(this);
             try
             {

                vbus_client_ptr client = GetClient();
                auto *vbus_req = vbus_create_request(client, make_str_view(service_ptr, service_id_bytes->Length));
                if (vbus_req == nullptr)
                {
                   throw gcnew ArgumentException("vbus refused to create request");
                }

                Threading::Tasks::TaskCompletionSource<Response ^> ^ task_compl =
                    gcnew Threading::Tasks::TaskCompletionSource<Response ^>();

                auto *async_context = new async_request_data(this, task_compl);
                vbus_req->user_data = async_context;
                vbus_req->request_callback = &async_request_data::request_callback;

                if (request->TimeoutMiliseconds > 0)
                {
                   vbus_req->timeout_ms = request->TimeoutMiliseconds;
                }

                vbus_req->action_id = request->ActionId;

                message_wrapper wrapped_data{request->Data};
                if (vbus_send_request(client, vbus_req, wrapped_data.get_array_view()))
                {
                   return task_compl->Task;
                }
                else
                {
                   throw gcnew OperationCanceledException("vbus_send_request returned false (unexpected)");
                }
             }
             catch (Exception ^)
             {
                throw;
             }
             catch (std::exception const &ex)
             {

                throw gcnew Exception(Utils::ToString(ex.what()));
             }
             catch (...)
             {
                throw gcnew Exception();
             }
          }

          void AccessIncrement()
      {
         Threading::Interlocked::Increment(access_counter_);
      }

      void AccessDecrement()
      {
         Threading::Interlocked::Decrement(access_counter_);
      }

      bool IsDisposed()
      {
         return client_ == IntPtr::Zero;
      }

    private:
      void Free()
      {

         vbus_client_ptr client = TryExchangeToNull();
         if (client)
         {
            vbus_stop_client(client);

            WaitUntilAllClientCallsAreFinished();

            if (thread_->IsAlive)
            {
               cts_->Cancel();
               thread_->Join();
            }

            vbus_free_client(client);

            delete subscription_callback_;
            delete services_;
         }
      }

      void WaitUntilAllClientCallsAreFinished()
      {
         int wait_for = 100;
         while (Threading::Interlocked::Read(access_counter_) > 0)
         {

            if (--wait_for <= 0)
            {
               break;
            }

            Threading::Thread::Sleep(100);
         }
      }

      void CheckDisposed()
      {
         if (client_ == IntPtr::Zero)
         {
            throw gcnew ObjectDisposedException("VbusClient");
         }
      }

      void DoRun()
      {
         try
         {
            auto client = GetClient();
            while (!cts_->IsCancellationRequested)
            {
               vbus_run_step(client, 1000);
            }
         }
         catch (...)
         {
         }
      }

      vbus_client_ptr TryExchangeToNull()
      {
         return (vbus_client_ptr)Threading::Interlocked::Exchange(client_, IntPtr::Zero);
      }

      Int64 access_counter_;

      IntPtr client_;
      VbusConfig ^ config_;
      subscription_callback *subscription_callback_;
      std::deque<service_callback> *services_;

      Threading::CancellationTokenSource ^ cts_;
      Threading::Thread ^ thread_;
   };

 public
   ref class VbusDirectory
   {
    public:
      VbusDirectory(String ^ props)
      {
         auto str = to_string(props);
         directory_ = vbus_run_directory(vbus::abi::to_vbus_string_view(str));
      }

      String ^
          GetConnectionString() {
             CheckDisposed();
             auto view = vbus::abi::to_std_string_view(vbus_get_connection_string(directory_));

             if (view.empty())
             {
                return String::Empty;
             }
             else
             {

                array<Byte> ^ buffer = gcnew array<Byte>(gsl::narrow<int>(view.size()));

                pin_ptr<Byte> buffer_ptr = &buffer[0];
                std::copy(view.begin(), view.end(), reinterpret_cast<char *>(buffer_ptr));

                return Text::Encoding::UTF8->GetString(buffer);
             }
          }

          String
          ^
          GetInProcessConnectionProps() {
             CheckDisposed();

             Text::StringBuilder ^ buffer = gcnew Text::StringBuilder(100);
             buffer->Append("vbus.instance.name = inproc-dir\nvbus.connection.string=");
             buffer->Append(GetConnectionString());
             return buffer->ToString();
          }

          ~VbusDirectory()
      {
         GC::SuppressFinalize(this);
         Free();
      }

      !VbusDirectory()
      {
         Free();
      }

    private:
      void Free()
      {
         if (auto dir = directory_; dir != nullptr)
         {
            directory_ = nullptr;
            vbus_free_directory(dir);
         }
      }
      void CheckDisposed()
      {
         if (directory_ == nullptr)
         {
            throw gcnew ObjectDisposedException("VbusDirectory");
         }
      }
      vbus_directory_ptr directory_;
   };

   void async_request_data::request_callback(vbus_request_t *request, vbus_response_t *response)
   {
      std::unique_ptr<async_request_data> self{reinterpret_cast<async_request_data *>(request->user_data)};

      Response ^ resp = gcnew Response();
      resp->Status = response->status;
      resp->Data = gcnew Message(response->message);

      ResponseCallBack ^ callback = gcnew ResponseCallBack(resp, self->task_completion_source_);

      Threading::SynchronizationContext ^ context = self->context_;
      if (context != nullptr)
      {
         context->Post(gcnew Threading::SendOrPostCallback(callback, &ResponseCallBack::OnDone), callback);
      }
      else
      {
         Threading::ThreadPool::UnsafeQueueUserWorkItem(
             gcnew Threading::WaitCallback(callback, &ResponseCallBack::OnDone), callback);
      }
   }

   access_handle::access_handle(VbusClient ^ client) : client_(client)
   {
      client_->AccessIncrement();
   }
   access_handle::~access_handle()
   {

      client_->AccessDecrement();
   }

   /* bool ResponseSender::IsClientDisposed()
    {
       return client_->IsDisposed();
    }*/
} // namespace Vbus