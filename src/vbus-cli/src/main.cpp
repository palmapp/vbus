#include <base64/base64.h>
#include <boost/algorithm/string.hpp>
#include <boost/dll/runtime_symbol_info.hpp>
#include <commons/application_properties.h>
#include <commons/cmd_parser.h>
#include <commons/guid.h>
#include <commons/make_string.h>
#include <fmt/format.h>
#include <iostream>
#include <serialization/msgpack.h>
#include <vbus/vbus.hpp>

#include <vbus/vbus_easylogging.hpp>
SHARE_EASYLOGGINGPP_WITH_VBUS

void apply_json_conversion(std::vector<std::string> &vector);
void apply_base64_conversion(std::vector<std::string> &vector);
void exec_cmd(std::string basicString, std::string basicString1, std::string basicString2,
              std::vector<std::string> vector);

using namespace boost::filesystem;
using namespace commons;
using namespace std::string_literals;
using namespace std::chrono_literals;

application_properties_settings prepare_properties_load_settings(const path &base_path, const path &variant)
{
   application_properties_settings app_prop_settings;
   app_prop_settings.base_path = base_path.c_str();
   if (auto it = std::find(app_prop_settings.properties_variants.begin(), app_prop_settings.properties_variants.end(),
                           variant.string());
       it == app_prop_settings.properties_variants.end())
   {
      app_prop_settings.properties_variants.push_back(variant.string());
   }
   return app_prop_settings;
}

void print_usage()
{
   std::cerr << "usage: vbus-cli <properties> <channel> <message parts>\n";

   std::cerr << "\t<channel> <channel path> sets the channel property (this argument is required)\n";
   std::cerr << "\t<message parts> message parts to be published (this argument is required)\n";
   std::cerr << "\t<properties>:";
   std::cerr << "\t\t-c <path> - sets the vbus configuration file for vbus.connection.string\n";
   std::cerr << "\t\t--cmd [help|list|list_details|pub_sticky|pub|subs] sets the cmd (the default is help)\n";

   std::cerr << "\t\t--convert [json|base64|base64json|none] sets the message conversion method to "
                "msgpack (the "
                "default is none)\n\t\tjson and base64json are not currently implemented\n";
   std::cerr << "\t\tif base64 is selected data are converted to binary and send over to vbus\n";
}

int index_of_channel(int argc, const char *argv[])
{
   using namespace boost::algorithm;

   bool is_property = false;
   int i = 1;
   for (; i < argc; ++i)
   {
      if (is_property)
      {
         is_property = false;
      }
      else if (starts_with(argv[i], "-"))
      {
         is_property = true;
      }
      else
      {
         break;
      }
   }

   return i;
}
std::vector<std::string> gather_message_parts(int idx, int argc, const char *argv[])
{
   std::vector<std::string> parts;
   parts.reserve(argc - idx);

   for (; idx < argc; ++idx)
   {
      parts.push_back(argv[idx]);
   }

   return parts;
}

void apply_message_conv(const std::string &convert, std::vector<std::string> &message_parts)
{
   if (!convert.empty())
   {
      if (convert == "json")
      {
         apply_json_conversion(message_parts);
      }
      else if (convert == "base64")
      {
         apply_base64_conversion(message_parts);
      }
      else if (convert == "base64json")
      {
         apply_base64_conversion(message_parts);
         apply_json_conversion(message_parts);
      }
      else
      {
         throw std::logic_error(make_string("unknown conversion method:", convert));
      }
   }
}

void apply_json_conversion(std::vector<std::string> &parts)
{
   throw std::logic_error("json conversion is not implemented");
}

void apply_base64_conversion(std::vector<std::string> &parts)
{
   for (auto &part : parts)
   {
      part = base64_decode(part);
   }
}

int main(int argc, const char *argv[])
{
   auto exe_directory = boost::dll::program_location().parent_path();
   path config_path = exe_directory / "env.properties";
   cmd::try_get_arg(config_path, "-c", argc, argv);

   config_path = absolute(config_path);
   try
   {

      application_properties props{prepare_properties_load_settings(config_path.parent_path(), config_path.filename())};
      auto connection_string = props.get<std::string>("vbus.connection.string");
      auto instance_name = props.get<std::string>("vbus.instance.name");

      if (connection_string.empty() || instance_name.empty())
      {
         std::cerr << "error: vbus.connection.string or vbus.instance.name not found in " << config_path.string()
                   << " or in any of parent directories. \n\n";

         print_usage();
         return 1;
      }

      auto vbus_props =
          fmt::format("vbus.connection.string={}\nvbus.instance.name={}\n", connection_string, instance_name);

      std::string cmd, convert;
      if (!cmd::try_get_arg(cmd, "--cmd", argc, argv))
      {
         cmd = "help";
      }

      if (cmd == "help")
      {
         print_usage();
         return 0;
      }

      cmd::try_get_arg(convert, "--convert", argc, argv);
      int idx = index_of_channel(argc, argv);
      if (idx < argc)
      {
         std::string channel = argv[idx];
         std::vector<std::string> message_parts = gather_message_parts(idx + 1, argc, argv);
         if ((cmd == "pub" || cmd == "pub_sticky") && message_parts.empty())
         {
            print_usage();
            return 1;
         }

         apply_message_conv(convert, message_parts);
         exec_cmd(vbus_props, cmd, channel, message_parts);
      }
      else if (cmd == "list")
      {
         exec_cmd(vbus_props, cmd, {}, {});
      }
   }
   catch (std::exception const &ex)
   {
      std::cerr << "exception: " << ex.what() << "\n\n";
      print_usage();
      return 1;
   }
   catch (...)
   {
      std::cerr << "exception: unknown\n\n";
      print_usage();
      return 1;
   }
}

void exec_cmd(std::string props, std::string cmd, std::string channel, std::vector<std::string> msg_parts)
{
   using namespace vbus::abi;

   std::mutex m;
   std::condition_variable cv;
   bool received_data = false;

   auto command_done = [&]() mutable
   {
      {
         std::unique_lock lk{m};
         received_data = true;
      }
      cv.notify_one();
   };

   auto wait_for_done = [&]() mutable
   {
      std::unique_lock lk{m};
      return cv.wait_for(lk, 10s, [&]() { return received_data; });
   };

   client cl{props};
   if (!channel.empty())
   {
      cl.subscribe(channel);

      auto subs_handler = vbus::abi::make_subscription_handler(
          [&](string_array const &msg) mutable
          {
             if (boost::starts_with(channel, msg[0]))
             {
                if (cmd == "subs")
                {
                   auto it = msg.begin();
                   ++it;
                   std::for_each(it, msg.end(), [](std::string_view part) { std::cout << part << "\n"; });
                }
                command_done();
             }
          });
      cl.set_subscription_handler(subs_handler);
      cl.run_async();

      if (bool sticky = cmd == "pub_sticky"; sticky || cmd == "pub")
      {
         std::this_thread::sleep_for(500ms);

         std::vector<std::string> publish_message{channel};
         std::copy(msg_parts.begin(), msg_parts.end(), std::back_inserter(publish_message));

         cl.publish(publish_message, sticky);
      }

      wait_for_done();
   }
   else if (cmd == "list" || cmd == "list_details")
   {
      auto handler = make_directory_status_handler(
          [&](directory_status_ptr status_ptr) mutable
          {
             if (cmd == "list")
             {
                for (size_t i = 0; i < status_ptr->length; ++i)
                {
                   std::cout << to_std_string_view(status_ptr->services[i].id) << '\n';
                }
             }
             else if (cmd == "list_details")
             {
                for (size_t i = 0; i < status_ptr->length; ++i)
                {
                   auto &service = status_ptr->services[i];
                   auto &info = status_ptr->statuses[i];

                   auto name = to_std_string_view(service.id);
                   auto connection_string = to_std_string_view(info.connection_string);

                   std::cout << name << " => " << connection_string
                             << (info.is_online ? " [online]\n" : " [offline]\n");
                }
             }

             command_done();
          });

      cl.set_directory_status_handler(handler);
      cl.run_async();
      wait_for_done();
   }

   if (received_data)
   {
      std::cout << "command completed\n";
   }
   else
   {
      throw std::logic_error{"command failed due time out"};
   }
}
